note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a1, a2: A
			b1, b2: B
			sa1, sa2: separate A
			sb1, sb2: separate B
		do
			create a1.make
			create a2.make
			create b1.make (a1, a2)
			create b2.make (a1, a2)

			create sa1.make
			create sa2.make
			create sb1.make (a1, sa2)
			create sb2.make (sa1, a2)

			sa1 := a1
			sa2 := sa1
			a2 := a1
		end

end
