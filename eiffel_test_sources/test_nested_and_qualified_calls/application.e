note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature -- Initialization

	make
			-- Run application.
		local
			a1, a2: A
			b1, b2: B
		do
			create a1.make
			create a2.make
			create b1.make (a1, a2)
			create b2.make (a1, a2)

			b1.command
			b2.get_a.command

			run_with (b1)
		end

	run_with (b: separate b)
		do
			b.get_a.yourself.command
		end

end
