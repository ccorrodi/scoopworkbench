note
	description: "Summary description for {B}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	B

create
	make

feature
	a1: separate A
	a2: A

	make (anA1: separate A; anA2: A)
		do
			a1 := anA1
			a2 := anA2
		end

	command
		do
			-- do nothing
		end

	query: INTEGER
		do
			Result := 1
		end

	get_separate_a: separate A
		do
			Result := a1
		end

	get_a: A
		do
			Result := a2
		end

end
