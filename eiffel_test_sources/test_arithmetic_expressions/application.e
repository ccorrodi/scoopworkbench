note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature -- Initialization
	make
			-- Run application.
		local
			i, j, k: INTEGER
		do
			i := 1 + 2 - 3
			j := 4
			k := 5
			i := 1 - j
			j := k - i
			k := i - j
		end

end
