note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			provider: separate PROVIDER
			client: separate CLIENT
			shared: separate SHARED
		do
			create shared.make
			create provider.make (shared)
			create client.make (shared)

			start (provider, client)
		end

	start (provider: separate PROVIDER; client: separate CLIENT)
		do
			client.go
			provider.go
		end


end
