note
	description: "Summary description for {SHARED}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	SHARED

create
	make

feature
	make
		do
			can_be_consumed := false
			can_be_produced := true
		end

	can_be_consumed: BOOLEAN
	can_be_produced: BOOLEAN

	produce
		require
			can_be_produced
		do
			can_be_consumed := true
			can_be_produced := false
		ensure
			can_be_consumed
		end

	consume
		require
			can_be_consumed
		do
			can_be_consumed := false
			can_be_produced := true
		ensure
			can_be_produced
		end

end
