note
	description: "Summary description for {CLIENT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CLIENT

create
	make

feature
	shared: separate SHARED

	make (a_shared: separate SHARED)
		do
			shared := a_shared
		end

	go
		local
			i: INTEGER
		do
--			consume (shared)
			from
				i := 0
			until
				i > 2
			loop
				consume (shared)
				i := i + 1
			end
		end

	consume (a_shared: separate SHARED)
		require
			a_shared.can_be_consumed
		do
			a_shared.consume
		end

end
