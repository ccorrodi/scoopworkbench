note
	description: "Summary description for {PROVIDER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PROVIDER

create
	make

feature
	shared: separate SHARED

	make (a_shared: separate SHARED)
		do
			shared := a_shared
		end

	go
		local
			i: INTEGER
		do
--			produce (shared)
			from
				i := 0
			until
				i > 2
			loop
				produce (shared)
				i := i + 1
			end
		end

	produce (a_shared: separate SHARED)
		require
			a_shared.can_be_produced
		do
			a_shared.produce
		end
end
