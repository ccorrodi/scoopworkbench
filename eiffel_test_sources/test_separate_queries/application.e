note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a1, a2: separate A
			b1, b2: separate B
		do
			create a1.make
			create a2.make
			create b1.make (a1, a2)
			create b2.make (a1, a2)

			run (a1, a2)
		end

	run (a: separate A; b: separate A)
		local
			i: INTEGER
		do
			i := a.query
			i := b.query
		end

end
