note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a, b: separate A
		do
			create a.make
			create b.make

			run (a, b)
		end

	run (a: separate A; b: separate A)
		do
			a.command
			b.command
		end

end
