note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a1, a2: A
			b1, b2: B
		do
			create a1.make
			create a2.make
			create b1.make (a1, a2)
			create b2.make (a1, a2)

			run (a1, a2)
		end

	run (a1: separate A; a2: separate A)
		local
			i: INTEGER
		do
			i := a1.query
			i := a2.query
			a1.command
			a2.command
			i := a1.query
			a1.command
			i := a1.query
		end

end
