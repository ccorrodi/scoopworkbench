note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature -- Initialization
	i: INTEGER

	make
			-- Run application.
		do
			from
				i := 0
			until
				i > 10
			loop
				i := i + 1
			end
		ensure
			i = 11
		end

end
