﻿class
	APPLICATION

create
	make

feature -- Initialisation

	i: INTEGER
	first_fork, left_fork, right_fork: separate FORK
	a_philosopher: separate PHILOSOPHER

	make
			-- Create philosophers and forks
			-- and initiate the dinner.
		do
			philosopher_count := 2
			round_count := 1

			-- Dining Philosophers with `philosopher_count' philosophers and `round_count' rounds.

			from
				i := 1
				create first_fork.make
				left_fork := first_fork
			until
				i > philosopher_count
			loop
				if i < philosopher_count then
					create right_fork.make
				else
					right_fork := first_fork
				end
				create a_philosopher.make (i, left_fork, right_fork, round_count)
				launch_philosopher (a_philosopher)
				left_fork := right_fork
				i := i + 1
			end
		end

feature {NONE} -- Implementation

	philosopher_count: INTEGER
			-- Number of philosophers.

	round_count: INTEGER
			-- Number of times each philosopher should eat.

	launch_philosopher (philosopher: separate PHILOSOPHER)
			-- Launch a_philosopher.
		do
			philosopher.live
		end

end
