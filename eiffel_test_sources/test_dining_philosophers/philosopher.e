﻿class
	PHILOSOPHER

create
	make

feature -- Initialisation

	make (philosopher: INTEGER; left, right: separate FORK; round_count: INTEGER)
			-- Initialise with ID of `philosopher', forks `left' and `right', and for `round_count' times to eat.
		require
			valid_id: philosopher > 0
			valid_times_to_eat: round_count > 0
		do
			id := philosopher
			left_fork := left
			right_fork := right
			times_to_eat := round_count
		ensure
			id_set: id = philosopher
			left_fork_set: left_fork = left
			right_fork_set: right_fork = right
			times_to_eat_set: times_to_eat = round_count
		end

feature -- Access

	id: INTEGER
			-- Philosopher's id.

feature -- Measurement

	times_to_eat: INTEGER
			-- How many times does it remain for the philosopher to eat?

feature -- Basic operations

	eat (left, right: separate FORK)
			-- Eat, having acquired `left' and `right' forks.
		do
			-- Eating takes place.
		ensure
			has_eaten: true
		end

	live
		do
			from
			until
				times_to_eat < 1
			loop
				-- Philosopher `Current.id' waiting for forks.
				eat (left_fork, right_fork)
				--bad_eat
				-- Philosopher `Current.id' has eaten.
				times_to_eat := times_to_eat - 1
			end
		end

	bad_eat
			-- Eat, by first picking up `left_fork' (and picking up `right_fork'
			-- in the `pickup_left' call.
		do
			pickup_left (left_fork)
		end

	pickup_left (left: separate FORK)
			-- After having picked up `left', proceed to pick up `right_fork'.
		do
			pickup_right (right_fork)
		end

	pickup_right (right: separate FORK)
			-- Both forks have been acquired at this point.
		do
			-- eating takes place
		end

feature {NONE} -- Access

	left_fork: separate FORK
			-- Left fork used for eating.	

	right_fork: separate FORK
			-- Right fork used for eating.

invariant
	valid_id: id >= 1

end
