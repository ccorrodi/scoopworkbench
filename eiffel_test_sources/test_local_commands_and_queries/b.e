note
	description: "Summary description for {B}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	B

create
	make

feature
	a1, a2: A

	make (anA1: A; anA2: A)
		do
			a1 := anA1
			a2 := anA2
		end

	command
		do
			-- do nothing
		end

	query: INTEGER
		do
			Result := 1
		end

end
