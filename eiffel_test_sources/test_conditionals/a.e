note
	description: "Summary description for {A}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	A

create
	make

feature
	make
		do
		end

	command
		do
			-- do nothing
		end

	query: INTEGER
		do
			Result := 1
		end

	yourself: A
		do
			Result := Current
		end

end
