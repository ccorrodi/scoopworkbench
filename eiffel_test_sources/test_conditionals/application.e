note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature -- Initialization

	make
			-- Run application.
		local
			b1, b2: BOOLEAN
			i1, i2, i3, i4: INTEGER
		do
			b1 := True
			b2 := False

			if True then
				i1 := 1
			else
				i2 := 2
			end

			if False then
			else
				i3 := 3
			end

			if b1 then
				i4 := 4
			end

		end

	run_with (b: separate b)
		do
			b.get_a.yourself.command
		end

end
