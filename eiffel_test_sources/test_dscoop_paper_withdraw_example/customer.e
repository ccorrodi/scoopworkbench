note
	description: "Summary description for {CUSTOMER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	CUSTOMER

create
	make

feature

	make
		do
		end

	withdraw (acc: separate ACCOUNT; am: INTEGER)
		do
			if acc.balance >= am then
				acc.set_balance (acc.balance - am)
			end
		end

	transfer (s, t: separate ACCOUNT; am: INTEGER)
		do
			if s.balance >= am then
				s.set_balance (s.balance - am)
				t.set_balance (t.balance - am)
			end
		end

end
