note
	description: "project application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

create
	make

feature {NONE} -- Initialization

	customer1, customer2: separate CUSTOMER
	a1, a2: separate ACCOUNT

	make
			-- Run application.
		do
			create a1.make
			create a2.make

			create customer1.make
			create customer2.make

			run (customer1, customer2)
		end

	run (c1: separate CUSTOMER; c2: separate CUSTOMER)
		do
			c1.transfer (a1, a2, 10)
			c2.withdraw (a1, 20)
		end
end
