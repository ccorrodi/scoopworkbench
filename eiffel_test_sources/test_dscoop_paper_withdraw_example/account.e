note
	description: "Summary description for {ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ACCOUNT

create
	make

feature

	make
		do
			balance := 100
		end

	balance: INTEGER

	set_balance (am: INTEGER)
		do
			balance := am
		end

end
