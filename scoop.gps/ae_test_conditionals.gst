<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
	<graph edgeids="false" edgemode="directed" id="debug_translation" role="graph">
		<node id="node0">
			<attr name="layout">
				<string>500 250 250 50</string>
			</attr>
		</node>
		<node id="node1">
			<attr name="layout">
				<string>500 650 250 50</string>
			</attr>
		</node>
		<node id="node2">
			<attr name="layout">
				<string>500 1050 250 50</string>
			</attr>
		</node>
		<node id="node3">
			<attr name="layout">
				<string>500 1450 250 50</string>
			</attr>
		</node>
		<node id="node4">
			<attr name="layout">
				<string>150 1450 250 50</string>
			</attr>
		</node>
		<node id="node5">
			<attr name="layout">
				<string>850 1450 250 50</string>
			</attr>
		</node>
		<node id="node6">
			<attr name="layout">
				<string>850 1550 250 50</string>
			</attr>
		</node>
		<node id="node7">
			<attr name="layout">
				<string>1050 1550 250 50</string>
			</attr>
		</node>
		<node id="node8">
			<attr name="layout">
				<string>1200 1450 250 50</string>
			</attr>
		</node>
		<node id="node9">
			<attr name="layout">
				<string>500 1850 250 50</string>
			</attr>
		</node>
		<node id="node10">
			<attr name="layout">
				<string>150 1850 250 50</string>
			</attr>
		</node>
		<node id="node11">
			<attr name="layout">
				<string>850 1850 250 50</string>
			</attr>
		</node>
		<node id="node12">
			<attr name="layout">
				<string>850 1950 250 50</string>
			</attr>
		</node>
		<node id="node13">
			<attr name="layout">
				<string>1050 1950 250 50</string>
			</attr>
		</node>
		<node id="node14">
			<attr name="layout">
				<string>1200 1850 250 50</string>
			</attr>
		</node>
		<node id="node15">
			<attr name="layout">
				<string>500 2250 250 50</string>
			</attr>
		</node>
		<node id="node16">
			<attr name="layout">
				<string>500 2650 250 50</string>
			</attr>
		</node>
		<node id="node17">
			<attr name="layout">
				<string>150 2650 250 50</string>
			</attr>
		</node>
		<node id="node18">
			<attr name="layout">
				<string>150 2750 250 50</string>
			</attr>
		</node>
		<node id="node19">
			<attr name="layout">
				<string>150 2850 250 50</string>
			</attr>
		</node>
		<node id="node20">
			<attr name="layout">
				<string>150 2950 250 50</string>
			</attr>
		</node>
		<node id="node21">
			<attr name="layout">
				<string>150 3050 250 50</string>
			</attr>
		</node>
		<node id="node22">
			<attr name="layout">
				<string>150 3150 250 50</string>
			</attr>
		</node>
		<node id="node23">
			<attr name="layout">
				<string>850 2650 250 50</string>
			</attr>
		</node>
		<node id="node24">
			<attr name="layout">
				<string>850 2750 250 50</string>
			</attr>
		</node>
		<node id="node25">
			<attr name="layout">
				<string>1050 2750 250 50</string>
			</attr>
		</node>
		<node id="node26">
			<attr name="layout">
				<string>1200 2650 250 50</string>
			</attr>
		</node>
		<node id="node27">
			<attr name="layout">
				<string>1550 2650 250 50</string>
			</attr>
		</node>
		<node id="node28">
			<attr name="layout">
				<string>1550 2750 250 50</string>
			</attr>
		</node>
		<node id="node29">
			<attr name="layout">
				<string>1750 2750 250 50</string>
			</attr>
		</node>
		<node id="node30">
			<attr name="layout">
				<string>1900 2650 250 50</string>
			</attr>
		</node>
		<node id="node31">
			<attr name="layout">
				<string>2250 2650 250 50</string>
			</attr>
		</node>
		<node id="node32">
			<attr name="layout">
				<string>2250 2750 250 50</string>
			</attr>
		</node>
		<node id="node33">
			<attr name="layout">
				<string>2600 2650 250 50</string>
			</attr>
		</node>
		<node id="node34">
			<attr name="layout">
				<string>2950 2650 250 50</string>
			</attr>
		</node>
		<node id="node35">
			<attr name="layout">
				<string>2950 2750 250 50</string>
			</attr>
		</node>
		<node id="node36">
			<attr name="layout">
				<string>3150 2750 250 50</string>
			</attr>
		</node>
		<node id="node37">
			<attr name="layout">
				<string>3300 2650 250 50</string>
			</attr>
		</node>
		<node id="node38">
			<attr name="layout">
				<string>3650 2650 250 50</string>
			</attr>
		</node>
		<node id="node39">
			<attr name="layout">
				<string>3650 2750 250 50</string>
			</attr>
		</node>
		<node id="node40">
			<attr name="layout">
				<string>4000 2650 250 50</string>
			</attr>
		</node>
		<node id="node41">
			<attr name="layout">
				<string>4350 2650 250 50</string>
			</attr>
		</node>
		<node id="node42">
			<attr name="layout">
				<string>4350 2750 250 50</string>
			</attr>
		</node>
		<node id="node43">
			<attr name="layout">
				<string>4700 2650 250 50</string>
			</attr>
		</node>
		<node id="node44">
			<attr name="layout">
				<string>5050 2650 250 50</string>
			</attr>
		</node>
		<node id="node45">
			<attr name="layout">
				<string>5050 2750 250 50</string>
			</attr>
		</node>
		<node id="node46">
			<attr name="layout">
				<string>5250 2750 250 50</string>
			</attr>
		</node>
		<node id="node47">
			<attr name="layout">
				<string>5400 2650 250 50</string>
			</attr>
		</node>
		<node id="node48">
			<attr name="layout">
				<string>5400 2650 250 50</string>
			</attr>
		</node>
		<node id="node49">
			<attr name="layout">
				<string>5400 2750 250 50</string>
			</attr>
		</node>
		<node id="node50">
			<attr name="layout">
				<string>5750 2650 250 50</string>
			</attr>
		</node>
		<node id="node51">
			<attr name="layout">
				<string>5750 2750 250 50</string>
			</attr>
		</node>
		<node id="node52">
			<attr name="layout">
				<string>6100 2650 250 50</string>
			</attr>
		</node>
		<node id="node53">
			<attr name="layout">
				<string>6450 2650 250 50</string>
			</attr>
		</node>
		<node id="node54">
			<attr name="layout">
				<string>6450 2750 250 50</string>
			</attr>
		</node>
		<node id="node55">
			<attr name="layout">
				<string>6650 2750 250 50</string>
			</attr>
		</node>
		<node id="node56">
			<attr name="layout">
				<string>6800 2650 250 50</string>
			</attr>
		</node>
		<node id="node57">
			<attr name="layout">
				<string>6800 2750 250 50</string>
			</attr>
		</node>
		<node id="node58">
			<attr name="layout">
				<string>7150 2650 250 50</string>
			</attr>
		</node>
		<node id="node59">
			<attr name="layout">
				<string>7500 2650 250 50</string>
			</attr>
		</node>
		<node id="node60">
			<attr name="layout">
				<string>7500 2750 250 50</string>
			</attr>
		</node>
		<node id="node61">
			<attr name="layout">
				<string>7700 2750 250 50</string>
			</attr>
		</node>
		<node id="node62">
			<attr name="layout">
				<string>500 3050 250 50</string>
			</attr>
		</node>
		<node id="node63">
			<attr name="layout">
				<string>150 3050 250 50</string>
			</attr>
		</node>
		<node id="node64">
			<attr name="layout">
				<string>850 3050 250 50</string>
			</attr>
		</node>
		<node id="node65">
			<attr name="layout">
				<string>850 3150 250 50</string>
			</attr>
		</node>
		<node id="node66">
			<attr name="layout">
				<string>1200 3050 250 50</string>
			</attr>
		</node>
		<node id="node67">
			<attr name="layout">
				<string>500 3450 250 50</string>
			</attr>
		</node>
		<node id="node68">
			<attr name="layout">
				<string>850 3450 250 50</string>
			</attr>
		</node>
		<node id="node69">
			<attr name="layout">
				<string>850 3550 250 50</string>
			</attr>
		</node>
		<node id="node70">
			<attr name="layout">
				<string>850 3650 250 50</string>
			</attr>
		</node>
		<node id="node71">
			<attr name="layout">
				<string>850 3750 250 50</string>
			</attr>
		</node>
		<node id="node72">
			<attr name="layout">
				<string>500 4250 250 50</string>
			</attr>
		</node>
		<node id="node73">
			<attr name="layout">
				<string>150 4250 250 50</string>
			</attr>
		</node>
		<node id="node74">
			<attr name="layout">
				<string>850 4250 250 50</string>
			</attr>
		</node>
		<node id="node75">
			<attr name="layout">
				<string>850 4350 250 50</string>
			</attr>
		</node>
		<node id="node76">
			<attr name="layout">
				<string>1050 4350 250 50</string>
			</attr>
		</node>
		<node id="node77">
			<attr name="layout">
				<string>1200 4250 250 50</string>
			</attr>
		</node>
		<node id="node78">
			<attr name="layout">
				<string>500 4650 250 50</string>
			</attr>
		</node>
		<node id="node79">
			<attr name="layout">
				<string>150 4650 250 50</string>
			</attr>
		</node>
		<node id="node80">
			<attr name="layout">
				<string>850 4650 250 50</string>
			</attr>
		</node>
		<node id="node81">
			<attr name="layout">
				<string>850 4750 250 50</string>
			</attr>
		</node>
		<node id="node82">
			<attr name="layout">
				<string>1050 4750 250 50</string>
			</attr>
		</node>
		<node id="node83">
			<attr name="layout">
				<string>1200 4650 250 50</string>
			</attr>
		</node>
		<node id="node84">
			<attr name="layout">
				<string>500 5050 250 50</string>
			</attr>
		</node>
		<node id="node85">
			<attr name="layout">
				<string>150 5050 250 50</string>
			</attr>
		</node>
		<node id="node86">
			<attr name="layout">
				<string>150 5150 250 50</string>
			</attr>
		</node>
		<node id="node87">
			<attr name="layout">
				<string>850 5050 250 50</string>
			</attr>
		</node>
		<node id="node88">
			<attr name="layout">
				<string>850 5150 250 50</string>
			</attr>
		</node>
		<node id="node89">
			<attr name="layout">
				<string>1050 5150 250 50</string>
			</attr>
		</node>
		<node id="node90">
			<attr name="layout">
				<string>1200 5050 250 50</string>
			</attr>
		</node>
		<node id="node91">
			<attr name="layout">
				<string>1550 5050 250 50</string>
			</attr>
		</node>
		<node id="node92">
			<attr name="layout">
				<string>1550 5150 250 50</string>
			</attr>
		</node>
		<node id="node93">
			<attr name="layout">
				<string>1750 5150 250 50</string>
			</attr>
		</node>
		<node id="node94">
			<attr name="layout">
				<string>1900 5050 250 50</string>
			</attr>
		</node>
		<node id="node95">
			<attr name="layout">
				<string>500 5450 250 50</string>
			</attr>
		</node>
		<node id="node96">
			<attr name="layout">
				<string>500 5850 250 50</string>
			</attr>
		</node>
		<node id="node97">
			<attr name="layout">
				<string>150 5850 250 50</string>
			</attr>
		</node>
		<node id="node98">
			<attr name="layout">
				<string>850 5850 250 50</string>
			</attr>
		</node>
		<node id="node99">
			<attr name="layout">
				<string>850 5950 250 50</string>
			</attr>
		</node>
		<node id="node100">
			<attr name="layout">
				<string>1050 5950 250 50</string>
			</attr>
		</node>
		<node id="node101">
			<attr name="layout">
				<string>1200 5850 250 50</string>
			</attr>
		</node>
		<node id="node102">
			<attr name="layout">
				<string>500 6250 250 50</string>
			</attr>
		</node>
		<node id="node103">
			<attr name="layout">
				<string>150 6250 250 50</string>
			</attr>
		</node>
		<node id="node104">
			<attr name="layout">
				<string>850 6250 250 50</string>
			</attr>
		</node>
		<node id="node105">
			<attr name="layout">
				<string>850 6350 250 50</string>
			</attr>
		</node>
		<node id="node106">
			<attr name="layout">
				<string>1050 6350 250 50</string>
			</attr>
		</node>
		<node id="node107">
			<attr name="layout">
				<string>1200 6250 250 50</string>
			</attr>
		</node>
		<node id="node108">
			<attr name="layout">
				<string>500 6650 250 50</string>
			</attr>
		</node>
		<node id="node109">
			<attr name="layout">
				<string>150 6650 250 50</string>
			</attr>
		</node>
		<node id="node110">
			<attr name="layout">
				<string>850 6650 250 50</string>
			</attr>
		</node>
		<node id="node111">
			<attr name="layout">
				<string>850 6750 250 50</string>
			</attr>
		</node>
		<node id="node112">
			<attr name="layout">
				<string>1050 6750 250 50</string>
			</attr>
		</node>
		<node id="node113">
			<attr name="layout">
				<string>1200 6650 250 50</string>
			</attr>
		</node>
		<node id="node114">
			<attr name="layout">
				<string>50 50 250 50</string>
			</attr>
		</node>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>let:name="A"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>type:InitialAndFinalState</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>type:InitialAndFinalState</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>let:procedure="query"</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node5" to="node5">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node8" to="node8">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>let:procedure="yourself"</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>let:name="Current"</string>
			</attr>
		</edge>
		<edge from="node14" to="node14">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node15" to="node15">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node15" to="node15">
			<attr name="label">
				<string>let:name="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node17" to="node17">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node17" to="node17">
			<attr name="label">
				<string>let:name="b1"</string>
			</attr>
		</edge>
		<edge from="node18" to="node18">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node18" to="node18">
			<attr name="label">
				<string>let:name="b2"</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>let:name="i1"</string>
			</attr>
		</edge>
		<edge from="node20" to="node20">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node20" to="node20">
			<attr name="label">
				<string>let:name="i2"</string>
			</attr>
		</edge>
		<edge from="node21" to="node21">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node21" to="node21">
			<attr name="label">
				<string>let:name="i3"</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>let:name="i4"</string>
			</attr>
		</edge>
		<edge from="node23" to="node23">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>let:name="b1"</string>
			</attr>
		</edge>
		<edge from="node25" to="node25">
			<attr name="label">
				<string>type:BooleanConstant</string>
			</attr>
		</edge>
		<edge from="node25" to="node25">
			<attr name="label">
				<string>let:value=true</string>
			</attr>
		</edge>
		<edge from="node26" to="node26">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node27" to="node27">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>let:name="b2"</string>
			</attr>
		</edge>
		<edge from="node29" to="node29">
			<attr name="label">
				<string>type:BooleanConstant</string>
			</attr>
		</edge>
		<edge from="node29" to="node29">
			<attr name="label">
				<string>let:value=false</string>
			</attr>
		</edge>
		<edge from="node30" to="node30">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node31" to="node31">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node32" to="node32">
			<attr name="label">
				<string>type:BooleanConstant</string>
			</attr>
		</edge>
		<edge from="node32" to="node32">
			<attr name="label">
				<string>let:value=true</string>
			</attr>
		</edge>
		<edge from="node33" to="node33">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node34" to="node34">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>let:name="i1"</string>
			</attr>
		</edge>
		<edge from="node36" to="node36">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node36" to="node36">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node37" to="node37">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node38" to="node38">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node39" to="node39">
			<attr name="label">
				<string>type:BooleanConstant</string>
			</attr>
		</edge>
		<edge from="node39" to="node39">
			<attr name="label">
				<string>let:value=false</string>
			</attr>
		</edge>
		<edge from="node40" to="node40">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node41" to="node41">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node42" to="node42">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node42" to="node42">
			<attr name="label">
				<string>let:name="b1"</string>
			</attr>
		</edge>
		<edge from="node43" to="node43">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node44" to="node44">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node45" to="node45">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node45" to="node45">
			<attr name="label">
				<string>let:name="i4"</string>
			</attr>
		</edge>
		<edge from="node46" to="node46">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node46" to="node46">
			<attr name="label">
				<string>let:value=4</string>
			</attr>
		</edge>
		<edge from="node47" to="node47">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node48" to="node48">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node49" to="node49">
			<attr name="label">
				<string>type:BooleanNegationExpression</string>
			</attr>
		</edge>
		<edge from="node50" to="node50">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node51" to="node51">
			<attr name="label">
				<string>type:BooleanNegationExpression</string>
			</attr>
		</edge>
		<edge from="node52" to="node52">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node53" to="node53">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node54" to="node54">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node54" to="node54">
			<attr name="label">
				<string>let:name="i3"</string>
			</attr>
		</edge>
		<edge from="node55" to="node55">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node55" to="node55">
			<attr name="label">
				<string>let:value=3</string>
			</attr>
		</edge>
		<edge from="node56" to="node56">
			<attr name="label">
				<string>type:ActionTest</string>
			</attr>
		</edge>
		<edge from="node57" to="node57">
			<attr name="label">
				<string>type:BooleanNegationExpression</string>
			</attr>
		</edge>
		<edge from="node58" to="node58">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node59" to="node59">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node60" to="node60">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node60" to="node60">
			<attr name="label">
				<string>let:name="i2"</string>
			</attr>
		</edge>
		<edge from="node61" to="node61">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node61" to="node61">
			<attr name="label">
				<string>let:value=2</string>
			</attr>
		</edge>
		<edge from="node62" to="node62">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node62" to="node62">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node62" to="node62">
			<attr name="label">
				<string>let:procedure="run_with"</string>
			</attr>
		</edge>
		<edge from="node63" to="node63">
			<attr name="label">
				<string>type:ParameterMapping</string>
			</attr>
		</edge>
		<edge from="node63" to="node63">
			<attr name="label">
				<string>let:index=1</string>
			</attr>
		</edge>
		<edge from="node63" to="node63">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node64" to="node64">
			<attr name="label">
				<string>type:ActionCommand</string>
			</attr>
		</edge>
		<edge from="node64" to="node64">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node65" to="node65">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node65" to="node65">
			<attr name="label">
				<string>let:name="yourself"</string>
			</attr>
		</edge>
		<edge from="node66" to="node66">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node67" to="node67">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node67" to="node67">
			<attr name="label">
				<string>let:name="B"</string>
			</attr>
		</edge>
		<edge from="node68" to="node68">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node68" to="node68">
			<attr name="label">
				<string>let:name="a1"</string>
			</attr>
		</edge>
		<edge from="node69" to="node69">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node69" to="node69">
			<attr name="label">
				<string>let:name="a2"</string>
			</attr>
		</edge>
		<edge from="node70" to="node70">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node70" to="node70">
			<attr name="label">
				<string>let:name="a1"</string>
			</attr>
		</edge>
		<edge from="node71" to="node71">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node71" to="node71">
			<attr name="label">
				<string>let:name="a2"</string>
			</attr>
		</edge>
		<edge from="node72" to="node72">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node72" to="node72">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node72" to="node72">
			<attr name="label">
				<string>let:procedure="a1"</string>
			</attr>
		</edge>
		<edge from="node73" to="node73">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node73" to="node73">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node74" to="node74">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node75" to="node75">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node75" to="node75">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node76" to="node76">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node76" to="node76">
			<attr name="label">
				<string>let:name="a1"</string>
			</attr>
		</edge>
		<edge from="node77" to="node77">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node78" to="node78">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node78" to="node78">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node78" to="node78">
			<attr name="label">
				<string>let:procedure="a2"</string>
			</attr>
		</edge>
		<edge from="node79" to="node79">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node79" to="node79">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node80" to="node80">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node81" to="node81">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node81" to="node81">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node82" to="node82">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node82" to="node82">
			<attr name="label">
				<string>let:name="a2"</string>
			</attr>
		</edge>
		<edge from="node83" to="node83">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node84" to="node84">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node84" to="node84">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node84" to="node84">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node85" to="node85">
			<attr name="label">
				<string>type:ParameterMapping</string>
			</attr>
		</edge>
		<edge from="node85" to="node85">
			<attr name="label">
				<string>let:index=1</string>
			</attr>
		</edge>
		<edge from="node85" to="node85">
			<attr name="label">
				<string>let:name="ana1"</string>
			</attr>
		</edge>
		<edge from="node86" to="node86">
			<attr name="label">
				<string>type:ParameterMapping</string>
			</attr>
		</edge>
		<edge from="node86" to="node86">
			<attr name="label">
				<string>let:index=2</string>
			</attr>
		</edge>
		<edge from="node86" to="node86">
			<attr name="label">
				<string>let:name="ana2"</string>
			</attr>
		</edge>
		<edge from="node87" to="node87">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node88" to="node88">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node88" to="node88">
			<attr name="label">
				<string>let:name="a1"</string>
			</attr>
		</edge>
		<edge from="node89" to="node89">
			<attr name="label">
				<string>type:ParameterExpression</string>
			</attr>
		</edge>
		<edge from="node89" to="node89">
			<attr name="label">
				<string>let:name="ana1"</string>
			</attr>
		</edge>
		<edge from="node90" to="node90">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node91" to="node91">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node92" to="node92">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node92" to="node92">
			<attr name="label">
				<string>let:name="a2"</string>
			</attr>
		</edge>
		<edge from="node93" to="node93">
			<attr name="label">
				<string>type:ParameterExpression</string>
			</attr>
		</edge>
		<edge from="node93" to="node93">
			<attr name="label">
				<string>let:name="ana2"</string>
			</attr>
		</edge>
		<edge from="node94" to="node94">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node95" to="node95">
			<attr name="label">
				<string>type:InitialAndFinalState</string>
			</attr>
		</edge>
		<edge from="node95" to="node95">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node95" to="node95">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node96" to="node96">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node96" to="node96">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node96" to="node96">
			<attr name="label">
				<string>let:procedure="query"</string>
			</attr>
		</edge>
		<edge from="node97" to="node97">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node97" to="node97">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node98" to="node98">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node99" to="node99">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node99" to="node99">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node100" to="node100">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node100" to="node100">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node101" to="node101">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node102" to="node102">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node102" to="node102">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node102" to="node102">
			<attr name="label">
				<string>let:procedure="get_separate_a"</string>
			</attr>
		</edge>
		<edge from="node103" to="node103">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node103" to="node103">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node104" to="node104">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node105" to="node105">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node105" to="node105">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node106" to="node106">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node106" to="node106">
			<attr name="label">
				<string>let:name="a1"</string>
			</attr>
		</edge>
		<edge from="node107" to="node107">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node108" to="node108">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node108" to="node108">
			<attr name="label">
				<string>let:class="B"</string>
			</attr>
		</edge>
		<edge from="node108" to="node108">
			<attr name="label">
				<string>let:procedure="get_a"</string>
			</attr>
		</edge>
		<edge from="node109" to="node109">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node109" to="node109">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node110" to="node110">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node111" to="node111">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node111" to="node111">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node112" to="node112">
			<attr name="label">
				<string>type:AttributeExpression</string>
			</attr>
		</edge>
		<edge from="node112" to="node112">
			<attr name="label">
				<string>let:name="a2"</string>
			</attr>
		</edge>
		<edge from="node113" to="node113">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node3" to="node4">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node3" to="node5">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node5" to="node8">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node5" to="node6">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node5" to="node7">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node9" to="node10">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node9" to="node11">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node11" to="node14">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node11" to="node12">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node11" to="node13">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node16" to="node17">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node18">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node19">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node20">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node21">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node22">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node16" to="node23">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node23" to="node26">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node23" to="node24">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node23" to="node25">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node26" to="node27">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node27" to="node30">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node27" to="node28">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node27" to="node29">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node30" to="node31">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node31" to="node32">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node31" to="node33">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node33" to="node34">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node34" to="node37">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node34" to="node35">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node34" to="node36">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node37" to="node38">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node38" to="node39">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node38" to="node40">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node40" to="node41">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node41" to="node42">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node41" to="node43">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node43" to="node44">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node44" to="node47">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node44" to="node45">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node44" to="node46">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node40" to="node48">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node48" to="node49">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node49" to="node42">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node48" to="node47">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node37" to="node50">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node50" to="node51">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node51" to="node39">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node50" to="node52">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node52" to="node53">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node53" to="node40">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node53" to="node54">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node53" to="node55">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node30" to="node56">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node56" to="node57">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node57" to="node32">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node56" to="node58">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node58" to="node59">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node59" to="node37">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node59" to="node60">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node59" to="node61">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node62" to="node63">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node62" to="node64">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node64" to="node65">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node64" to="node66">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node67" to="node68">
			<attr name="label">
				<string>attribute</string>
			</attr>
		</edge>
		<edge from="node67" to="node69">
			<attr name="label">
				<string>attribute</string>
			</attr>
		</edge>
		<edge from="node67" to="node70">
			<attr name="label">
				<string>attribute</string>
			</attr>
		</edge>
		<edge from="node67" to="node71">
			<attr name="label">
				<string>attribute</string>
			</attr>
		</edge>
		<edge from="node72" to="node73">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node72" to="node74">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node74" to="node77">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node74" to="node75">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node74" to="node76">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node78" to="node79">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node78" to="node80">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node80" to="node83">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node80" to="node81">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node80" to="node82">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node84" to="node85">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node84" to="node86">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node84" to="node87">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node87" to="node90">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node87" to="node88">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node87" to="node89">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node90" to="node91">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node91" to="node94">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node91" to="node92">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node91" to="node93">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node96" to="node97">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node96" to="node98">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node98" to="node101">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node98" to="node99">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node98" to="node100">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node102" to="node103">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node102" to="node104">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node104" to="node107">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node104" to="node105">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node104" to="node106">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node108" to="node109">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node108" to="node110">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node110" to="node113">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node110" to="node111">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node110" to="node112">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node114" to="node114">
			<attr name="label">
				<string>type:Initialization</string>
			</attr>
		</edge>
		<edge from="node114" to="node114">
			<attr name="label">
				<string>let:root_class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node114" to="node114">
			<attr name="label">
				<string>let:root_procedure="make"</string>
			</attr>
		</edge>
	</graph>
</gxl>
