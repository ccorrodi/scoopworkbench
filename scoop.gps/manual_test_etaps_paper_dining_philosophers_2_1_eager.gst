<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="paper_dining_philosophers_2_1_eager">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>472 235 306 80</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>478 615 294 120</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>159 635 232 80</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>149 735 251 80</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>155 835 239 80</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>207 935 136 80</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>93 1035 364 80</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>130 1135 289 80</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>121 1235 307 80</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>849 655 251 40</string>
            </attr>
        </node>
        <node id="n10">
            <attr name="layout">
                <string>793 735 364 80</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>1063 735 223 80</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>1238 655 173 40</string>
            </attr>
        </node>
        <node id="n13">
            <attr name="layout">
                <string>1549 655 251 40</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>1530 735 289 80</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>1763 735 223 80</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>1938 655 173 40</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>2249 655 251 40</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>2266 735 218 80</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>2463 735 223 80</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>2638 655 173 40</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>2946 595 257 160</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>2955 735 239 80</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>3338 655 173 40</string>
            </attr>
        </node>
        <node id="n24">
            <attr name="layout">
                <string>3649 655 251 40</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>3659 735 232 80</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>3855 735 239 80</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>4038 655 173 40</string>
            </attr>
        </node>
        <node id="n28">
            <attr name="layout">
                <string>4402 655 146 40</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>4259 755 432 40</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>4366 835 218 80</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>4493 835 364 80</string>
            </attr>
        </node>
        <node id="n32">
            <attr name="layout">
                <string>4757 655 135 40</string>
            </attr>
        </node>
        <node id="n33">
            <attr name="layout">
                <string>4752 655 146 40</string>
            </attr>
        </node>
        <node id="n34">
            <attr name="layout">
                <string>4631 755 388 40</string>
            </attr>
        </node>
        <node id="n35">
            <attr name="layout">
                <string>5088 655 173 40</string>
            </attr>
        </node>
        <node id="n36">
            <attr name="layout">
                <string>5452 655 146 40</string>
            </attr>
        </node>
        <node id="n37">
            <attr name="layout">
                <string>5309 755 432 40</string>
            </attr>
        </node>
        <node id="n38">
            <attr name="layout">
                <string>5343 835 364 80</string>
            </attr>
        </node>
        <node id="n39">
            <attr name="layout">
                <string>5616 835 218 80</string>
            </attr>
        </node>
        <node id="n40">
            <attr name="layout">
                <string>5788 655 173 40</string>
            </attr>
        </node>
        <node id="n41">
            <attr name="layout">
                <string>6096 595 257 160</string>
            </attr>
        </node>
        <node id="n42">
            <attr name="layout">
                <string>6099 735 251 80</string>
            </attr>
        </node>
        <node id="n43">
            <attr name="layout">
                <string>6488 655 173 40</string>
            </attr>
        </node>
        <node id="n44">
            <attr name="layout">
                <string>6745 595 359 160</string>
            </attr>
        </node>
        <node id="n45">
            <attr name="layout">
                <string>6771 735 307 80</string>
            </attr>
        </node>
        <node id="n46">
            <attr name="layout">
                <string>7053 735 144 80</string>
            </attr>
        </node>
        <node id="n47">
            <attr name="layout">
                <string>7016 835 218 80</string>
            </attr>
        </node>
        <node id="n48">
            <attr name="layout">
                <string>7253 735 144 80</string>
            </attr>
        </node>
        <node id="n49">
            <attr name="layout">
                <string>7209 835 232 80</string>
            </attr>
        </node>
        <node id="n50">
            <attr name="layout">
                <string>7453 735 144 80</string>
            </attr>
        </node>
        <node id="n51">
            <attr name="layout">
                <string>7399 835 251 80</string>
            </attr>
        </node>
        <node id="n52">
            <attr name="layout">
                <string>7653 735 144 80</string>
            </attr>
        </node>
        <node id="n53">
            <attr name="layout">
                <string>7580 835 289 80</string>
            </attr>
        </node>
        <node id="n54">
            <attr name="layout">
                <string>7188 655 173 40</string>
            </attr>
        </node>
        <node id="n55">
            <attr name="layout">
                <string>7408 635 434 80</string>
            </attr>
        </node>
        <node id="n56">
            <attr name="layout">
                <string>7513 735 224 80</string>
            </attr>
        </node>
        <node id="n57">
            <attr name="layout">
                <string>7753 735 144 80</string>
            </attr>
        </node>
        <node id="n58">
            <attr name="layout">
                <string>7671 835 307 80</string>
            </attr>
        </node>
        <node id="n59">
            <attr name="layout">
                <string>7888 655 173 40</string>
            </attr>
        </node>
        <node id="n60">
            <attr name="layout">
                <string>8199 655 251 40</string>
            </attr>
        </node>
        <node id="n61">
            <attr name="layout">
                <string>8209 735 232 80</string>
            </attr>
        </node>
        <node id="n62">
            <attr name="layout">
                <string>8399 735 251 80</string>
            </attr>
        </node>
        <node id="n63">
            <attr name="layout">
                <string>8588 655 173 40</string>
            </attr>
        </node>
        <node id="n64">
            <attr name="layout">
                <string>8899 655 251 40</string>
            </attr>
        </node>
        <node id="n65">
            <attr name="layout">
                <string>8916 735 218 80</string>
            </attr>
        </node>
        <node id="n66">
            <attr name="layout">
                <string>9115 755 219 40</string>
            </attr>
        </node>
        <node id="n67">
            <attr name="layout">
                <string>9116 735 218 80</string>
            </attr>
        </node>
        <node id="n68">
            <attr name="layout">
                <string>9113 735 223 80</string>
            </attr>
        </node>
        <node id="n69">
            <attr name="layout">
                <string>9288 655 173 40</string>
            </attr>
        </node>
        <node id="n70">
            <attr name="layout">
                <string>9652 655 146 40</string>
            </attr>
        </node>
        <node id="n71">
            <attr name="layout">
                <string>9531 755 388 40</string>
            </attr>
        </node>
        <node id="n72">
            <attr name="layout">
                <string>9988 655 173 40</string>
            </attr>
        </node>
        <node id="n73">
            <attr name="layout">
                <string>10299 655 251 40</string>
            </attr>
        </node>
        <node id="n74">
            <attr name="layout">
                <string>10299 735 251 80</string>
            </attr>
        </node>
        <node id="n75">
            <attr name="layout">
                <string>10505 735 239 80</string>
            </attr>
        </node>
        <node id="n76">
            <attr name="layout">
                <string>408 1015 434 120</string>
            </attr>
        </node>
        <node id="n77">
            <attr name="layout">
                <string>135 1015 280 120</string>
            </attr>
        </node>
        <node id="n78">
            <attr name="layout">
                <string>859 1035 231 80</string>
            </attr>
        </node>
        <node id="n79">
            <attr name="layout">
                <string>829 1135 291 80</string>
            </attr>
        </node>
        <node id="n80">
            <attr name="layout">
                <string>1257 1055 135 40</string>
            </attr>
        </node>
        <node id="n81">
            <attr name="layout">
                <string>516 1435 218 80</string>
            </attr>
        </node>
        <node id="n82">
            <attr name="layout">
                <string>490 1815 269 120</string>
            </attr>
        </node>
        <node id="n83">
            <attr name="layout">
                <string>490 2215 269 120</string>
            </attr>
        </node>
        <node id="n84">
            <attr name="layout">
                <string>467 2635 316 80</string>
            </attr>
        </node>
        <node id="n85">
            <attr name="layout">
                <string>898 2635 153 80</string>
            </attr>
        </node>
        <node id="n86">
            <attr name="layout">
                <string>830 2735 289 80</string>
            </attr>
        </node>
        <node id="n87">
            <attr name="layout">
                <string>859 2835 232 80</string>
            </attr>
        </node>
        <node id="n88">
            <attr name="layout">
                <string>849 2935 251 80</string>
            </attr>
        </node>
        <node id="n89">
            <attr name="layout">
                <string>898 3035 153 80</string>
            </attr>
        </node>
        <node id="n90">
            <attr name="layout">
                <string>830 3135 289 80</string>
            </attr>
        </node>
        <node id="n91">
            <attr name="layout">
                <string>859 3235 232 80</string>
            </attr>
        </node>
        <node id="n92">
            <attr name="layout">
                <string>849 3335 251 80</string>
            </attr>
        </node>
        <node id="n93">
            <attr name="layout">
                <string>473 3815 304 120</string>
            </attr>
        </node>
        <node id="n94">
            <attr name="layout">
                <string>117 3815 316 120</string>
            </attr>
        </node>
        <node id="n95">
            <attr name="layout">
                <string>141 3915 268 120</string>
            </attr>
        </node>
        <node id="n96">
            <attr name="layout">
                <string>141 4015 268 120</string>
            </attr>
        </node>
        <node id="n97">
            <attr name="layout">
                <string>130 4115 289 120</string>
            </attr>
        </node>
        <node id="n98">
            <attr name="layout">
                <string>813 3855 323 40</string>
            </attr>
        </node>
        <node id="n99">
            <attr name="layout">
                <string>759 3955 432 40</string>
            </attr>
        </node>
        <node id="n100">
            <attr name="layout">
                <string>817 4035 316 80</string>
            </attr>
        </node>
        <node id="n101">
            <attr name="layout">
                <string>1063 4035 223 80</string>
            </attr>
        </node>
        <node id="n102">
            <attr name="layout">
                <string>1238 3855 173 40</string>
            </attr>
        </node>
        <node id="n103">
            <attr name="layout">
                <string>1513 3855 323 40</string>
            </attr>
        </node>
        <node id="n104">
            <attr name="layout">
                <string>1459 3955 432 40</string>
            </attr>
        </node>
        <node id="n105">
            <attr name="layout">
                <string>1529 4035 291 80</string>
            </attr>
        </node>
        <node id="n106">
            <attr name="layout">
                <string>1763 4035 223 80</string>
            </attr>
        </node>
        <node id="n107">
            <attr name="layout">
                <string>1938 3855 173 40</string>
            </attr>
        </node>
        <node id="n108">
            <attr name="layout">
                <string>2249 3855 251 40</string>
            </attr>
        </node>
        <node id="n109">
            <attr name="layout">
                <string>2239 3935 272 80</string>
            </attr>
        </node>
        <node id="n110">
            <attr name="layout">
                <string>2417 3935 316 80</string>
            </attr>
        </node>
        <node id="n111">
            <attr name="layout">
                <string>2638 3855 173 40</string>
            </attr>
        </node>
        <node id="n112">
            <attr name="layout">
                <string>2949 3855 251 40</string>
            </attr>
        </node>
        <node id="n113">
            <attr name="layout">
                <string>2939 3935 272 80</string>
            </attr>
        </node>
        <node id="n114">
            <attr name="layout">
                <string>3129 3935 291 80</string>
            </attr>
        </node>
        <node id="n115">
            <attr name="layout">
                <string>3338 3855 173 40</string>
            </attr>
        </node>
        <node id="n116">
            <attr name="layout">
                <string>3649 3855 251 40</string>
            </attr>
        </node>
        <node id="n117">
            <attr name="layout">
                <string>3639 3935 272 80</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>3829 3935 291 80</string>
            </attr>
        </node>
        <node id="n119">
            <attr name="layout">
                <string>4038 3855 173 40</string>
            </attr>
        </node>
        <node id="n120">
            <attr name="layout">
                <string>4349 3855 251 40</string>
            </attr>
        </node>
        <node id="n121">
            <attr name="layout">
                <string>4330 3935 289 80</string>
            </attr>
        </node>
        <node id="n122">
            <attr name="layout">
                <string>4529 3935 291 80</string>
            </attr>
        </node>
        <node id="n123">
            <attr name="layout">
                <string>4757 3855 135 40</string>
            </attr>
        </node>
        <node id="n124">
            <attr name="layout">
                <string>473 4215 304 120</string>
            </attr>
        </node>
        <node id="n125">
            <attr name="layout">
                <string>172 4235 206 80</string>
            </attr>
        </node>
        <node id="n126">
            <attr name="layout">
                <string>849 4255 251 40</string>
            </attr>
        </node>
        <node id="n127">
            <attr name="layout">
                <string>866 4335 218 80</string>
            </attr>
        </node>
        <node id="n128">
            <attr name="layout">
                <string>1039 4335 272 80</string>
            </attr>
        </node>
        <node id="n129">
            <attr name="layout">
                <string>1257 4255 135 40</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>451 4615 348 120</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>172 4635 206 80</string>
            </attr>
        </node>
        <node id="n132">
            <attr name="layout">
                <string>849 4655 251 40</string>
            </attr>
        </node>
        <node id="n133">
            <attr name="layout">
                <string>866 4735 218 80</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>1030 4735 289 80</string>
            </attr>
        </node>
        <node id="n135">
            <attr name="layout">
                <string>1257 4655 135 40</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>473 5015 304 120</string>
            </attr>
        </node>
        <node id="n137">
            <attr name="layout">
                <string>172 5035 206 80</string>
            </attr>
        </node>
        <node id="n138">
            <attr name="layout">
                <string>849 5055 251 40</string>
            </attr>
        </node>
        <node id="n139">
            <attr name="layout">
                <string>866 5135 218 80</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>1039 5135 272 80</string>
            </attr>
        </node>
        <node id="n141">
            <attr name="layout">
                <string>1257 5055 135 40</string>
            </attr>
        </node>
        <node id="n142">
            <attr name="layout">
                <string>470 5415 310 120</string>
            </attr>
        </node>
        <node id="n143">
            <attr name="layout">
                <string>172 5435 206 80</string>
            </attr>
        </node>
        <node id="n144">
            <attr name="layout">
                <string>849 5455 251 40</string>
            </attr>
        </node>
        <node id="n145">
            <attr name="layout">
                <string>866 5535 218 80</string>
            </attr>
        </node>
        <node id="n146">
            <attr name="layout">
                <string>1039 5535 272 80</string>
            </attr>
        </node>
        <node id="n147">
            <attr name="layout">
                <string>1257 5455 135 40</string>
            </attr>
        </node>
        <node id="n148">
            <attr name="layout">
                <string>473 5815 304 120</string>
            </attr>
        </node>
        <node id="n149">
            <attr name="layout">
                <string>141 5815 268 120</string>
            </attr>
        </node>
        <node id="n150">
            <attr name="layout">
                <string>141 5915 268 120</string>
            </attr>
        </node>
        <node id="n151">
            <attr name="layout">
                <string>859 5835 231 80</string>
            </attr>
        </node>
        <node id="n152">
            <attr name="layout">
                <string>844 6058 291 80</string>
            </attr>
        </node>
        <node id="n153">
            <attr name="layout">
                <string>1238 5855 173 40</string>
            </attr>
        </node>
        <node id="n154">
            <attr name="layout">
                <string>1559 5835 231 80</string>
            </attr>
        </node>
        <node id="n155">
            <attr name="layout">
                <string>1526 6039 291 80</string>
            </attr>
        </node>
        <node id="n156">
            <attr name="layout">
                <string>1957 5855 135 40</string>
            </attr>
        </node>
        <node id="n157">
            <attr name="layout">
                <string>473 6215 304 120</string>
            </attr>
        </node>
        <node id="n158">
            <attr name="layout">
                <string>812 6235 325 80</string>
            </attr>
        </node>
        <node id="n159">
            <attr name="layout">
                <string>863 6335 224 80</string>
            </attr>
        </node>
        <node id="n160">
            <attr name="layout">
                <string>1103 6335 144 80</string>
            </attr>
        </node>
        <node id="n161">
            <attr name="layout">
                <string>1039 6435 272 80</string>
            </attr>
        </node>
        <node id="n162">
            <attr name="layout">
                <string>1257 6255 135 40</string>
            </attr>
        </node>
        <node id="n163">
            <attr name="layout">
                <string>462 6615 325 120</string>
            </attr>
        </node>
        <node id="n164">
            <attr name="layout">
                <string>141 6615 268 120</string>
            </attr>
        </node>
        <node id="n165">
            <attr name="layout">
                <string>803 6635 344 80</string>
            </attr>
        </node>
        <node id="n166">
            <attr name="layout">
                <string>863 6735 224 80</string>
            </attr>
        </node>
        <node id="n167">
            <attr name="layout">
                <string>1103 6735 144 80</string>
            </attr>
        </node>
        <node id="n168">
            <attr name="layout">
                <string>1039 6835 272 80</string>
            </attr>
        </node>
        <node id="n169">
            <attr name="layout">
                <string>1257 6655 135 40</string>
            </attr>
        </node>
        <node id="n170">
            <attr name="layout">
                <string>453 7015 344 120</string>
            </attr>
        </node>
        <node id="n171">
            <attr name="layout">
                <string>522 7212 268 120</string>
            </attr>
        </node>
        <node id="n172">
            <attr name="layout">
                <string>473 7415 304 120</string>
            </attr>
        </node>
        <node id="n173">
            <attr name="layout">
                <string>902 7455 146 40</string>
            </attr>
        </node>
        <node id="n174">
            <attr name="layout">
                <string>759 7555 432 40</string>
            </attr>
        </node>
        <node id="n175">
            <attr name="layout">
                <string>863 7635 223 80</string>
            </attr>
        </node>
        <node id="n176">
            <attr name="layout">
                <string>1030 7635 289 80</string>
            </attr>
        </node>
        <node id="n177">
            <attr name="layout">
                <string>1257 7455 135 40</string>
            </attr>
        </node>
        <node id="n178">
            <attr name="layout">
                <string>1252 7455 146 40</string>
            </attr>
        </node>
        <node id="n179">
            <attr name="layout">
                <string>1131 7555 388 40</string>
            </attr>
        </node>
        <node id="n180">
            <attr name="layout">
                <string>1588 7455 173 40</string>
            </attr>
        </node>
        <node id="n181">
            <attr name="layout">
                <string>1909 7435 231 80</string>
            </attr>
        </node>
        <node id="n182">
            <attr name="layout">
                <string>1913 7535 224 80</string>
            </attr>
        </node>
        <node id="n183">
            <attr name="layout">
                <string>2153 7535 144 80</string>
            </attr>
        </node>
        <node id="n184">
            <attr name="layout">
                <string>2089 7635 272 80</string>
            </attr>
        </node>
        <node id="n185">
            <attr name="layout">
                <string>2353 7535 144 80</string>
            </attr>
        </node>
        <node id="n186">
            <attr name="layout">
                <string>2289 7635 272 80</string>
            </attr>
        </node>
        <node id="n187">
            <attr name="layout">
                <string>2288 7455 173 40</string>
            </attr>
        </node>
        <node id="n188">
            <attr name="layout">
                <string>2599 7455 251 40</string>
            </attr>
        </node>
        <node id="n189">
            <attr name="layout">
                <string>2580 7535 289 80</string>
            </attr>
        </node>
        <node id="n190">
            <attr name="layout">
                <string>2795 7555 260 40</string>
            </attr>
        </node>
        <node id="n191">
            <attr name="layout">
                <string>2780 7535 289 80</string>
            </attr>
        </node>
        <node id="n192">
            <attr name="layout">
                <string>2813 7535 223 80</string>
            </attr>
        </node>
        <node id="n193">
            <attr name="layout">
                <string>2988 7455 173 40</string>
            </attr>
        </node>
        <node id="n194">
            <attr name="layout">
                <string>-5 15 360 120</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:name = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n1" to="n4">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n7">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n2">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n3">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n6">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n9">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n1" to="n8">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n5">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:name = "first_fork"</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:name = "philosopher_count"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:name = "a_philosopher"</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n9" to="n10">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n9" to="n12">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n9" to="n11">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>let:name = "philosopher_count"</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:value = 2</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n12" to="n13">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n13" to="n14">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n13" to="n15">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n13" to="n16">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n16" to="n17">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n17" to="n20">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n17" to="n18">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n17" to="n19">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n20" to="n21">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:template = "FORK"</string>
            </attr>
        </edge>
        <edge from="n21" to="n23">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n21" to="n22">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:name = "first_fork"</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n23" to="n24">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n24" to="n27">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n24" to="n26">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n24" to="n25">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:name = "first_fork"</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n27" to="n33">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n27" to="n28">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n28" to="n29">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n28" to="n32">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n29" to="n30">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n29" to="n31">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>let:name = "philosopher_count"</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n33" to="n35">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n33" to="n34">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n34" to="n29">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n35" to="n36">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n35" to="n70">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n36" to="n37">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n36" to="n40">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n37" to="n38">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n37" to="n39">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>let:name = "philosopher_count"</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n40" to="n41">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>let:template = "FORK"</string>
            </attr>
        </edge>
        <edge from="n41" to="n42">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n41" to="n43">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n43" to="n44">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:template = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n44" to="n48">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n44" to="n52">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n44" to="n45">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n44" to="n50">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n44" to="n46">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n44" to="n54">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>let:name = "a_philosopher"</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n46" to="n47">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n48" to="n49">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:index = 3</string>
            </attr>
        </edge>
        <edge from="n50" to="n51">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>let:index = 4</string>
            </attr>
        </edge>
        <edge from="n52" to="n53">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n54" to="n55">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>let:procedure = "launch_philosopher"</string>
            </attr>
        </edge>
        <edge from="n55" to="n59">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n55" to="n56">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n55" to="n57">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n57" to="n58">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>let:name = "a_philosopher"</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n59" to="n60">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n60" to="n63">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n60" to="n61">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n60" to="n62">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n63" to="n64">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n64" to="n65">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n64" to="n69">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n64" to="n66">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n66" to="n66">
            <attr name="label">
                <string>type:IntegerAddition</string>
            </attr>
        </edge>
        <edge from="n66" to="n68">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n66" to="n67">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n67" to="n67">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n67" to="n67">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n69" to="n69">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n69" to="n33">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n69" to="n28">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n70" to="n70">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n70" to="n71">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n70" to="n72">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n71" to="n71">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n71" to="n37">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n72" to="n72">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n72" to="n73">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n73" to="n73">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n73" to="n43">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n73" to="n75">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n73" to="n74">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>let:name = "first_fork"</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>let:procedure = "launch_philosopher"</string>
            </attr>
        </edge>
        <edge from="n76" to="n78">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n76" to="n77">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:name = "philosopher"</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n78" to="n80">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n78" to="n79">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>let:name = "philosopher"</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>let:name = "FORK"</string>
            </attr>
        </edge>
        <edge from="n82" to="n82">
            <attr name="label">
                <string>type:InitialAndFinalState</string>
            </attr>
        </edge>
        <edge from="n82" to="n82">
            <attr name="label">
                <string>let:class = "FORK"</string>
            </attr>
        </edge>
        <edge from="n82" to="n82">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>type:InitialAndFinalState</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>let:class = "FORK"</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>let:procedure = "use"</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>let:name = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n84" to="n89">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n91">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n88">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n85">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n92">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n90">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n87">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n84" to="n86">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n85" to="n85">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n85" to="n85">
            <attr name="label">
                <string>let:name = "id"</string>
            </attr>
        </edge>
        <edge from="n86" to="n86">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n86" to="n86">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>let:name = "id"</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n93" to="n98">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n93" to="n97">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n93" to="n96">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n93" to="n94">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n93" to="n95">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:name = "philosopher_id"</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>let:name = "left"</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:index = 3</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>let:name = "right"</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>let:index = 4</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n98" to="n99">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n98" to="n102">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n99" to="n100">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n99" to="n101">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:name = "philosopher_id"</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n102" to="n103">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n103" to="n104">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n103" to="n107">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n104" to="n105">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n104" to="n106">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n105" to="n105">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n105" to="n105">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n107" to="n108">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n108" to="n111">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n108" to="n109">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n108" to="n110">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>let:name = "id"</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:name = "philosopher_id"</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n111" to="n112">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n112" to="n114">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n112" to="n115">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n112" to="n113">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>let:name = "left"</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n115" to="n116">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n116" to="n117">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n116" to="n118">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n116" to="n119">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:name = "right"</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n119" to="n120">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n120" to="n122">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n120" to="n121">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n120" to="n123">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>let:procedure = "id"</string>
            </attr>
        </edge>
        <edge from="n124" to="n126">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n124" to="n125">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n126" to="n126">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n126" to="n127">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n126" to="n128">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n126" to="n129">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>let:name = "id"</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>let:procedure = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n130" to="n132">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n130" to="n131">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n132" to="n135">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n132" to="n133">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n132" to="n134">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>let:procedure = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n136" to="n138">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n136" to="n137">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n138" to="n140">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n138" to="n139">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n138" to="n141">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>let:procedure = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n142" to="n144">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n142" to="n143">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n144" to="n146">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n144" to="n145">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n144" to="n147">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n145" to="n145">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n145" to="n145">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>let:procedure = "eat"</string>
            </attr>
        </edge>
        <edge from="n148" to="n149">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n148" to="n151">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n148" to="n150">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:name = "left"</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>let:name = "right"</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>let:procedure = "use"</string>
            </attr>
        </edge>
        <edge from="n151" to="n152">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n151" to="n153">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:name = "left"</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n153" to="n154">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>let:procedure = "use"</string>
            </attr>
        </edge>
        <edge from="n154" to="n156">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n154" to="n155">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>let:name = "right"</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:procedure = "bad_eat"</string>
            </attr>
        </edge>
        <edge from="n157" to="n158">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:procedure = "pickup_left"</string>
            </attr>
        </edge>
        <edge from="n158" to="n160">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n158" to="n159">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n158" to="n162">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n159" to="n159">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n159" to="n159">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n160" to="n161">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n162" to="n162">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>let:procedure = "pickup_left"</string>
            </attr>
        </edge>
        <edge from="n163" to="n165">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n163" to="n164">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>let:name = "left"</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:procedure = "pickup_right"</string>
            </attr>
        </edge>
        <edge from="n165" to="n169">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n165" to="n166">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n165" to="n167">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n167" to="n168">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>type:InitialAndFinalState</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>let:procedure = "pickup_right"</string>
            </attr>
        </edge>
        <edge from="n170" to="n171">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n171" to="n171">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n171" to="n171">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n171" to="n171">
            <attr name="label">
                <string>let:name = "right"</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>let:class = "PHILOSOPHER"</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n172" to="n178">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n172" to="n173">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n173" to="n177">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n173" to="n174">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n174" to="n174">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n174" to="n175">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n174" to="n176">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n175" to="n175">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n175" to="n175">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n178" to="n178">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n178" to="n179">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n178" to="n180">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n179" to="n179">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n179" to="n174">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n180" to="n180">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n180" to="n181">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>let:procedure = "eat"</string>
            </attr>
        </edge>
        <edge from="n181" to="n183">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n181" to="n187">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n181" to="n185">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n181" to="n182">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n183" to="n183">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n183" to="n183">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n183" to="n184">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n185" to="n186">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n187" to="n188">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n188" to="n190">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n188" to="n193">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n188" to="n189">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>type:IntegerSubtraction</string>
            </attr>
        </edge>
        <edge from="n190" to="n191">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n190" to="n192">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>let:name = "times_to_eat"</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n193" to="n178">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n193" to="n173">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>type:Initialization</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>let:root_class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>let:root_procedure = "make"</string>
            </attr>
        </edge>
    </graph>
</gxl>
