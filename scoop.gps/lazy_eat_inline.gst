<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="lazy_eat_inline">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n7">
            <attr name="layout">
                <string>205 721 152 68</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>479 747 228 34</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>540 639 117 51</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>528 513 146 51</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>870 753 92 34</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>525 1126 220 34</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>1109 758 228 34</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>1165 632 117 51</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>1147 509 146 51</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>226 1006 92 34</string>
            </attr>
        </node>
        <node id="n24">
            <attr name="layout">
                <string>1070 1119 220 34</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>436 992 121 51</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>437 886 118 51</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>659 1003 92 34</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>894 995 121 51</string>
            </attr>
        </node>
        <node id="n28">
            <attr name="layout">
                <string>894 877 118 51</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>1138 1001 92 34</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>859 1122 92 34</string>
            </attr>
        </node>
        <node id="n32">
            <attr name="layout">
                <string>226 1131 74 34</string>
            </attr>
        </node>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:class = &quot;PHILOSOPHER&quot;</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:procedure = &quot;lazy_eat&quot;</string>
            </attr>
        </edge>
        <edge from="n7" to="n15">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:ActionStartInlineSeparateBlock</string>
            </attr>
        </edge>
        <edge from="n15" to="n16">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n15" to="n18">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n15" to="n19">
            <attr name="label">
                <string>_ends_with</string>
            </attr>
            <attr name="layout">
                <string>500 0 499 784 266 834 122 1049 526 1123 11</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:InlineParameter</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>let:name = &quot;left&quot;</string>
            </attr>
        </edge>
        <edge from="n16" to="n17">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:name = &quot;left_fork&quot;</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n18" to="n21">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:ActionEndInlineSeparateBlock</string>
            </attr>
        </edge>
        <edge from="n19" to="n32">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:ActionStartInlineSeparateBlock</string>
            </attr>
        </edge>
        <edge from="n21" to="n20">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n21" to="n23">
            <attr name="label">
                <string>to_state</string>
            </attr>
            <attr name="layout">
                <string>500 0 1104 787 428 860 291 1003 11</string>
            </attr>
        </edge>
        <edge from="n21" to="n24">
            <attr name="label">
                <string>_ends_with</string>
            </attr>
            <attr name="layout">
                <string>500 0 1294 795 1294 1021 1200 1116 11</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:InlineParameter</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:name = &quot;right&quot;</string>
            </attr>
        </edge>
        <edge from="n20" to="n22">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:name = &quot;right_fork&quot;</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n23" to="n25">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>type:ActionEndInlineSeparateBlock</string>
            </attr>
        </edge>
        <edge from="n24" to="n31">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:procedure = &quot;use&quot;</string>
            </attr>
        </edge>
        <edge from="n25" to="n27">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n25" to="n26">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:name = &quot;left&quot;</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n27" to="n29">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>let:procedure = &quot;use&quot;</string>
            </attr>
        </edge>
        <edge from="n29" to="n28">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n29" to="n30">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>let:name = &quot;right&quot;</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n30" to="n24">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n31" to="n19">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
    </graph>
</gxl>
