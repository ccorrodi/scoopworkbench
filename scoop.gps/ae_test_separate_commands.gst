<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
	<graph edgeids="false" edgemode="directed" id="debug_translation" role="graph">
		<node id="node0">
			<attr name="layout">
				<string>500 250 250 50</string>
			</attr>
		</node>
		<node id="node1">
			<attr name="layout">
				<string>500 650 250 50</string>
			</attr>
		</node>
		<node id="node2">
			<attr name="layout">
				<string>500 1050 250 50</string>
			</attr>
		</node>
		<node id="node3">
			<attr name="layout">
				<string>500 1450 250 50</string>
			</attr>
		</node>
		<node id="node4">
			<attr name="layout">
				<string>150 1450 250 50</string>
			</attr>
		</node>
		<node id="node5">
			<attr name="layout">
				<string>850 1450 250 50</string>
			</attr>
		</node>
		<node id="node6">
			<attr name="layout">
				<string>850 1550 250 50</string>
			</attr>
		</node>
		<node id="node7">
			<attr name="layout">
				<string>1050 1550 250 50</string>
			</attr>
		</node>
		<node id="node8">
			<attr name="layout">
				<string>1200 1450 250 50</string>
			</attr>
		</node>
		<node id="node9">
			<attr name="layout">
				<string>500 1850 250 50</string>
			</attr>
		</node>
		<node id="node10">
			<attr name="layout">
				<string>500 2250 250 50</string>
			</attr>
		</node>
		<node id="node11">
			<attr name="layout">
				<string>150 2250 250 50</string>
			</attr>
		</node>
		<node id="node12">
			<attr name="layout">
				<string>150 2350 250 50</string>
			</attr>
		</node>
		<node id="node13">
			<attr name="layout">
				<string>850 2250 250 50</string>
			</attr>
		</node>
		<node id="node14">
			<attr name="layout">
				<string>850 2350 250 50</string>
			</attr>
		</node>
		<node id="node15">
			<attr name="layout">
				<string>1200 2250 250 50</string>
			</attr>
		</node>
		<node id="node16">
			<attr name="layout">
				<string>1550 2250 250 50</string>
			</attr>
		</node>
		<node id="node17">
			<attr name="layout">
				<string>1550 2350 250 50</string>
			</attr>
		</node>
		<node id="node18">
			<attr name="layout">
				<string>1900 2250 250 50</string>
			</attr>
		</node>
		<node id="node19">
			<attr name="layout">
				<string>2250 2250 250 50</string>
			</attr>
		</node>
		<node id="node20">
			<attr name="layout">
				<string>2250 2350 250 50</string>
			</attr>
		</node>
		<node id="node21">
			<attr name="layout">
				<string>2450 2350 250 50</string>
			</attr>
		</node>
		<node id="node22">
			<attr name="layout">
				<string>2450 2450 250 50</string>
			</attr>
		</node>
		<node id="node23">
			<attr name="layout">
				<string>2650 2350 250 50</string>
			</attr>
		</node>
		<node id="node24">
			<attr name="layout">
				<string>2650 2450 250 50</string>
			</attr>
		</node>
		<node id="node25">
			<attr name="layout">
				<string>2600 2250 250 50</string>
			</attr>
		</node>
		<node id="node26">
			<attr name="layout">
				<string>500 2650 250 50</string>
			</attr>
		</node>
		<node id="node27">
			<attr name="layout">
				<string>150 2650 250 50</string>
			</attr>
		</node>
		<node id="node28">
			<attr name="layout">
				<string>150 2750 250 50</string>
			</attr>
		</node>
		<node id="node29">
			<attr name="layout">
				<string>850 2650 250 50</string>
			</attr>
		</node>
		<node id="node30">
			<attr name="layout">
				<string>850 2750 250 50</string>
			</attr>
		</node>
		<node id="node31">
			<attr name="layout">
				<string>1200 2650 250 50</string>
			</attr>
		</node>
		<node id="node32">
			<attr name="layout">
				<string>1550 2650 250 50</string>
			</attr>
		</node>
		<node id="node33">
			<attr name="layout">
				<string>1550 2750 250 50</string>
			</attr>
		</node>
		<node id="node34">
			<attr name="layout">
				<string>1900 2650 250 50</string>
			</attr>
		</node>
		<node id="node35">
			<attr name="layout">
				<string>50 50 250 50</string>
			</attr>
		</node>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>let:name="A"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>type:InitialAndFinalState</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>type:InitialAndFinalState</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>let:class="A"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>let:procedure="query"</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node5" to="node5">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>let:name="Result"</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node8" to="node8">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>let:name="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>let:name="a"</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>type:ActionCreate</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>let:template="A"</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>flag:separate</string>
			</attr>
		</edge>
		<edge from="node14" to="node14">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node14" to="node14">
			<attr name="label">
				<string>let:name="a"</string>
			</attr>
		</edge>
		<edge from="node15" to="node15">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>type:ActionCreate</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>let:template="A"</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>flag:separate</string>
			</attr>
		</edge>
		<edge from="node17" to="node17">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node17" to="node17">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node18" to="node18">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>type:ActionCommand</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>let:procedure="run"</string>
			</attr>
		</edge>
		<edge from="node20" to="node20">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node20" to="node20">
			<attr name="label">
				<string>let:name="Current"</string>
			</attr>
		</edge>
		<edge from="node21" to="node21">
			<attr name="label">
				<string>type:Parameter</string>
			</attr>
		</edge>
		<edge from="node21" to="node21">
			<attr name="label">
				<string>let:index=1</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>let:name="a"</string>
			</attr>
		</edge>
		<edge from="node23" to="node23">
			<attr name="label">
				<string>type:Parameter</string>
			</attr>
		</edge>
		<edge from="node23" to="node23">
			<attr name="label">
				<string>let:index=2</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node25" to="node25">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node26" to="node26">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node26" to="node26">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node26" to="node26">
			<attr name="label">
				<string>let:procedure="run"</string>
			</attr>
		</edge>
		<edge from="node27" to="node27">
			<attr name="label">
				<string>type:ParameterMapping</string>
			</attr>
		</edge>
		<edge from="node27" to="node27">
			<attr name="label">
				<string>let:index=1</string>
			</attr>
		</edge>
		<edge from="node27" to="node27">
			<attr name="label">
				<string>let:name="a"</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>type:ParameterMapping</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>let:index=2</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node29" to="node29">
			<attr name="label">
				<string>type:ActionCommand</string>
			</attr>
		</edge>
		<edge from="node29" to="node29">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node30" to="node30">
			<attr name="label">
				<string>type:ParameterExpression</string>
			</attr>
		</edge>
		<edge from="node30" to="node30">
			<attr name="label">
				<string>let:name="a"</string>
			</attr>
		</edge>
		<edge from="node31" to="node31">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node32" to="node32">
			<attr name="label">
				<string>type:ActionCommand</string>
			</attr>
		</edge>
		<edge from="node32" to="node32">
			<attr name="label">
				<string>let:procedure="command"</string>
			</attr>
		</edge>
		<edge from="node33" to="node33">
			<attr name="label">
				<string>type:ParameterExpression</string>
			</attr>
		</edge>
		<edge from="node33" to="node33">
			<attr name="label">
				<string>let:name="b"</string>
			</attr>
		</edge>
		<edge from="node34" to="node34">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node3" to="node4">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node3" to="node5">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node5" to="node8">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node5" to="node6">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node5" to="node7">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node10" to="node11">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node10" to="node12">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node10" to="node13">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node13" to="node14">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node13" to="node15">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node15" to="node16">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node16" to="node17">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node16" to="node18">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node18" to="node19">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node19" to="node20">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node19" to="node21">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node21" to="node22">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node19" to="node23">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node23" to="node24">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node19" to="node25">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node26" to="node27">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node26" to="node28">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node26" to="node29">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node29" to="node30">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node29" to="node31">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node31" to="node32">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node32" to="node33">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node32" to="node34">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>type:Initialization</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>let:root_class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>let:root_procedure="make"</string>
			</attr>
		</edge>
	</graph>
</gxl>
