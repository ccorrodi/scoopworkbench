<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="manual_test_qoq_initialize_root">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>1202 130 306 120</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>478 595 294 160</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>113 287 232 120</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>10 494 251 120</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>38 676 239 120</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>39 795 136 120</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>448 64 364 120</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>865 30 289 120</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>971 419 307 120</string>
            </attr>
        </node>
        <node id="n190">
            <attr name="layout">
                <string>52 49 360 160</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:name = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n1" to="n4">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n2">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n5">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n6">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n3">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n8">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n7">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:name = "left_fork"</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:name = "right_fork"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:name = "first_fork"</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:name = "i"</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:name = "philosopher_count"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:name = "round_count"</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:name = "a_philosopher"</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>type:Initialization</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>let:root_class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>let:root_procedure = "make"</string>
            </attr>
        </edge>
    </graph>
</gxl>
