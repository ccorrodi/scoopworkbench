<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="manual_dp_rq_deadlock_simplified">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n196">
            <attr name="layout">
                <string>638 897 171 68</string>
            </attr>
        </node>
        <node id="n197">
            <attr name="layout">
                <string>303 892 143 68</string>
            </attr>
        </node>
        <node id="n294">
            <attr name="layout">
                <string>1187 281 116 34</string>
            </attr>
        </node>
        <node id="n295">
            <attr name="layout">
                <string>817 403 85 34</string>
            </attr>
        </node>
        <node id="n297">
            <attr name="layout">
                <string>1020 405 73 34</string>
            </attr>
        </node>
        <node id="n298">
            <attr name="layout">
                <string>1014 68 90 51</string>
            </attr>
        </node>
        <node id="n301">
            <attr name="layout">
                <string>1022 251 59 34</string>
            </attr>
        </node>
        <node id="n319">
            <attr name="layout">
                <string>192 322 116 34</string>
            </attr>
        </node>
        <node id="n320">
            <attr name="layout">
                <string>542 396 85 34</string>
            </attr>
        </node>
        <node id="n322">
            <attr name="layout">
                <string>395 391 73 34</string>
            </attr>
        </node>
        <node id="n323">
            <attr name="layout">
                <string>396 126 90 51</string>
            </attr>
        </node>
        <node id="n326">
            <attr name="layout">
                <string>402 274 59 34</string>
            </attr>
        </node>
        <node id="n333">
            <attr name="layout">
                <string>345 701 85 34</string>
            </attr>
        </node>
        <node id="n335">
            <attr name="layout">
                <string>533 701 73 34</string>
            </attr>
        </node>
        <node id="n339">
            <attr name="layout">
                <string>401 603 59 34</string>
            </attr>
        </node>
        <node id="n391">
            <attr name="layout">
                <string>970 716 85 34</string>
            </attr>
        </node>
        <node id="n393">
            <attr name="layout">
                <string>788 710 73 34</string>
            </attr>
        </node>
        <node id="n397">
            <attr name="layout">
                <string>1014 590 59 34</string>
            </attr>
        </node>
        <node id="n453">
            <attr name="layout">
                <string>539 564 88 34</string>
            </attr>
        </node>
        <node id="n458">
            <attr name="layout">
                <string>208 609 88 34</string>
            </attr>
        </node>
        <node id="n459">
            <attr name="layout">
                <string>210 451 95 51</string>
            </attr>
        </node>
        <node id="n493">
            <attr name="layout">
                <string>786 551 88 34</string>
            </attr>
        </node>
        <node id="n498">
            <attr name="layout">
                <string>1193 584 88 34</string>
            </attr>
        </node>
        <node id="n499">
            <attr name="layout">
                <string>1198 449 95 51</string>
            </attr>
        </node>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>type:InitialAndFinalState</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:class = string:&quot;PHILOSOPHER&quot;</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:procedure = string:&quot;pickup_right&quot;</string>
            </attr>
        </edge>
        <edge from="n196" to="n197">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:index = int:1</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:name = string:&quot;right&quot;</string>
            </attr>
        </edge>
        <edge from="n294" to="n294">
            <attr name="label">
                <string>type:ReferenceValue</string>
            </attr>
        </edge>
        <edge from="n294" to="n298">
            <attr name="label">
                <string>refers_to</string>
            </attr>
        </edge>
        <edge from="n295" to="n295">
            <attr name="label">
                <string>type:WorkQueue</string>
            </attr>
        </edge>
        <edge from="n297" to="n297">
            <attr name="label">
                <string>type:Processor</string>
            </attr>
        </edge>
        <edge from="n297" to="n301">
            <attr name="label">
                <string>storage</string>
            </attr>
        </edge>
        <edge from="n297" to="n295">
            <attr name="label">
                <string>sync</string>
            </attr>
        </edge>
        <edge from="n298" to="n298">
            <attr name="label">
                <string>type:Object</string>
            </attr>
        </edge>
        <edge from="n298" to="n298">
            <attr name="label">
                <string>let:type = string:&quot;FORK&quot;</string>
            </attr>
        </edge>
        <edge from="n298" to="n301">
            <attr name="label">
                <string>handler</string>
            </attr>
        </edge>
        <edge from="n301" to="n301">
            <attr name="label">
                <string>type:Memory</string>
            </attr>
        </edge>
        <edge from="n319" to="n319">
            <attr name="label">
                <string>type:ReferenceValue</string>
            </attr>
        </edge>
        <edge from="n319" to="n323">
            <attr name="label">
                <string>refers_to</string>
            </attr>
        </edge>
        <edge from="n320" to="n320">
            <attr name="label">
                <string>type:WorkQueue</string>
            </attr>
        </edge>
        <edge from="n322" to="n322">
            <attr name="label">
                <string>type:Processor</string>
            </attr>
        </edge>
        <edge from="n322" to="n326">
            <attr name="label">
                <string>storage</string>
            </attr>
        </edge>
        <edge from="n322" to="n320">
            <attr name="label">
                <string>sync</string>
            </attr>
        </edge>
        <edge from="n323" to="n323">
            <attr name="label">
                <string>type:Object</string>
            </attr>
        </edge>
        <edge from="n323" to="n323">
            <attr name="label">
                <string>let:type = string:&quot;FORK&quot;</string>
            </attr>
        </edge>
        <edge from="n323" to="n326">
            <attr name="label">
                <string>handler</string>
            </attr>
        </edge>
        <edge from="n326" to="n326">
            <attr name="label">
                <string>type:Memory</string>
            </attr>
        </edge>
        <edge from="n333" to="n333">
            <attr name="label">
                <string>type:WorkQueue</string>
            </attr>
        </edge>
        <edge from="n335" to="n335">
            <attr name="label">
                <string>type:Processor</string>
            </attr>
        </edge>
        <edge from="n335" to="n196">
            <attr name="label">
                <string>_current_state_before_reservations</string>
            </attr>
        </edge>
        <edge from="n335" to="n333">
            <attr name="label">
                <string>sync</string>
            </attr>
        </edge>
        <edge from="n335" to="n339">
            <attr name="label">
                <string>storage</string>
            </attr>
        </edge>
        <edge from="n335" to="n453">
            <attr name="label">
                <string>_lock</string>
            </attr>
        </edge>
        <edge from="n339" to="n339">
            <attr name="label">
                <string>type:Memory</string>
            </attr>
        </edge>
        <edge from="n339" to="n458">
            <attr name="label">
                <string>active_frame</string>
            </attr>
        </edge>
        <edge from="n391" to="n391">
            <attr name="label">
                <string>type:WorkQueue</string>
            </attr>
        </edge>
        <edge from="n393" to="n393">
            <attr name="label">
                <string>type:Processor</string>
            </attr>
        </edge>
        <edge from="n393" to="n397">
            <attr name="label">
                <string>storage</string>
            </attr>
        </edge>
        <edge from="n393" to="n391">
            <attr name="label">
                <string>sync</string>
            </attr>
        </edge>
        <edge from="n393" to="n196">
            <attr name="label">
                <string>_current_state_before_reservations</string>
            </attr>
        </edge>
        <edge from="n393" to="n493">
            <attr name="label">
                <string>_lock</string>
            </attr>
        </edge>
        <edge from="n397" to="n397">
            <attr name="label">
                <string>type:Memory</string>
            </attr>
        </edge>
        <edge from="n397" to="n498">
            <attr name="label">
                <string>active_frame</string>
            </attr>
        </edge>
        <edge from="n453" to="n453">
            <attr name="label">
                <string>type:_QueueLock</string>
            </attr>
        </edge>
        <edge from="n453" to="n295">
            <attr name="label">
                <string>_queue</string>
            </attr>
        </edge>
        <edge from="n458" to="n458">
            <attr name="label">
                <string>type:StackFrame</string>
            </attr>
        </edge>
        <edge from="n458" to="n459">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n459" to="n459">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n459" to="n459">
            <attr name="label">
                <string>let:name = string:&quot;right&quot;</string>
            </attr>
        </edge>
        <edge from="n459" to="n319">
            <attr name="label">
                <string>value</string>
            </attr>
        </edge>
        <edge from="n493" to="n493">
            <attr name="label">
                <string>type:_QueueLock</string>
            </attr>
        </edge>
        <edge from="n493" to="n320">
            <attr name="label">
                <string>_queue</string>
            </attr>
        </edge>
        <edge from="n498" to="n498">
            <attr name="label">
                <string>type:StackFrame</string>
            </attr>
        </edge>
        <edge from="n498" to="n499">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n499" to="n499">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n499" to="n499">
            <attr name="label">
                <string>let:name = string:&quot;right&quot;</string>
            </attr>
        </edge>
        <edge from="n499" to="n294">
            <attr name="label">
                <string>value</string>
            </attr>
        </edge>
    </graph>
</gxl>
