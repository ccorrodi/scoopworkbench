<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="paper_producer_consumer_5">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>472 235 306 80</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>810 235 330 80</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>805 335 339 80</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>851 435 248 80</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>840 535 270 80</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>810 635 330 80</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>805 735 339 80</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>851 835 248 80</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>840 935 270 80</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>478 1415 294 120</string>
            </attr>
        </node>
        <node id="n10">
            <attr name="layout">
                <string>849 1455 251 40</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>839 1535 272 80</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>1063 1535 223 80</string>
            </attr>
        </node>
        <node id="n13">
            <attr name="layout">
                <string>1238 1455 173 40</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>1511 1395 327 160</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>1539 1535 272 80</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>1938 1455 173 40</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>2216 1395 317 160</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>2210 1535 330 80</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>2503 1535 144 80</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>2439 1635 272 80</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>2703 1535 144 80</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>2639 1635 272 80</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>2638 1455 173 40</string>
            </attr>
        </node>
        <node id="n24">
            <attr name="layout">
                <string>2911 1395 327 160</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>2905 1535 339 80</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>3203 1535 144 80</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>3139 1635 272 80</string>
            </attr>
        </node>
        <node id="n28">
            <attr name="layout">
                <string>3403 1535 144 80</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>3339 1635 272 80</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>3338 1455 173 40</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>3659 1435 231 80</string>
            </attr>
        </node>
        <node id="n32">
            <attr name="layout">
                <string>3663 1535 224 80</string>
            </attr>
        </node>
        <node id="n33">
            <attr name="layout">
                <string>3903 1535 144 80</string>
            </attr>
        </node>
        <node id="n34">
            <attr name="layout">
                <string>3810 1635 330 80</string>
            </attr>
        </node>
        <node id="n35">
            <attr name="layout">
                <string>4103 1535 144 80</string>
            </attr>
        </node>
        <node id="n36">
            <attr name="layout">
                <string>4005 1635 339 80</string>
            </attr>
        </node>
        <node id="n37">
            <attr name="layout">
                <string>4057 1455 135 40</string>
            </attr>
        </node>
        <node id="n38">
            <attr name="layout">
                <string>425 1798 389 120</string>
            </attr>
        </node>
        <node id="n39">
            <attr name="layout">
                <string>172 1835 206 80</string>
            </attr>
        </node>
        <node id="n40">
            <attr name="layout">
                <string>849 1855 251 40</string>
            </attr>
        </node>
        <node id="n41">
            <attr name="layout">
                <string>866 1935 218 80</string>
            </attr>
        </node>
        <node id="n42">
            <attr name="layout">
                <string>1010 1935 330 80</string>
            </attr>
        </node>
        <node id="n43">
            <attr name="layout">
                <string>1257 1855 135 40</string>
            </attr>
        </node>
        <node id="n44">
            <attr name="layout">
                <string>426 2215 398 120</string>
            </attr>
        </node>
        <node id="n45">
            <attr name="layout">
                <string>172 2235 206 80</string>
            </attr>
        </node>
        <node id="n46">
            <attr name="layout">
                <string>849 2255 251 40</string>
            </attr>
        </node>
        <node id="n47">
            <attr name="layout">
                <string>866 2335 218 80</string>
            </attr>
        </node>
        <node id="n48">
            <attr name="layout">
                <string>1005 2335 339 80</string>
            </attr>
        </node>
        <node id="n49">
            <attr name="layout">
                <string>1257 2255 135 40</string>
            </attr>
        </node>
        <node id="n50">
            <attr name="layout">
                <string>471 2615 307 120</string>
            </attr>
        </node>
        <node id="n51">
            <attr name="layout">
                <string>172 2635 206 80</string>
            </attr>
        </node>
        <node id="n52">
            <attr name="layout">
                <string>849 2655 251 40</string>
            </attr>
        </node>
        <node id="n53">
            <attr name="layout">
                <string>866 2735 218 80</string>
            </attr>
        </node>
        <node id="n54">
            <attr name="layout">
                <string>1039 2735 272 80</string>
            </attr>
        </node>
        <node id="n55">
            <attr name="layout">
                <string>1257 2655 135 40</string>
            </attr>
        </node>
        <node id="n56">
            <attr name="layout">
                <string>460 3015 329 120</string>
            </attr>
        </node>
        <node id="n57">
            <attr name="layout">
                <string>172 3035 206 80</string>
            </attr>
        </node>
        <node id="n58">
            <attr name="layout">
                <string>849 3055 251 40</string>
            </attr>
        </node>
        <node id="n59">
            <attr name="layout">
                <string>866 3135 218 80</string>
            </attr>
        </node>
        <node id="n60">
            <attr name="layout">
                <string>1039 3135 272 80</string>
            </attr>
        </node>
        <node id="n61">
            <attr name="layout">
                <string>1257 3055 135 40</string>
            </attr>
        </node>
        <node id="n62">
            <attr name="layout">
                <string>478 3415 294 120</string>
            </attr>
        </node>
        <node id="n63">
            <attr name="layout">
                <string>141 3415 268 120</string>
            </attr>
        </node>
        <node id="n64">
            <attr name="layout">
                <string>141 3515 268 120</string>
            </attr>
        </node>
        <node id="n65">
            <attr name="layout">
                <string>859 3435 231 80</string>
            </attr>
        </node>
        <node id="n66">
            <attr name="layout">
                <string>829 3535 291 80</string>
            </attr>
        </node>
        <node id="n67">
            <attr name="layout">
                <string>1238 3455 173 40</string>
            </attr>
        </node>
        <node id="n68">
            <attr name="layout">
                <string>1559 3435 231 80</string>
            </attr>
        </node>
        <node id="n69">
            <attr name="layout">
                <string>1529 3535 291 80</string>
            </attr>
        </node>
        <node id="n70">
            <attr name="layout">
                <string>1957 3455 135 40</string>
            </attr>
        </node>
        <node id="n71">
            <attr name="layout">
                <string>483 3835 284 80</string>
            </attr>
        </node>
        <node id="n72">
            <attr name="layout">
                <string>840 3835 270 80</string>
            </attr>
        </node>
        <node id="n73">
            <attr name="layout">
                <string>851 3935 248 80</string>
            </attr>
        </node>
        <node id="n74">
            <attr name="layout">
                <string>801 4035 348 80</string>
            </attr>
        </node>
        <node id="n75">
            <attr name="layout">
                <string>840 4135 270 80</string>
            </attr>
        </node>
        <node id="n76">
            <attr name="layout">
                <string>851 4235 248 80</string>
            </attr>
        </node>
        <node id="n77">
            <attr name="layout">
                <string>801 4335 348 80</string>
            </attr>
        </node>
        <node id="n78">
            <attr name="layout">
                <string>489 4815 272 120</string>
            </attr>
        </node>
        <node id="n79">
            <attr name="layout">
                <string>126 4815 297 120</string>
            </attr>
        </node>
        <node id="n80">
            <attr name="layout">
                <string>137 4915 275 120</string>
            </attr>
        </node>
        <node id="n81">
            <attr name="layout">
                <string>813 4855 323 40</string>
            </attr>
        </node>
        <node id="n82">
            <attr name="layout">
                <string>759 4955 432 40</string>
            </attr>
        </node>
        <node id="n83">
            <attr name="layout">
                <string>826 5035 297 80</string>
            </attr>
        </node>
        <node id="n84">
            <attr name="layout">
                <string>1063 5035 223 80</string>
            </attr>
        </node>
        <node id="n85">
            <attr name="layout">
                <string>1238 4855 173 40</string>
            </attr>
        </node>
        <node id="n86">
            <attr name="layout">
                <string>1549 4855 251 40</string>
            </attr>
        </node>
        <node id="n87">
            <attr name="layout">
                <string>1539 4935 272 80</string>
            </attr>
        </node>
        <node id="n88">
            <attr name="layout">
                <string>1729 4935 291 80</string>
            </attr>
        </node>
        <node id="n89">
            <attr name="layout">
                <string>1938 4855 173 40</string>
            </attr>
        </node>
        <node id="n90">
            <attr name="layout">
                <string>2249 4855 251 40</string>
            </attr>
        </node>
        <node id="n91">
            <attr name="layout">
                <string>2239 4935 272 80</string>
            </attr>
        </node>
        <node id="n92">
            <attr name="layout">
                <string>2426 4935 297 80</string>
            </attr>
        </node>
        <node id="n93">
            <attr name="layout">
                <string>2657 4855 135 40</string>
            </attr>
        </node>
        <node id="n94">
            <attr name="layout">
                <string>460 5215 329 120</string>
            </attr>
        </node>
        <node id="n95">
            <attr name="layout">
                <string>172 5235 206 80</string>
            </attr>
        </node>
        <node id="n96">
            <attr name="layout">
                <string>849 5255 251 40</string>
            </attr>
        </node>
        <node id="n97">
            <attr name="layout">
                <string>866 5335 218 80</string>
            </attr>
        </node>
        <node id="n98">
            <attr name="layout">
                <string>1039 5335 272 80</string>
            </attr>
        </node>
        <node id="n99">
            <attr name="layout">
                <string>1257 5255 135 40</string>
            </attr>
        </node>
        <node id="n100">
            <attr name="layout">
                <string>489 5615 272 120</string>
            </attr>
        </node>
        <node id="n101">
            <attr name="layout">
                <string>902 5655 146 40</string>
            </attr>
        </node>
        <node id="n102">
            <attr name="layout">
                <string>759 5755 432 40</string>
            </attr>
        </node>
        <node id="n103">
            <attr name="layout">
                <string>863 5835 223 80</string>
            </attr>
        </node>
        <node id="n104">
            <attr name="layout">
                <string>1039 5835 272 80</string>
            </attr>
        </node>
        <node id="n105">
            <attr name="layout">
                <string>1257 5655 135 40</string>
            </attr>
        </node>
        <node id="n106">
            <attr name="layout">
                <string>1252 5655 146 40</string>
            </attr>
        </node>
        <node id="n107">
            <attr name="layout">
                <string>1131 5755 388 40</string>
            </attr>
        </node>
        <node id="n108">
            <attr name="layout">
                <string>1588 5655 173 40</string>
            </attr>
        </node>
        <node id="n109">
            <attr name="layout">
                <string>1874 5635 302 80</string>
            </attr>
        </node>
        <node id="n110">
            <attr name="layout">
                <string>1913 5735 224 80</string>
            </attr>
        </node>
        <node id="n111">
            <attr name="layout">
                <string>2153 5735 144 80</string>
            </attr>
        </node>
        <node id="n112">
            <attr name="layout">
                <string>2089 5835 272 80</string>
            </attr>
        </node>
        <node id="n113">
            <attr name="layout">
                <string>2288 5655 173 40</string>
            </attr>
        </node>
        <node id="n114">
            <attr name="layout">
                <string>2599 5655 251 40</string>
            </attr>
        </node>
        <node id="n115">
            <attr name="layout">
                <string>2589 5735 272 80</string>
            </attr>
        </node>
        <node id="n116">
            <attr name="layout">
                <string>2795 5755 260 40</string>
            </attr>
        </node>
        <node id="n117">
            <attr name="layout">
                <string>2789 5735 272 80</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>2813 5735 223 80</string>
            </attr>
        </node>
        <node id="n119">
            <attr name="layout">
                <string>2988 5655 173 40</string>
            </attr>
        </node>
        <node id="n120">
            <attr name="layout">
                <string>474 6015 302 120</string>
            </attr>
        </node>
        <node id="n121">
            <attr name="layout">
                <string>137 6015 275 120</string>
            </attr>
        </node>
        <node id="n122">
            <attr name="layout">
                <string>813 6055 323 40</string>
            </attr>
        </node>
        <node id="n123">
            <attr name="layout">
                <string>825 6135 300 80</string>
            </attr>
        </node>
        <node id="n124">
            <attr name="layout">
                <string>829 6235 291 80</string>
            </attr>
        </node>
        <node id="n125">
            <attr name="layout">
                <string>1257 6055 173 40</string>
            </attr>
        </node>
        <node id="n126">
            <attr name="layout">
                <string>1549 6055 251 40</string>
            </attr>
        </node>
        <node id="n127">
            <attr name="layout">
                <string>1501 6135 348 80</string>
            </attr>
        </node>
        <node id="n128">
            <attr name="layout">
                <string>1752 6135 245 80</string>
            </attr>
        </node>
        <node id="n129">
            <attr name="layout">
                <string>1729 6235 291 80</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>1938 6055 173 40</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>2233 6035 284 80</string>
            </attr>
        </node>
        <node id="n132">
            <attr name="layout">
                <string>2229 6135 291 80</string>
            </attr>
        </node>
        <node id="n133">
            <attr name="layout">
                <string>2657 6055 135 40</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>471 6415 307 120</string>
            </attr>
        </node>
        <node id="n135">
            <attr name="layout">
                <string>172 6435 206 80</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>849 6455 251 40</string>
            </attr>
        </node>
        <node id="n137">
            <attr name="layout">
                <string>866 6535 218 80</string>
            </attr>
        </node>
        <node id="n138">
            <attr name="layout">
                <string>1039 6535 272 80</string>
            </attr>
        </node>
        <node id="n139">
            <attr name="layout">
                <string>1257 6455 135 40</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>421 6815 407 120</string>
            </attr>
        </node>
        <node id="n141">
            <attr name="layout">
                <string>172 6835 206 80</string>
            </attr>
        </node>
        <node id="n142">
            <attr name="layout">
                <string>849 6855 251 40</string>
            </attr>
        </node>
        <node id="n143">
            <attr name="layout">
                <string>866 6935 218 80</string>
            </attr>
        </node>
        <node id="n144">
            <attr name="layout">
                <string>1001 6935 348 80</string>
            </attr>
        </node>
        <node id="n145">
            <attr name="layout">
                <string>1257 6855 135 40</string>
            </attr>
        </node>
        <node id="n146">
            <attr name="layout">
                <string>483 7235 284 80</string>
            </attr>
        </node>
        <node id="n147">
            <attr name="layout">
                <string>882 7235 186 80</string>
            </attr>
        </node>
        <node id="n148">
            <attr name="layout">
                <string>882 7335 186 80</string>
            </attr>
        </node>
        <node id="n149">
            <attr name="layout">
                <string>489 7815 272 120</string>
            </attr>
        </node>
        <node id="n150">
            <attr name="layout">
                <string>849 7855 251 40</string>
            </attr>
        </node>
        <node id="n151">
            <attr name="layout">
                <string>839 7935 272 80</string>
            </attr>
        </node>
        <node id="n152">
            <attr name="layout">
                <string>1063 7935 223 80</string>
            </attr>
        </node>
        <node id="n153">
            <attr name="layout">
                <string>1257 7855 135 40</string>
            </attr>
        </node>
        <node id="n154">
            <attr name="layout">
                <string>489 8215 272 120</string>
            </attr>
        </node>
        <node id="n155">
            <attr name="layout">
                <string>172 8235 206 80</string>
            </attr>
        </node>
        <node id="n156">
            <attr name="layout">
                <string>849 8255 251 40</string>
            </attr>
        </node>
        <node id="n157">
            <attr name="layout">
                <string>866 8335 218 80</string>
            </attr>
        </node>
        <node id="n158">
            <attr name="layout">
                <string>1039 8335 272 80</string>
            </attr>
        </node>
        <node id="n159">
            <attr name="layout">
                <string>1257 8255 135 40</string>
            </attr>
        </node>
        <node id="n160">
            <attr name="layout">
                <string>475 8615 300 120</string>
            </attr>
        </node>
        <node id="n161">
            <attr name="layout">
                <string>172 8635 206 80</string>
            </attr>
        </node>
        <node id="n162">
            <attr name="layout">
                <string>849 8655 251 40</string>
            </attr>
        </node>
        <node id="n163">
            <attr name="layout">
                <string>866 8735 218 80</string>
            </attr>
        </node>
        <node id="n164">
            <attr name="layout">
                <string>959 8755 432 40</string>
            </attr>
        </node>
        <node id="n165">
            <attr name="layout">
                <string>1039 8835 272 80</string>
            </attr>
        </node>
        <node id="n166">
            <attr name="layout">
                <string>1263 8835 223 80</string>
            </attr>
        </node>
        <node id="n167">
            <attr name="layout">
                <string>1257 8655 135 40</string>
            </attr>
        </node>
        <node id="n168">
            <attr name="layout">
                <string>489 9015 272 120</string>
            </attr>
        </node>
        <node id="n169">
            <attr name="layout">
                <string>141 9015 268 120</string>
            </attr>
        </node>
        <node id="n170">
            <attr name="layout">
                <string>813 9055 323 40</string>
            </attr>
        </node>
        <node id="n171">
            <attr name="layout">
                <string>759 9155 432 40</string>
            </attr>
        </node>
        <node id="n172">
            <attr name="layout">
                <string>829 9235 291 80</string>
            </attr>
        </node>
        <node id="n173">
            <attr name="layout">
                <string>1063 9235 223 80</string>
            </attr>
        </node>
        <node id="n174">
            <attr name="layout">
                <string>1238 9055 173 40</string>
            </attr>
        </node>
        <node id="n175">
            <attr name="layout">
                <string>1513 9055 323 40</string>
            </attr>
        </node>
        <node id="n176">
            <attr name="layout">
                <string>1481 9155 388 40</string>
            </attr>
        </node>
        <node id="n177">
            <attr name="layout">
                <string>1525 9135 300 80</string>
            </attr>
        </node>
        <node id="n178">
            <attr name="layout">
                <string>1563 9235 224 80</string>
            </attr>
        </node>
        <node id="n179">
            <attr name="layout">
                <string>1938 9055 173 40</string>
            </attr>
        </node>
        <node id="n180">
            <attr name="layout">
                <string>2249 9055 251 40</string>
            </attr>
        </node>
        <node id="n181">
            <attr name="layout">
                <string>2239 9135 272 80</string>
            </attr>
        </node>
        <node id="n182">
            <attr name="layout">
                <string>2429 9135 291 80</string>
            </attr>
        </node>
        <node id="n183">
            <attr name="layout">
                <string>2657 9055 135 40</string>
            </attr>
        </node>
        <node id="n184">
            <attr name="layout">
                <string>483 9415 284 120</string>
            </attr>
        </node>
        <node id="n185">
            <attr name="layout">
                <string>813 9455 323 40</string>
            </attr>
        </node>
        <node id="n186">
            <attr name="layout">
                <string>825 9535 300 80</string>
            </attr>
        </node>
        <node id="n187">
            <attr name="layout">
                <string>863 9635 224 80</string>
            </attr>
        </node>
        <node id="n188">
            <attr name="layout">
                <string>1238 9455 173 40</string>
            </attr>
        </node>
        <node id="n189">
            <attr name="layout">
                <string>1549 9455 251 40</string>
            </attr>
        </node>
        <node id="n190">
            <attr name="layout">
                <string>1539 9535 272 80</string>
            </attr>
        </node>
        <node id="n191">
            <attr name="layout">
                <string>1763 9535 223 80</string>
            </attr>
        </node>
        <node id="n192">
            <attr name="layout">
                <string>1957 9455 135 40</string>
            </attr>
        </node>
        <node id="n193">
            <attr name="layout">
                <string>488 9835 274 80</string>
            </attr>
        </node>
        <node id="n194">
            <attr name="layout">
                <string>840 9835 270 80</string>
            </attr>
        </node>
        <node id="n195">
            <attr name="layout">
                <string>851 9935 248 80</string>
            </attr>
        </node>
        <node id="n196">
            <attr name="layout">
                <string>877 10035 196 80</string>
            </attr>
        </node>
        <node id="n197">
            <attr name="layout">
                <string>840 10135 270 80</string>
            </attr>
        </node>
        <node id="n198">
            <attr name="layout">
                <string>851 10235 248 80</string>
            </attr>
        </node>
        <node id="n199">
            <attr name="layout">
                <string>877 10335 196 80</string>
            </attr>
        </node>
        <node id="n200">
            <attr name="layout">
                <string>494 10815 262 120</string>
            </attr>
        </node>
        <node id="n201">
            <attr name="layout">
                <string>126 10815 297 120</string>
            </attr>
        </node>
        <node id="n202">
            <attr name="layout">
                <string>137 10915 275 120</string>
            </attr>
        </node>
        <node id="n203">
            <attr name="layout">
                <string>813 10855 323 40</string>
            </attr>
        </node>
        <node id="n204">
            <attr name="layout">
                <string>759 10955 432 40</string>
            </attr>
        </node>
        <node id="n205">
            <attr name="layout">
                <string>826 11035 297 80</string>
            </attr>
        </node>
        <node id="n206">
            <attr name="layout">
                <string>1063 11035 223 80</string>
            </attr>
        </node>
        <node id="n207">
            <attr name="layout">
                <string>1238 10855 173 40</string>
            </attr>
        </node>
        <node id="n208">
            <attr name="layout">
                <string>1549 10855 251 40</string>
            </attr>
        </node>
        <node id="n209">
            <attr name="layout">
                <string>1539 10935 272 80</string>
            </attr>
        </node>
        <node id="n210">
            <attr name="layout">
                <string>1726 10935 297 80</string>
            </attr>
        </node>
        <node id="n211">
            <attr name="layout">
                <string>1938 10855 173 40</string>
            </attr>
        </node>
        <node id="n212">
            <attr name="layout">
                <string>2249 10855 251 40</string>
            </attr>
        </node>
        <node id="n213">
            <attr name="layout">
                <string>2239 10935 272 80</string>
            </attr>
        </node>
        <node id="n214">
            <attr name="layout">
                <string>2429 10935 291 80</string>
            </attr>
        </node>
        <node id="n215">
            <attr name="layout">
                <string>2638 10855 173 40</string>
            </attr>
        </node>
        <node id="n216">
            <attr name="layout">
                <string>2949 10855 251 40</string>
            </attr>
        </node>
        <node id="n217">
            <attr name="layout">
                <string>2939 10935 272 80</string>
            </attr>
        </node>
        <node id="n218">
            <attr name="layout">
                <string>3163 10935 223 80</string>
            </attr>
        </node>
        <node id="n219">
            <attr name="layout">
                <string>3357 10855 135 40</string>
            </attr>
        </node>
        <node id="n220">
            <attr name="layout">
                <string>460 11215 329 120</string>
            </attr>
        </node>
        <node id="n221">
            <attr name="layout">
                <string>172 11235 206 80</string>
            </attr>
        </node>
        <node id="n222">
            <attr name="layout">
                <string>849 11255 251 40</string>
            </attr>
        </node>
        <node id="n223">
            <attr name="layout">
                <string>866 11335 218 80</string>
            </attr>
        </node>
        <node id="n224">
            <attr name="layout">
                <string>1039 11335 272 80</string>
            </attr>
        </node>
        <node id="n225">
            <attr name="layout">
                <string>1257 11255 135 40</string>
            </attr>
        </node>
        <node id="n226">
            <attr name="layout">
                <string>494 11615 262 120</string>
            </attr>
        </node>
        <node id="n227">
            <attr name="layout">
                <string>902 11655 146 40</string>
            </attr>
        </node>
        <node id="n228">
            <attr name="layout">
                <string>759 11755 432 40</string>
            </attr>
        </node>
        <node id="n229">
            <attr name="layout">
                <string>863 11835 223 80</string>
            </attr>
        </node>
        <node id="n230">
            <attr name="layout">
                <string>1039 11835 272 80</string>
            </attr>
        </node>
        <node id="n231">
            <attr name="layout">
                <string>1257 11655 135 40</string>
            </attr>
        </node>
        <node id="n232">
            <attr name="layout">
                <string>1252 11655 146 40</string>
            </attr>
        </node>
        <node id="n233">
            <attr name="layout">
                <string>1131 11755 388 40</string>
            </attr>
        </node>
        <node id="n234">
            <attr name="layout">
                <string>1588 11655 173 40</string>
            </attr>
        </node>
        <node id="n235">
            <attr name="layout">
                <string>1878 11635 293 80</string>
            </attr>
        </node>
        <node id="n236">
            <attr name="layout">
                <string>1913 11735 224 80</string>
            </attr>
        </node>
        <node id="n237">
            <attr name="layout">
                <string>2153 11735 144 80</string>
            </attr>
        </node>
        <node id="n238">
            <attr name="layout">
                <string>2089 11835 272 80</string>
            </attr>
        </node>
        <node id="n239">
            <attr name="layout">
                <string>2288 11655 173 40</string>
            </attr>
        </node>
        <node id="n240">
            <attr name="layout">
                <string>2599 11655 251 40</string>
            </attr>
        </node>
        <node id="n241">
            <attr name="layout">
                <string>2589 11735 272 80</string>
            </attr>
        </node>
        <node id="n242">
            <attr name="layout">
                <string>2795 11755 260 40</string>
            </attr>
        </node>
        <node id="n243">
            <attr name="layout">
                <string>2789 11735 272 80</string>
            </attr>
        </node>
        <node id="n244">
            <attr name="layout">
                <string>2813 11735 223 80</string>
            </attr>
        </node>
        <node id="n245">
            <attr name="layout">
                <string>2988 11655 173 40</string>
            </attr>
        </node>
        <node id="n246">
            <attr name="layout">
                <string>478 12015 293 120</string>
            </attr>
        </node>
        <node id="n247">
            <attr name="layout">
                <string>137 12015 275 120</string>
            </attr>
        </node>
        <node id="n248">
            <attr name="layout">
                <string>813 12055 323 40</string>
            </attr>
        </node>
        <node id="n249">
            <attr name="layout">
                <string>781 12155 388 40</string>
            </attr>
        </node>
        <node id="n250">
            <attr name="layout">
                <string>825 12135 300 80</string>
            </attr>
        </node>
        <node id="n251">
            <attr name="layout">
                <string>829 12235 291 80</string>
            </attr>
        </node>
        <node id="n252">
            <attr name="layout">
                <string>1238 12055 173 40</string>
            </attr>
        </node>
        <node id="n253">
            <attr name="layout">
                <string>1549 12055 251 40</string>
            </attr>
        </node>
        <node id="n254">
            <attr name="layout">
                <string>1539 12135 272 80</string>
            </attr>
        </node>
        <node id="n255">
            <attr name="layout">
                <string>1765 12155 219 40</string>
            </attr>
        </node>
        <node id="n256">
            <attr name="layout">
                <string>1739 12135 272 80</string>
            </attr>
        </node>
        <node id="n257">
            <attr name="layout">
                <string>1763 12135 223 80</string>
            </attr>
        </node>
        <node id="n258">
            <attr name="layout">
                <string>1938 12055 173 40</string>
            </attr>
        </node>
        <node id="n259">
            <attr name="layout">
                <string>2259 12035 231 80</string>
            </attr>
        </node>
        <node id="n260">
            <attr name="layout">
                <string>2229 12135 291 80</string>
            </attr>
        </node>
        <node id="n261">
            <attr name="layout">
                <string>2503 12135 144 80</string>
            </attr>
        </node>
        <node id="n262">
            <attr name="layout">
                <string>2439 12235 272 80</string>
            </attr>
        </node>
        <node id="n263">
            <attr name="layout">
                <string>2657 12055 135 40</string>
            </attr>
        </node>
        <node id="n264">
            <attr name="layout">
                <string>471 12415 307 120</string>
            </attr>
        </node>
        <node id="n265">
            <attr name="layout">
                <string>172 12435 206 80</string>
            </attr>
        </node>
        <node id="n266">
            <attr name="layout">
                <string>849 12455 251 40</string>
            </attr>
        </node>
        <node id="n267">
            <attr name="layout">
                <string>866 12535 218 80</string>
            </attr>
        </node>
        <node id="n268">
            <attr name="layout">
                <string>1039 12535 272 80</string>
            </attr>
        </node>
        <node id="n269">
            <attr name="layout">
                <string>1257 12455 135 40</string>
            </attr>
        </node>
        <node id="n270">
            <attr name="layout">
                <string>494 12815 262 120</string>
            </attr>
        </node>
        <node id="n271">
            <attr name="layout">
                <string>172 12835 206 80</string>
            </attr>
        </node>
        <node id="n272">
            <attr name="layout">
                <string>849 12855 251 40</string>
            </attr>
        </node>
        <node id="n273">
            <attr name="layout">
                <string>866 12935 218 80</string>
            </attr>
        </node>
        <node id="n274">
            <attr name="layout">
                <string>1039 12935 272 80</string>
            </attr>
        </node>
        <node id="n275">
            <attr name="layout">
                <string>1257 12855 135 40</string>
            </attr>
        </node>
        <node id="n276">
            <attr name="layout">
                <string>-5 15 360 120</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:name = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n0" to="n1">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n5">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n4">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n3">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n6">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n2">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n7">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n8">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:name = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:name = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:name = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:name = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n9" to="n10">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n10" to="n11">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n10" to="n12">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n10" to="n13">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>let:value = 5</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n13" to="n14">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>let:template = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n14" to="n15">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n14" to="n16">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n16" to="n17">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:template = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n17" to="n18">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n17" to="n21">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n17" to="n19">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n17" to="n23">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:name = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n19" to="n20">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n21" to="n22">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n23" to="n24">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:template = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n24" to="n30">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n24" to="n26">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n24" to="n25">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n24" to="n28">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:name = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n26" to="n27">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n28" to="n29">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n30" to="n31">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>let:procedure = "run"</string>
            </attr>
        </edge>
        <edge from="n31" to="n32">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n31" to="n35">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n31" to="n33">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n31" to="n37">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n33" to="n34">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>let:name = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n35" to="n36">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>let:name = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>let:procedure = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n38" to="n40">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n38" to="n39">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n40" to="n41">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n40" to="n42">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n40" to="n43">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>let:name = "single_producer"</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:procedure = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n44" to="n45">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n44" to="n46">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n46" to="n48">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n46" to="n49">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n46" to="n47">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>let:name = "single_consumer"</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:procedure = "inventory"</string>
            </attr>
        </edge>
        <edge from="n50" to="n52">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n50" to="n51">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n52" to="n54">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n52" to="n53">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n52" to="n55">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>let:procedure = "item_count"</string>
            </attr>
        </edge>
        <edge from="n56" to="n58">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n56" to="n57">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n58" to="n60">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n58" to="n61">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n58" to="n59">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>let:class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>let:procedure = "run"</string>
            </attr>
        </edge>
        <edge from="n62" to="n64">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n62" to="n65">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n62" to="n63">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>let:name = "l_p"</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>let:name = "l_c"</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n65" to="n67">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n65" to="n66">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n66" to="n66">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n66" to="n66">
            <attr name="label">
                <string>let:name = "l_p"</string>
            </attr>
        </edge>
        <edge from="n67" to="n67">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n67" to="n68">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n68" to="n70">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n68" to="n69">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n69" to="n69">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n69" to="n69">
            <attr name="label">
                <string>let:name = "l_c"</string>
            </attr>
        </edge>
        <edge from="n70" to="n70">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n71" to="n71">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n71" to="n71">
            <attr name="label">
                <string>let:name = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n71" to="n74">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n77">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n72">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n75">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n73">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n76">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n72" to="n72">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n72" to="n72">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n73" to="n73">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n73" to="n73">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>let:name = "l_consumed_item"</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:name = "l_consumed_item"</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n78" to="n81">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n78" to="n79">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n78" to="n80">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n81" to="n85">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n81" to="n82">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n82" to="n82">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n82" to="n83">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n82" to="n84">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n85" to="n85">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n85" to="n86">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n86" to="n86">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n86" to="n88">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n86" to="n87">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n86" to="n89">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n89" to="n90">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n90" to="n93">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n90" to="n91">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n90" to="n92">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:procedure = "item_count"</string>
            </attr>
        </edge>
        <edge from="n94" to="n96">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n94" to="n95">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n96" to="n99">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n96" to="n98">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n96" to="n97">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n100" to="n101">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n100" to="n106">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n101" to="n102">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n101" to="n105">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n102" to="n104">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n102" to="n103">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n105" to="n105">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n106" to="n108">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n106" to="n107">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n107" to="n102">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n108" to="n109">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>let:procedure = "consume"</string>
            </attr>
        </edge>
        <edge from="n109" to="n113">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n109" to="n111">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n109" to="n110">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n111" to="n112">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n113" to="n114">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n114" to="n119">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n114" to="n116">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n114" to="n115">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>type:IntegerSubtraction</string>
            </attr>
        </edge>
        <edge from="n116" to="n117">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n116" to="n118">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n119" to="n101">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n119" to="n106">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:procedure = "consume"</string>
            </attr>
        </edge>
        <edge from="n120" to="n121">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n120" to="n122">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n122" to="n123">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n122" to="n125">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>let:procedure = "has_item"</string>
            </attr>
        </edge>
        <edge from="n123" to="n124">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n125" to="n126">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n126" to="n126">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n126" to="n127">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n126" to="n130">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n126" to="n128">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>let:name = "l_consumed_item"</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>let:procedure = "item"</string>
            </attr>
        </edge>
        <edge from="n128" to="n129">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n130" to="n131">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:procedure = "remove"</string>
            </attr>
        </edge>
        <edge from="n131" to="n132">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n131" to="n133">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:procedure = "inventory"</string>
            </attr>
        </edge>
        <edge from="n134" to="n135">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n134" to="n136">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n136" to="n139">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n136" to="n137">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n136" to="n138">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:class = "CONSUMER"</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:procedure = "l_consumed_item"</string>
            </attr>
        </edge>
        <edge from="n140" to="n142">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n140" to="n141">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n142" to="n144">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n142" to="n143">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n142" to="n145">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>let:name = "l_consumed_item"</string>
            </attr>
        </edge>
        <edge from="n145" to="n145">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:name = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n146" to="n147">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n146" to="n148">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:class = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n149" to="n150">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n150" to="n153">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n150" to="n151">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n150" to="n152">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>let:class = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>let:procedure = "item"</string>
            </attr>
        </edge>
        <edge from="n154" to="n155">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n154" to="n156">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n156" to="n159">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n156" to="n157">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n156" to="n158">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n159" to="n159">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:class = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:procedure = "has_item"</string>
            </attr>
        </edge>
        <edge from="n160" to="n162">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n160" to="n161">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n162" to="n162">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n162" to="n167">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n162" to="n164">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n162" to="n163">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n164" to="n165">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n164" to="n166">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:class = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:procedure = "put"</string>
            </attr>
        </edge>
        <edge from="n168" to="n170">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n168" to="n169">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:name = "a_value"</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n170" to="n174">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n170" to="n171">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n171" to="n171">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n171" to="n172">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n171" to="n173">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>let:name = "a_value"</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n174" to="n174">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n174" to="n175">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n175" to="n175">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n175" to="n179">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n175" to="n176">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n176" to="n177">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:procedure = "has_item"</string>
            </attr>
        </edge>
        <edge from="n177" to="n178">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n178" to="n178">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n178" to="n178">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n179" to="n179">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n179" to="n180">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n180" to="n180">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n180" to="n183">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n180" to="n182">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n180" to="n181">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>let:name = "a_value"</string>
            </attr>
        </edge>
        <edge from="n183" to="n183">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:class = "INVENTORY"</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:procedure = "remove"</string>
            </attr>
        </edge>
        <edge from="n184" to="n185">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n185" to="n188">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n185" to="n186">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>let:procedure = "has_item"</string>
            </attr>
        </edge>
        <edge from="n186" to="n187">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n188" to="n189">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n189" to="n192">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n189" to="n191">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n189" to="n190">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>let:name = "item"</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:name = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n193" to="n199">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n195">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n198">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n194">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n196">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n197">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n198" to="n198">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n198" to="n198">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n199" to="n199">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n199" to="n199">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>let:procedure = "make"</string>
            </attr>
        </edge>
        <edge from="n200" to="n203">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n200" to="n202">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n200" to="n201">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n203" to="n203">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n203" to="n207">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n203" to="n204">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n204" to="n204">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n204" to="n206">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n204" to="n205">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n205" to="n205">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n205" to="n205">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n206" to="n206">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n206" to="n206">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n207" to="n207">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n207" to="n208">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n208" to="n208">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n208" to="n211">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n208" to="n209">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n208" to="n210">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n209" to="n209">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n209" to="n209">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n210" to="n210">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n210" to="n210">
            <attr name="label">
                <string>let:name = "a_item_count"</string>
            </attr>
        </edge>
        <edge from="n211" to="n211">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n211" to="n212">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n212" to="n212">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n212" to="n214">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n212" to="n215">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n212" to="n213">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n213" to="n213">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n213" to="n213">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n214" to="n214">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n214" to="n214">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n215" to="n215">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n215" to="n216">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n216" to="n216">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n216" to="n217">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n216" to="n219">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n216" to="n218">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n217" to="n217">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n217" to="n217">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n218" to="n218">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n218" to="n218">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n219" to="n219">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>let:procedure = "item_count"</string>
            </attr>
        </edge>
        <edge from="n220" to="n221">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n220" to="n222">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n221" to="n221">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n221" to="n221">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n222" to="n222">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n222" to="n225">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n222" to="n223">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n222" to="n224">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n223" to="n223">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n223" to="n223">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n224" to="n224">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n224" to="n224">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n225" to="n225">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>let:procedure = "live"</string>
            </attr>
        </edge>
        <edge from="n226" to="n227">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n226" to="n232">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n227" to="n227">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n227" to="n231">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n227" to="n228">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n228" to="n228">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n228" to="n229">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n228" to="n230">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n229" to="n229">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n229" to="n229">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n230" to="n230">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n230" to="n230">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n231" to="n231">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n232" to="n232">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n232" to="n233">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n232" to="n234">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n233" to="n233">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n233" to="n228">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n234" to="n234">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n234" to="n235">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n235" to="n235">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n235" to="n235">
            <attr name="label">
                <string>let:procedure = "produce"</string>
            </attr>
        </edge>
        <edge from="n235" to="n239">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n235" to="n236">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n235" to="n237">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n236" to="n236">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n236" to="n236">
            <attr name="label">
                <string>let:name = "Current"</string>
            </attr>
        </edge>
        <edge from="n237" to="n237">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n237" to="n237">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n237" to="n238">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n238" to="n238">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n238" to="n238">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n239" to="n239">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n239" to="n240">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n240" to="n240">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n240" to="n245">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n240" to="n242">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n240" to="n241">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n242" to="n242">
            <attr name="label">
                <string>type:IntegerSubtraction</string>
            </attr>
        </edge>
        <edge from="n242" to="n243">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n242" to="n244">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n243" to="n243">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n243" to="n243">
            <attr name="label">
                <string>let:name = "item_count"</string>
            </attr>
        </edge>
        <edge from="n244" to="n244">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n244" to="n244">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n245" to="n245">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n245" to="n227">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n245" to="n232">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>let:procedure = "produce"</string>
            </attr>
        </edge>
        <edge from="n246" to="n248">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n246" to="n247">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n248" to="n248">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n248" to="n252">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n248" to="n249">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n249" to="n249">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n249" to="n250">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n250" to="n250">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n250" to="n250">
            <attr name="label">
                <string>let:procedure = "has_item"</string>
            </attr>
        </edge>
        <edge from="n250" to="n251">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n251" to="n251">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n251" to="n251">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n252" to="n252">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n252" to="n253">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n253" to="n253">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n253" to="n258">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n253" to="n255">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n253" to="n254">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n254" to="n254">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n254" to="n254">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n255" to="n255">
            <attr name="label">
                <string>type:IntegerAddition</string>
            </attr>
        </edge>
        <edge from="n255" to="n257">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n255" to="n256">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n256" to="n256">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n256" to="n256">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n257" to="n257">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n257" to="n257">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n258" to="n258">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n258" to="n259">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n259" to="n259">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n259" to="n259">
            <attr name="label">
                <string>let:procedure = "put"</string>
            </attr>
        </edge>
        <edge from="n259" to="n260">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n259" to="n261">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n259" to="n263">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n260" to="n260">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n260" to="n260">
            <attr name="label">
                <string>let:name = "a_inventory"</string>
            </attr>
        </edge>
        <edge from="n261" to="n261">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n261" to="n261">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n261" to="n262">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n262" to="n262">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n262" to="n262">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n263" to="n263">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>let:procedure = "inventory"</string>
            </attr>
        </edge>
        <edge from="n264" to="n266">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n264" to="n265">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n265" to="n265">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n265" to="n265">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n266" to="n266">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n266" to="n268">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n266" to="n267">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n266" to="n269">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n267" to="n267">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n267" to="n267">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n268" to="n268">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n268" to="n268">
            <attr name="label">
                <string>let:name = "inventory"</string>
            </attr>
        </edge>
        <edge from="n269" to="n269">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>let:class = "PRODUCER"</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>let:procedure = "value"</string>
            </attr>
        </edge>
        <edge from="n270" to="n271">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n270" to="n272">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n271" to="n271">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n271" to="n271">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n272" to="n272">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n272" to="n273">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n272" to="n274">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n272" to="n275">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n273" to="n273">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n273" to="n273">
            <attr name="label">
                <string>let:name = "Result"</string>
            </attr>
        </edge>
        <edge from="n274" to="n274">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n274" to="n274">
            <attr name="label">
                <string>let:name = "value"</string>
            </attr>
        </edge>
        <edge from="n275" to="n275">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>type:Initialization</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>let:root_class = "APPLICATION"</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>let:root_procedure = "make"</string>
            </attr>
        </edge>
    </graph>
</gxl>
