<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
	<graph edgeids="false" edgemode="directed" id="debug_translation" role="graph">
		<node id="node0">
			<attr name="layout">
				<string>500 250 250 50</string>
			</attr>
		</node>
		<node id="node1">
			<attr name="layout">
				<string>500 650 250 50</string>
			</attr>
		</node>
		<node id="node2">
			<attr name="layout">
				<string>150 650 250 50</string>
			</attr>
		</node>
		<node id="node3">
			<attr name="layout">
				<string>150 750 250 50</string>
			</attr>
		</node>
		<node id="node4">
			<attr name="layout">
				<string>150 850 250 50</string>
			</attr>
		</node>
		<node id="node5">
			<attr name="layout">
				<string>850 650 250 50</string>
			</attr>
		</node>
		<node id="node6">
			<attr name="layout">
				<string>850 750 250 50</string>
			</attr>
		</node>
		<node id="node7">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node8">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node9">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node10">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node11">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node12">
			<attr name="layout">
				<string>1200 650 250 50</string>
			</attr>
		</node>
		<node id="node13">
			<attr name="layout">
				<string>1550 650 250 50</string>
			</attr>
		</node>
		<node id="node14">
			<attr name="layout">
				<string>1550 750 250 50</string>
			</attr>
		</node>
		<node id="node15">
			<attr name="layout">
				<string>1750 750 250 50</string>
			</attr>
		</node>
		<node id="node16">
			<attr name="layout">
				<string>1900 650 250 50</string>
			</attr>
		</node>
		<node id="node17">
			<attr name="layout">
				<string>2250 650 250 50</string>
			</attr>
		</node>
		<node id="node18">
			<attr name="layout">
				<string>2250 750 250 50</string>
			</attr>
		</node>
		<node id="node19">
			<attr name="layout">
				<string>2450 750 250 50</string>
			</attr>
		</node>
		<node id="node20">
			<attr name="layout">
				<string>2600 650 250 50</string>
			</attr>
		</node>
		<node id="node21">
			<attr name="layout">
				<string>2950 650 250 50</string>
			</attr>
		</node>
		<node id="node22">
			<attr name="layout">
				<string>2950 750 250 50</string>
			</attr>
		</node>
		<node id="node23">
			<attr name="layout">
				<string>3150 750 250 50</string>
			</attr>
		</node>
		<node id="node24">
			<attr name="layout">
				<string>3150 750 250 50</string>
			</attr>
		</node>
		<node id="node25">
			<attr name="layout">
				<string>3150 750 250 50</string>
			</attr>
		</node>
		<node id="node26">
			<attr name="layout">
				<string>3300 650 250 50</string>
			</attr>
		</node>
		<node id="node27">
			<attr name="layout">
				<string>3650 650 250 50</string>
			</attr>
		</node>
		<node id="node28">
			<attr name="layout">
				<string>3650 750 250 50</string>
			</attr>
		</node>
		<node id="node29">
			<attr name="layout">
				<string>3850 750 250 50</string>
			</attr>
		</node>
		<node id="node30">
			<attr name="layout">
				<string>3850 750 250 50</string>
			</attr>
		</node>
		<node id="node31">
			<attr name="layout">
				<string>3850 750 250 50</string>
			</attr>
		</node>
		<node id="node32">
			<attr name="layout">
				<string>4000 650 250 50</string>
			</attr>
		</node>
		<node id="node33">
			<attr name="layout">
				<string>4350 650 250 50</string>
			</attr>
		</node>
		<node id="node34">
			<attr name="layout">
				<string>4350 750 250 50</string>
			</attr>
		</node>
		<node id="node35">
			<attr name="layout">
				<string>4550 750 250 50</string>
			</attr>
		</node>
		<node id="node36">
			<attr name="layout">
				<string>4550 750 250 50</string>
			</attr>
		</node>
		<node id="node37">
			<attr name="layout">
				<string>4550 750 250 50</string>
			</attr>
		</node>
		<node id="node38">
			<attr name="layout">
				<string>4700 650 250 50</string>
			</attr>
		</node>
		<node id="node39">
			<attr name="layout">
				<string>50 50 250 50</string>
			</attr>
		</node>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>let:name="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>let:name="j"</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>let:name="k"</string>
			</attr>
		</edge>
		<edge from="node5" to="node5">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>type:IntegerSubtraction</string>
			</attr>
		</edge>
		<edge from="node8" to="node8">
			<attr name="label">
				<string>type:IntegerAddition</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>let:value=2</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>let:value=3</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node13" to="node13">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node14" to="node14">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node14" to="node14">
			<attr name="label">
				<string>let:name="j"</string>
			</attr>
		</edge>
		<edge from="node15" to="node15">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node15" to="node15">
			<attr name="label">
				<string>let:value=4</string>
			</attr>
		</edge>
		<edge from="node16" to="node16">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node17" to="node17">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node18" to="node18">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node18" to="node18">
			<attr name="label">
				<string>let:name="k"</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node19" to="node19">
			<attr name="label">
				<string>let:value=5</string>
			</attr>
		</edge>
		<edge from="node20" to="node20">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node21" to="node21">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node22" to="node22">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node23" to="node23">
			<attr name="label">
				<string>type:IntegerSubtraction</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node24" to="node24">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node25" to="node25">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node25" to="node25">
			<attr name="label">
				<string>let:name="j"</string>
			</attr>
		</edge>
		<edge from="node26" to="node26">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node27" to="node27">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node28" to="node28">
			<attr name="label">
				<string>let:name="j"</string>
			</attr>
		</edge>
		<edge from="node29" to="node29">
			<attr name="label">
				<string>type:IntegerSubtraction</string>
			</attr>
		</edge>
		<edge from="node30" to="node30">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node30" to="node30">
			<attr name="label">
				<string>let:name="k"</string>
			</attr>
		</edge>
		<edge from="node31" to="node31">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node31" to="node31">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node32" to="node32">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node33" to="node33">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node34" to="node34">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node34" to="node34">
			<attr name="label">
				<string>let:name="k"</string>
			</attr>
		</edge>
		<edge from="node35" to="node35">
			<attr name="label">
				<string>type:IntegerSubtraction</string>
			</attr>
		</edge>
		<edge from="node36" to="node36">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node36" to="node36">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node37" to="node37">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node37" to="node37">
			<attr name="label">
				<string>let:name="j"</string>
			</attr>
		</edge>
		<edge from="node38" to="node38">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node1" to="node2">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node1" to="node3">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node1" to="node4">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node1" to="node5">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node5" to="node12">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node5" to="node6">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node5" to="node7">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node7" to="node8">
			<attr name="label">
				<string>left</string>
			</attr>
		</edge>
		<edge from="node7" to="node11">
			<attr name="label">
				<string>right</string>
			</attr>
		</edge>
		<edge from="node8" to="node9">
			<attr name="label">
				<string>left</string>
			</attr>
		</edge>
		<edge from="node8" to="node10">
			<attr name="label">
				<string>right</string>
			</attr>
		</edge>
		<edge from="node12" to="node13">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node13" to="node16">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node13" to="node14">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node13" to="node15">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node16" to="node17">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node17" to="node20">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node17" to="node18">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node17" to="node19">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node20" to="node21">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node21" to="node26">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node21" to="node22">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node21" to="node23">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node23" to="node24">
			<attr name="label">
				<string>left</string>
			</attr>
		</edge>
		<edge from="node23" to="node25">
			<attr name="label">
				<string>right</string>
			</attr>
		</edge>
		<edge from="node26" to="node27">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node27" to="node32">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node27" to="node28">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node27" to="node29">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node29" to="node30">
			<attr name="label">
				<string>left</string>
			</attr>
		</edge>
		<edge from="node29" to="node31">
			<attr name="label">
				<string>right</string>
			</attr>
		</edge>
		<edge from="node32" to="node33">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node33" to="node38">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node33" to="node34">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node33" to="node35">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node35" to="node36">
			<attr name="label">
				<string>left</string>
			</attr>
		</edge>
		<edge from="node35" to="node37">
			<attr name="label">
				<string>right</string>
			</attr>
		</edge>
		<edge from="node39" to="node39">
			<attr name="label">
				<string>type:Initialization</string>
			</attr>
		</edge>
		<edge from="node39" to="node39">
			<attr name="label">
				<string>let:root_class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node39" to="node39">
			<attr name="label">
				<string>let:root_procedure="make"</string>
			</attr>
		</edge>
	</graph>
</gxl>
