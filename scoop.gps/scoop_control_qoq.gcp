recipe initialize() {
	scoop_qoq_initialize_root;
}

recipe constraints() {
	scoop_constraints;
	try correctness_dining_philosophers_2_using_forks_at_the_same_time;
	try correctness_barbershop_1_chair_too_many_customers_entered;
	try correctness_dining_savages_two_at_pot_with_one_serving;
	try correctness_producer_consumer_producing_and_consuming_at_the_same_time;
	skip;
}

recipe errors() {
	try correctness_dining_philosophers_2_using_forks_at_the_same_time;
	try correctness_barbershop_1_chair_too_many_customers_entered;
	try correctness_dining_savages_two_at_pot_with_one_serving;
	try correctness_producer_consumer_producing_and_consuming_at_the_same_time;
	try scoop_qoq_error_deadlock_2;
	skip;
}

/*
Perform cleanup tasks.

This recipe may fail.
*/
recipe collect_garbage() {
	try scoop_gc_values+;
	else try scoop_gc_primitive_boolean_values+;
	else try scoop_gc_primitive_integer_values+;
	//else try scoop_qoq_gc_evaluates_to+;
	else try scoop_gc_evaluation+;
	else try scoop_gc_remove_disconnected_stack_frames+;
	else try scoop_gc_remove_disconnected_variables_ref+;
	else try scoop_gc_remove_disconnected_variables_int+;
	else try scoop_gc_remove_disconnected_variables_bool+;
	else try scoop_gc_remove_disconnected_variables_ref_keep_value+;
	else try scoop_gc_remove_disconnected_variables_int_keep_value+;
	else try scoop_gc_remove_disconnected_variables_bool_keep_value+;
	else try scoop_gc_remove_disconnected_remote_call+;
	else try scoop_qoq_gc_remove_closed_queue_single+;
	else try scoop_qoq_gc_remove_closed_queue_multiple+;
	else try scoop_qoq_scheduling_pop_closed_subqueue+;
	else try scoop_gc_remove_disconnected_variable_void+;
	else fail;

	alap {
		try scoop_gc_values+;
		else try scoop_gc_primitive_boolean_values+;
		else try scoop_gc_primitive_integer_values+;
		//else try scoop_qoq_gc_evaluates_to+;
		else try scoop_gc_evaluation+;
		else try scoop_gc_remove_disconnected_stack_frames+;
		else try scoop_gc_remove_disconnected_variables_ref+;
		else try scoop_gc_remove_disconnected_variables_int+;
		else try scoop_gc_remove_disconnected_variables_bool+;
		else try scoop_gc_remove_disconnected_variables_ref_keep_value+;
		else try scoop_gc_remove_disconnected_variables_int_keep_value+;
		else try scoop_gc_remove_disconnected_variables_bool_keep_value+;
		else try scoop_gc_remove_disconnected_remote_call+;
		else try scoop_qoq_gc_remove_closed_queue_single+;
		else try scoop_qoq_gc_remove_closed_queue_multiple+;
		else try scoop_qoq_scheduling_pop_closed_subqueue+;
		else try scoop_gc_remove_disconnected_variable_void+;
		else fail;
	}
}

/*
Perform top-level actions (i.e. computations / setups involving the "Action" type nodes).

This recipe may fail.
*/
recipe execute_one_action() {
	scoop_action_assignment |
	scoop_action_create |
	scoop_action_create_nonseparate |
	scoop_qoq_action_create_separate_instantiation |
	scoop_qoq_action_cleanup_final_state |
	scoop_action_test |
	scoop_action_precondition_test |
	scoop_qoq_action_waitcondition_test |
	scoop_action_command_local |
	scoop_action_postcondition_test |
	scoop_action_end_inline_separate;
}

/*
Perform scheduling (i.e. tasks involving enqueueing and dequeuing of tasks).

This recipe may fail.
*/
recipe scheduling() {
	// Here, we want to execute the actions and queries that generate or consume a queue item.
	// We apply only one rule here, as we might want to advance the processors locally in between
	// different scheduling events in order to capture all interleavings.

	( scoop_qoq_scheduling_action_command_remote
	| scoop_qoq_scheduling_dequeue_single
	| scoop_qoq_scheduling_reserve_handlers
	| scoop_qoq_scheduling_create_query_call
	| scoop_qoq_scheduling_action_start_inline_separate
	);
}