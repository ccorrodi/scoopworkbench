<?xml version="1.0" encoding="UTF-8"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
	<graph edgeids="false" edgemode="directed" id="debug_translation" role="graph">
		<node id="node0">
			<attr name="layout">
				<string>500 250 250 50</string>
			</attr>
		</node>
		<node id="node1">
			<attr name="layout">
				<string>500 650 250 50</string>
			</attr>
		</node>
		<node id="node2">
			<attr name="layout">
				<string>150 650 250 50</string>
			</attr>
		</node>
		<node id="node3">
			<attr name="layout">
				<string>850 650 250 50</string>
			</attr>
		</node>
		<node id="node4">
			<attr name="layout">
				<string>850 750 250 50</string>
			</attr>
		</node>
		<node id="node5">
			<attr name="layout">
				<string>1050 750 250 50</string>
			</attr>
		</node>
		<node id="node6">
			<attr name="layout">
				<string>1200 650 250 50</string>
			</attr>
		</node>
		<node id="node7">
			<attr name="layout">
				<string>1550 650 250 50</string>
			</attr>
		</node>
		<node id="node8">
			<attr name="layout">
				<string>1550 750 250 50</string>
			</attr>
		</node>
		<node id="node9">
			<attr name="layout">
				<string>1750 750 250 50</string>
			</attr>
		</node>
		<node id="node10">
			<attr name="layout">
				<string>1750 850 250 50</string>
			</attr>
		</node>
		<node id="node11">
			<attr name="layout">
				<string>1900 650 250 50</string>
			</attr>
		</node>
		<node id="node12">
			<attr name="layout">
				<string>50 50 250 50</string>
			</attr>
		</node>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>type:ObjectTemplate</string>
			</attr>
		</edge>
		<edge from="node0" to="node0">
			<attr name="label">
				<string>let:name="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>type:InitialState</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node1" to="node1">
			<attr name="label">
				<string>let:procedure="make"</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>type:Variable</string>
			</attr>
		</edge>
		<edge from="node2" to="node2">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node3" to="node3">
			<attr name="label">
				<string>type:ActionAssignment</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node4" to="node4">
			<attr name="label">
				<string>let:name="i"</string>
			</attr>
		</edge>
		<edge from="node5" to="node5">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node5" to="node5">
			<attr name="label">
				<string>let:value=0</string>
			</attr>
		</edge>
		<edge from="node6" to="node6">
			<attr name="label">
				<string>type:ControlState</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>type:ActionCommand</string>
			</attr>
		</edge>
		<edge from="node7" to="node7">
			<attr name="label">
				<string>let:procedure="print"</string>
			</attr>
		</edge>
		<edge from="node8" to="node8">
			<attr name="label">
				<string>type:LocalExpression</string>
			</attr>
		</edge>
		<edge from="node8" to="node8">
			<attr name="label">
				<string>let:name="Current"</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>type:Parameter</string>
			</attr>
		</edge>
		<edge from="node9" to="node9">
			<attr name="label">
				<string>let:index=1</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>type:IntegerConstant</string>
			</attr>
		</edge>
		<edge from="node10" to="node10">
			<attr name="label">
				<string>let:value=1</string>
			</attr>
		</edge>
		<edge from="node11" to="node11">
			<attr name="label">
				<string>type:FinalState</string>
			</attr>
		</edge>
		<edge from="node1" to="node2">
			<attr name="label">
				<string>variable</string>
			</attr>
		</edge>
		<edge from="node1" to="node3">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node3" to="node6">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node3" to="node4">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node3" to="node5">
			<attr name="label">
				<string>source</string>
			</attr>
		</edge>
		<edge from="node6" to="node7">
			<attr name="label">
				<string>to_action</string>
			</attr>
		</edge>
		<edge from="node7" to="node8">
			<attr name="label">
				<string>target</string>
			</attr>
		</edge>
		<edge from="node7" to="node9">
			<attr name="label">
				<string>parameter</string>
			</attr>
		</edge>
		<edge from="node9" to="node10">
			<attr name="label">
				<string>expression</string>
			</attr>
		</edge>
		<edge from="node7" to="node11">
			<attr name="label">
				<string>to_state</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>type:Initialization</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>let:root_class="APPLICATION"</string>
			</attr>
		</edge>
		<edge from="node12" to="node12">
			<attr name="label">
				<string>let:root_procedure="make"</string>
			</attr>
		</edge>
	</graph>
</gxl>
