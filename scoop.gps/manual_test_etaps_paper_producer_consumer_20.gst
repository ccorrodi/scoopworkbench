<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<gxl xmlns="http://www.gupro.de/GXL/gxl-1.0.dtd">
    <graph role="graph" edgeids="false" edgemode="directed" id="paper_producer_consumer_20">
        <attr name="$version">
            <string>curly</string>
        </attr>
        <node id="n0">
            <attr name="layout">
                <string>500 250 250 50</string>
            </attr>
        </node>
        <node id="n1">
            <attr name="layout">
                <string>850 250 250 50</string>
            </attr>
        </node>
        <node id="n2">
            <attr name="layout">
                <string>850 350 250 50</string>
            </attr>
        </node>
        <node id="n3">
            <attr name="layout">
                <string>850 450 250 50</string>
            </attr>
        </node>
        <node id="n4">
            <attr name="layout">
                <string>850 550 250 50</string>
            </attr>
        </node>
        <node id="n5">
            <attr name="layout">
                <string>850 650 250 50</string>
            </attr>
        </node>
        <node id="n6">
            <attr name="layout">
                <string>850 750 250 50</string>
            </attr>
        </node>
        <node id="n7">
            <attr name="layout">
                <string>850 850 250 50</string>
            </attr>
        </node>
        <node id="n8">
            <attr name="layout">
                <string>850 950 250 50</string>
            </attr>
        </node>
        <node id="n9">
            <attr name="layout">
                <string>500 1450 250 50</string>
            </attr>
        </node>
        <node id="n10">
            <attr name="layout">
                <string>850 1450 250 50</string>
            </attr>
        </node>
        <node id="n11">
            <attr name="layout">
                <string>850 1550 250 50</string>
            </attr>
        </node>
        <node id="n12">
            <attr name="layout">
                <string>1050 1550 250 50</string>
            </attr>
        </node>
        <node id="n13">
            <attr name="layout">
                <string>1200 1450 250 50</string>
            </attr>
        </node>
        <node id="n14">
            <attr name="layout">
                <string>1550 1450 250 50</string>
            </attr>
        </node>
        <node id="n15">
            <attr name="layout">
                <string>1550 1550 250 50</string>
            </attr>
        </node>
        <node id="n16">
            <attr name="layout">
                <string>1900 1450 250 50</string>
            </attr>
        </node>
        <node id="n17">
            <attr name="layout">
                <string>2250 1450 250 50</string>
            </attr>
        </node>
        <node id="n18">
            <attr name="layout">
                <string>2250 1550 250 50</string>
            </attr>
        </node>
        <node id="n19">
            <attr name="layout">
                <string>2450 1550 250 50</string>
            </attr>
        </node>
        <node id="n20">
            <attr name="layout">
                <string>2450 1650 250 50</string>
            </attr>
        </node>
        <node id="n21">
            <attr name="layout">
                <string>2650 1550 250 50</string>
            </attr>
        </node>
        <node id="n22">
            <attr name="layout">
                <string>2650 1650 250 50</string>
            </attr>
        </node>
        <node id="n23">
            <attr name="layout">
                <string>2600 1450 250 50</string>
            </attr>
        </node>
        <node id="n24">
            <attr name="layout">
                <string>2950 1450 250 50</string>
            </attr>
        </node>
        <node id="n25">
            <attr name="layout">
                <string>2950 1550 250 50</string>
            </attr>
        </node>
        <node id="n26">
            <attr name="layout">
                <string>3150 1550 250 50</string>
            </attr>
        </node>
        <node id="n27">
            <attr name="layout">
                <string>3150 1650 250 50</string>
            </attr>
        </node>
        <node id="n28">
            <attr name="layout">
                <string>3350 1550 250 50</string>
            </attr>
        </node>
        <node id="n29">
            <attr name="layout">
                <string>3350 1650 250 50</string>
            </attr>
        </node>
        <node id="n30">
            <attr name="layout">
                <string>3300 1450 250 50</string>
            </attr>
        </node>
        <node id="n31">
            <attr name="layout">
                <string>3650 1450 250 50</string>
            </attr>
        </node>
        <node id="n32">
            <attr name="layout">
                <string>3650 1550 250 50</string>
            </attr>
        </node>
        <node id="n33">
            <attr name="layout">
                <string>3850 1550 250 50</string>
            </attr>
        </node>
        <node id="n34">
            <attr name="layout">
                <string>3850 1650 250 50</string>
            </attr>
        </node>
        <node id="n35">
            <attr name="layout">
                <string>4050 1550 250 50</string>
            </attr>
        </node>
        <node id="n36">
            <attr name="layout">
                <string>4050 1650 250 50</string>
            </attr>
        </node>
        <node id="n37">
            <attr name="layout">
                <string>4000 1450 250 50</string>
            </attr>
        </node>
        <node id="n38">
            <attr name="layout">
                <string>500 1850 250 50</string>
            </attr>
        </node>
        <node id="n39">
            <attr name="layout">
                <string>150 1850 250 50</string>
            </attr>
        </node>
        <node id="n40">
            <attr name="layout">
                <string>850 1850 250 50</string>
            </attr>
        </node>
        <node id="n41">
            <attr name="layout">
                <string>850 1950 250 50</string>
            </attr>
        </node>
        <node id="n42">
            <attr name="layout">
                <string>1050 1950 250 50</string>
            </attr>
        </node>
        <node id="n43">
            <attr name="layout">
                <string>1200 1850 250 50</string>
            </attr>
        </node>
        <node id="n44">
            <attr name="layout">
                <string>500 2250 250 50</string>
            </attr>
        </node>
        <node id="n45">
            <attr name="layout">
                <string>150 2250 250 50</string>
            </attr>
        </node>
        <node id="n46">
            <attr name="layout">
                <string>850 2250 250 50</string>
            </attr>
        </node>
        <node id="n47">
            <attr name="layout">
                <string>850 2350 250 50</string>
            </attr>
        </node>
        <node id="n48">
            <attr name="layout">
                <string>1050 2350 250 50</string>
            </attr>
        </node>
        <node id="n49">
            <attr name="layout">
                <string>1200 2250 250 50</string>
            </attr>
        </node>
        <node id="n50">
            <attr name="layout">
                <string>500 2650 250 50</string>
            </attr>
        </node>
        <node id="n51">
            <attr name="layout">
                <string>150 2650 250 50</string>
            </attr>
        </node>
        <node id="n52">
            <attr name="layout">
                <string>850 2650 250 50</string>
            </attr>
        </node>
        <node id="n53">
            <attr name="layout">
                <string>850 2750 250 50</string>
            </attr>
        </node>
        <node id="n54">
            <attr name="layout">
                <string>1050 2750 250 50</string>
            </attr>
        </node>
        <node id="n55">
            <attr name="layout">
                <string>1200 2650 250 50</string>
            </attr>
        </node>
        <node id="n56">
            <attr name="layout">
                <string>500 3050 250 50</string>
            </attr>
        </node>
        <node id="n57">
            <attr name="layout">
                <string>150 3050 250 50</string>
            </attr>
        </node>
        <node id="n58">
            <attr name="layout">
                <string>850 3050 250 50</string>
            </attr>
        </node>
        <node id="n59">
            <attr name="layout">
                <string>850 3150 250 50</string>
            </attr>
        </node>
        <node id="n60">
            <attr name="layout">
                <string>1050 3150 250 50</string>
            </attr>
        </node>
        <node id="n61">
            <attr name="layout">
                <string>1200 3050 250 50</string>
            </attr>
        </node>
        <node id="n62">
            <attr name="layout">
                <string>500 3450 250 50</string>
            </attr>
        </node>
        <node id="n63">
            <attr name="layout">
                <string>150 3450 250 50</string>
            </attr>
        </node>
        <node id="n64">
            <attr name="layout">
                <string>150 3550 250 50</string>
            </attr>
        </node>
        <node id="n65">
            <attr name="layout">
                <string>850 3450 250 50</string>
            </attr>
        </node>
        <node id="n66">
            <attr name="layout">
                <string>850 3550 250 50</string>
            </attr>
        </node>
        <node id="n67">
            <attr name="layout">
                <string>1200 3450 250 50</string>
            </attr>
        </node>
        <node id="n68">
            <attr name="layout">
                <string>1550 3450 250 50</string>
            </attr>
        </node>
        <node id="n69">
            <attr name="layout">
                <string>1550 3550 250 50</string>
            </attr>
        </node>
        <node id="n70">
            <attr name="layout">
                <string>1900 3450 250 50</string>
            </attr>
        </node>
        <node id="n71">
            <attr name="layout">
                <string>500 3850 250 50</string>
            </attr>
        </node>
        <node id="n72">
            <attr name="layout">
                <string>850 3850 250 50</string>
            </attr>
        </node>
        <node id="n73">
            <attr name="layout">
                <string>850 3950 250 50</string>
            </attr>
        </node>
        <node id="n74">
            <attr name="layout">
                <string>850 4050 250 50</string>
            </attr>
        </node>
        <node id="n75">
            <attr name="layout">
                <string>850 4150 250 50</string>
            </attr>
        </node>
        <node id="n76">
            <attr name="layout">
                <string>850 4250 250 50</string>
            </attr>
        </node>
        <node id="n77">
            <attr name="layout">
                <string>850 4350 250 50</string>
            </attr>
        </node>
        <node id="n78">
            <attr name="layout">
                <string>500 4850 250 50</string>
            </attr>
        </node>
        <node id="n79">
            <attr name="layout">
                <string>150 4850 250 50</string>
            </attr>
        </node>
        <node id="n80">
            <attr name="layout">
                <string>150 4950 250 50</string>
            </attr>
        </node>
        <node id="n81">
            <attr name="layout">
                <string>850 4850 250 50</string>
            </attr>
        </node>
        <node id="n82">
            <attr name="layout">
                <string>850 4950 250 50</string>
            </attr>
        </node>
        <node id="n83">
            <attr name="layout">
                <string>850 5050 250 50</string>
            </attr>
        </node>
        <node id="n84">
            <attr name="layout">
                <string>1050 5050 250 50</string>
            </attr>
        </node>
        <node id="n85">
            <attr name="layout">
                <string>1200 4850 250 50</string>
            </attr>
        </node>
        <node id="n86">
            <attr name="layout">
                <string>1550 4850 250 50</string>
            </attr>
        </node>
        <node id="n87">
            <attr name="layout">
                <string>1550 4950 250 50</string>
            </attr>
        </node>
        <node id="n88">
            <attr name="layout">
                <string>1750 4950 250 50</string>
            </attr>
        </node>
        <node id="n89">
            <attr name="layout">
                <string>1900 4850 250 50</string>
            </attr>
        </node>
        <node id="n90">
            <attr name="layout">
                <string>2250 4850 250 50</string>
            </attr>
        </node>
        <node id="n91">
            <attr name="layout">
                <string>2250 4950 250 50</string>
            </attr>
        </node>
        <node id="n92">
            <attr name="layout">
                <string>2450 4950 250 50</string>
            </attr>
        </node>
        <node id="n93">
            <attr name="layout">
                <string>2600 4850 250 50</string>
            </attr>
        </node>
        <node id="n94">
            <attr name="layout">
                <string>500 5250 250 50</string>
            </attr>
        </node>
        <node id="n95">
            <attr name="layout">
                <string>150 5250 250 50</string>
            </attr>
        </node>
        <node id="n96">
            <attr name="layout">
                <string>850 5250 250 50</string>
            </attr>
        </node>
        <node id="n97">
            <attr name="layout">
                <string>850 5350 250 50</string>
            </attr>
        </node>
        <node id="n98">
            <attr name="layout">
                <string>1050 5350 250 50</string>
            </attr>
        </node>
        <node id="n99">
            <attr name="layout">
                <string>1200 5250 250 50</string>
            </attr>
        </node>
        <node id="n100">
            <attr name="layout">
                <string>500 5650 250 50</string>
            </attr>
        </node>
        <node id="n101">
            <attr name="layout">
                <string>850 5650 250 50</string>
            </attr>
        </node>
        <node id="n102">
            <attr name="layout">
                <string>850 5750 250 50</string>
            </attr>
        </node>
        <node id="n103">
            <attr name="layout">
                <string>850 5850 250 50</string>
            </attr>
        </node>
        <node id="n104">
            <attr name="layout">
                <string>1050 5850 250 50</string>
            </attr>
        </node>
        <node id="n105">
            <attr name="layout">
                <string>1200 5650 250 50</string>
            </attr>
        </node>
        <node id="n106">
            <attr name="layout">
                <string>1200 5650 250 50</string>
            </attr>
        </node>
        <node id="n107">
            <attr name="layout">
                <string>1200 5750 250 50</string>
            </attr>
        </node>
        <node id="n108">
            <attr name="layout">
                <string>1550 5650 250 50</string>
            </attr>
        </node>
        <node id="n109">
            <attr name="layout">
                <string>1900 5650 250 50</string>
            </attr>
        </node>
        <node id="n110">
            <attr name="layout">
                <string>1900 5750 250 50</string>
            </attr>
        </node>
        <node id="n111">
            <attr name="layout">
                <string>2100 5750 250 50</string>
            </attr>
        </node>
        <node id="n112">
            <attr name="layout">
                <string>2100 5850 250 50</string>
            </attr>
        </node>
        <node id="n113">
            <attr name="layout">
                <string>2250 5650 250 50</string>
            </attr>
        </node>
        <node id="n114">
            <attr name="layout">
                <string>2600 5650 250 50</string>
            </attr>
        </node>
        <node id="n115">
            <attr name="layout">
                <string>2600 5750 250 50</string>
            </attr>
        </node>
        <node id="n116">
            <attr name="layout">
                <string>2800 5750 250 50</string>
            </attr>
        </node>
        <node id="n117">
            <attr name="layout">
                <string>2800 5750 250 50</string>
            </attr>
        </node>
        <node id="n118">
            <attr name="layout">
                <string>2800 5750 250 50</string>
            </attr>
        </node>
        <node id="n119">
            <attr name="layout">
                <string>2950 5650 250 50</string>
            </attr>
        </node>
        <node id="n120">
            <attr name="layout">
                <string>500 6050 250 50</string>
            </attr>
        </node>
        <node id="n121">
            <attr name="layout">
                <string>150 6050 250 50</string>
            </attr>
        </node>
        <node id="n122">
            <attr name="layout">
                <string>850 6050 250 50</string>
            </attr>
        </node>
        <node id="n123">
            <attr name="layout">
                <string>850 6150 250 50</string>
            </attr>
        </node>
        <node id="n124">
            <attr name="layout">
                <string>850 6250 250 50</string>
            </attr>
        </node>
        <node id="n125">
            <attr name="layout">
                <string>1200 6050 250 50</string>
            </attr>
        </node>
        <node id="n126">
            <attr name="layout">
                <string>1550 6050 250 50</string>
            </attr>
        </node>
        <node id="n127">
            <attr name="layout">
                <string>1550 6150 250 50</string>
            </attr>
        </node>
        <node id="n128">
            <attr name="layout">
                <string>1750 6150 250 50</string>
            </attr>
        </node>
        <node id="n129">
            <attr name="layout">
                <string>1750 6250 250 50</string>
            </attr>
        </node>
        <node id="n130">
            <attr name="layout">
                <string>1900 6050 250 50</string>
            </attr>
        </node>
        <node id="n131">
            <attr name="layout">
                <string>2250 6050 250 50</string>
            </attr>
        </node>
        <node id="n132">
            <attr name="layout">
                <string>2250 6150 250 50</string>
            </attr>
        </node>
        <node id="n133">
            <attr name="layout">
                <string>2600 6050 250 50</string>
            </attr>
        </node>
        <node id="n134">
            <attr name="layout">
                <string>500 6450 250 50</string>
            </attr>
        </node>
        <node id="n135">
            <attr name="layout">
                <string>150 6450 250 50</string>
            </attr>
        </node>
        <node id="n136">
            <attr name="layout">
                <string>850 6450 250 50</string>
            </attr>
        </node>
        <node id="n137">
            <attr name="layout">
                <string>850 6550 250 50</string>
            </attr>
        </node>
        <node id="n138">
            <attr name="layout">
                <string>1050 6550 250 50</string>
            </attr>
        </node>
        <node id="n139">
            <attr name="layout">
                <string>1200 6450 250 50</string>
            </attr>
        </node>
        <node id="n140">
            <attr name="layout">
                <string>500 6850 250 50</string>
            </attr>
        </node>
        <node id="n141">
            <attr name="layout">
                <string>150 6850 250 50</string>
            </attr>
        </node>
        <node id="n142">
            <attr name="layout">
                <string>850 6850 250 50</string>
            </attr>
        </node>
        <node id="n143">
            <attr name="layout">
                <string>850 6950 250 50</string>
            </attr>
        </node>
        <node id="n144">
            <attr name="layout">
                <string>1050 6950 250 50</string>
            </attr>
        </node>
        <node id="n145">
            <attr name="layout">
                <string>1200 6850 250 50</string>
            </attr>
        </node>
        <node id="n146">
            <attr name="layout">
                <string>500 7250 250 50</string>
            </attr>
        </node>
        <node id="n147">
            <attr name="layout">
                <string>850 7250 250 50</string>
            </attr>
        </node>
        <node id="n148">
            <attr name="layout">
                <string>850 7350 250 50</string>
            </attr>
        </node>
        <node id="n149">
            <attr name="layout">
                <string>500 7850 250 50</string>
            </attr>
        </node>
        <node id="n150">
            <attr name="layout">
                <string>850 7850 250 50</string>
            </attr>
        </node>
        <node id="n151">
            <attr name="layout">
                <string>850 7950 250 50</string>
            </attr>
        </node>
        <node id="n152">
            <attr name="layout">
                <string>1050 7950 250 50</string>
            </attr>
        </node>
        <node id="n153">
            <attr name="layout">
                <string>1200 7850 250 50</string>
            </attr>
        </node>
        <node id="n154">
            <attr name="layout">
                <string>500 8250 250 50</string>
            </attr>
        </node>
        <node id="n155">
            <attr name="layout">
                <string>150 8250 250 50</string>
            </attr>
        </node>
        <node id="n156">
            <attr name="layout">
                <string>850 8250 250 50</string>
            </attr>
        </node>
        <node id="n157">
            <attr name="layout">
                <string>850 8350 250 50</string>
            </attr>
        </node>
        <node id="n158">
            <attr name="layout">
                <string>1050 8350 250 50</string>
            </attr>
        </node>
        <node id="n159">
            <attr name="layout">
                <string>1200 8250 250 50</string>
            </attr>
        </node>
        <node id="n160">
            <attr name="layout">
                <string>500 8650 250 50</string>
            </attr>
        </node>
        <node id="n161">
            <attr name="layout">
                <string>150 8650 250 50</string>
            </attr>
        </node>
        <node id="n162">
            <attr name="layout">
                <string>850 8650 250 50</string>
            </attr>
        </node>
        <node id="n163">
            <attr name="layout">
                <string>850 8750 250 50</string>
            </attr>
        </node>
        <node id="n164">
            <attr name="layout">
                <string>1050 8750 250 50</string>
            </attr>
        </node>
        <node id="n165">
            <attr name="layout">
                <string>1050 8850 250 50</string>
            </attr>
        </node>
        <node id="n166">
            <attr name="layout">
                <string>1250 8850 250 50</string>
            </attr>
        </node>
        <node id="n167">
            <attr name="layout">
                <string>1200 8650 250 50</string>
            </attr>
        </node>
        <node id="n168">
            <attr name="layout">
                <string>500 9050 250 50</string>
            </attr>
        </node>
        <node id="n169">
            <attr name="layout">
                <string>150 9050 250 50</string>
            </attr>
        </node>
        <node id="n170">
            <attr name="layout">
                <string>850 9050 250 50</string>
            </attr>
        </node>
        <node id="n171">
            <attr name="layout">
                <string>850 9150 250 50</string>
            </attr>
        </node>
        <node id="n172">
            <attr name="layout">
                <string>850 9250 250 50</string>
            </attr>
        </node>
        <node id="n173">
            <attr name="layout">
                <string>1050 9250 250 50</string>
            </attr>
        </node>
        <node id="n174">
            <attr name="layout">
                <string>1200 9050 250 50</string>
            </attr>
        </node>
        <node id="n175">
            <attr name="layout">
                <string>1550 9050 250 50</string>
            </attr>
        </node>
        <node id="n176">
            <attr name="layout">
                <string>1550 9150 250 50</string>
            </attr>
        </node>
        <node id="n177">
            <attr name="layout">
                <string>1550 9150 250 50</string>
            </attr>
        </node>
        <node id="n178">
            <attr name="layout">
                <string>1550 9250 250 50</string>
            </attr>
        </node>
        <node id="n179">
            <attr name="layout">
                <string>1900 9050 250 50</string>
            </attr>
        </node>
        <node id="n180">
            <attr name="layout">
                <string>2250 9050 250 50</string>
            </attr>
        </node>
        <node id="n181">
            <attr name="layout">
                <string>2250 9150 250 50</string>
            </attr>
        </node>
        <node id="n182">
            <attr name="layout">
                <string>2450 9150 250 50</string>
            </attr>
        </node>
        <node id="n183">
            <attr name="layout">
                <string>2600 9050 250 50</string>
            </attr>
        </node>
        <node id="n184">
            <attr name="layout">
                <string>500 9450 250 50</string>
            </attr>
        </node>
        <node id="n185">
            <attr name="layout">
                <string>850 9450 250 50</string>
            </attr>
        </node>
        <node id="n186">
            <attr name="layout">
                <string>850 9550 250 50</string>
            </attr>
        </node>
        <node id="n187">
            <attr name="layout">
                <string>850 9650 250 50</string>
            </attr>
        </node>
        <node id="n188">
            <attr name="layout">
                <string>1200 9450 250 50</string>
            </attr>
        </node>
        <node id="n189">
            <attr name="layout">
                <string>1550 9450 250 50</string>
            </attr>
        </node>
        <node id="n190">
            <attr name="layout">
                <string>1550 9550 250 50</string>
            </attr>
        </node>
        <node id="n191">
            <attr name="layout">
                <string>1750 9550 250 50</string>
            </attr>
        </node>
        <node id="n192">
            <attr name="layout">
                <string>1900 9450 250 50</string>
            </attr>
        </node>
        <node id="n193">
            <attr name="layout">
                <string>500 9850 250 50</string>
            </attr>
        </node>
        <node id="n194">
            <attr name="layout">
                <string>850 9850 250 50</string>
            </attr>
        </node>
        <node id="n195">
            <attr name="layout">
                <string>850 9950 250 50</string>
            </attr>
        </node>
        <node id="n196">
            <attr name="layout">
                <string>850 10050 250 50</string>
            </attr>
        </node>
        <node id="n197">
            <attr name="layout">
                <string>850 10150 250 50</string>
            </attr>
        </node>
        <node id="n198">
            <attr name="layout">
                <string>850 10250 250 50</string>
            </attr>
        </node>
        <node id="n199">
            <attr name="layout">
                <string>850 10350 250 50</string>
            </attr>
        </node>
        <node id="n200">
            <attr name="layout">
                <string>500 10850 250 50</string>
            </attr>
        </node>
        <node id="n201">
            <attr name="layout">
                <string>150 10850 250 50</string>
            </attr>
        </node>
        <node id="n202">
            <attr name="layout">
                <string>150 10950 250 50</string>
            </attr>
        </node>
        <node id="n203">
            <attr name="layout">
                <string>850 10850 250 50</string>
            </attr>
        </node>
        <node id="n204">
            <attr name="layout">
                <string>850 10950 250 50</string>
            </attr>
        </node>
        <node id="n205">
            <attr name="layout">
                <string>850 11050 250 50</string>
            </attr>
        </node>
        <node id="n206">
            <attr name="layout">
                <string>1050 11050 250 50</string>
            </attr>
        </node>
        <node id="n207">
            <attr name="layout">
                <string>1200 10850 250 50</string>
            </attr>
        </node>
        <node id="n208">
            <attr name="layout">
                <string>1550 10850 250 50</string>
            </attr>
        </node>
        <node id="n209">
            <attr name="layout">
                <string>1550 10950 250 50</string>
            </attr>
        </node>
        <node id="n210">
            <attr name="layout">
                <string>1750 10950 250 50</string>
            </attr>
        </node>
        <node id="n211">
            <attr name="layout">
                <string>1900 10850 250 50</string>
            </attr>
        </node>
        <node id="n212">
            <attr name="layout">
                <string>2250 10850 250 50</string>
            </attr>
        </node>
        <node id="n213">
            <attr name="layout">
                <string>2250 10950 250 50</string>
            </attr>
        </node>
        <node id="n214">
            <attr name="layout">
                <string>2450 10950 250 50</string>
            </attr>
        </node>
        <node id="n215">
            <attr name="layout">
                <string>2600 10850 250 50</string>
            </attr>
        </node>
        <node id="n216">
            <attr name="layout">
                <string>2950 10850 250 50</string>
            </attr>
        </node>
        <node id="n217">
            <attr name="layout">
                <string>2950 10950 250 50</string>
            </attr>
        </node>
        <node id="n218">
            <attr name="layout">
                <string>3150 10950 250 50</string>
            </attr>
        </node>
        <node id="n219">
            <attr name="layout">
                <string>3300 10850 250 50</string>
            </attr>
        </node>
        <node id="n220">
            <attr name="layout">
                <string>500 11250 250 50</string>
            </attr>
        </node>
        <node id="n221">
            <attr name="layout">
                <string>150 11250 250 50</string>
            </attr>
        </node>
        <node id="n222">
            <attr name="layout">
                <string>850 11250 250 50</string>
            </attr>
        </node>
        <node id="n223">
            <attr name="layout">
                <string>850 11350 250 50</string>
            </attr>
        </node>
        <node id="n224">
            <attr name="layout">
                <string>1050 11350 250 50</string>
            </attr>
        </node>
        <node id="n225">
            <attr name="layout">
                <string>1200 11250 250 50</string>
            </attr>
        </node>
        <node id="n226">
            <attr name="layout">
                <string>500 11650 250 50</string>
            </attr>
        </node>
        <node id="n227">
            <attr name="layout">
                <string>850 11650 250 50</string>
            </attr>
        </node>
        <node id="n228">
            <attr name="layout">
                <string>850 11750 250 50</string>
            </attr>
        </node>
        <node id="n229">
            <attr name="layout">
                <string>850 11850 250 50</string>
            </attr>
        </node>
        <node id="n230">
            <attr name="layout">
                <string>1050 11850 250 50</string>
            </attr>
        </node>
        <node id="n231">
            <attr name="layout">
                <string>1200 11650 250 50</string>
            </attr>
        </node>
        <node id="n232">
            <attr name="layout">
                <string>1200 11650 250 50</string>
            </attr>
        </node>
        <node id="n233">
            <attr name="layout">
                <string>1200 11750 250 50</string>
            </attr>
        </node>
        <node id="n234">
            <attr name="layout">
                <string>1550 11650 250 50</string>
            </attr>
        </node>
        <node id="n235">
            <attr name="layout">
                <string>1900 11650 250 50</string>
            </attr>
        </node>
        <node id="n236">
            <attr name="layout">
                <string>1900 11750 250 50</string>
            </attr>
        </node>
        <node id="n237">
            <attr name="layout">
                <string>2100 11750 250 50</string>
            </attr>
        </node>
        <node id="n238">
            <attr name="layout">
                <string>2100 11850 250 50</string>
            </attr>
        </node>
        <node id="n239">
            <attr name="layout">
                <string>2250 11650 250 50</string>
            </attr>
        </node>
        <node id="n240">
            <attr name="layout">
                <string>2600 11650 250 50</string>
            </attr>
        </node>
        <node id="n241">
            <attr name="layout">
                <string>2600 11750 250 50</string>
            </attr>
        </node>
        <node id="n242">
            <attr name="layout">
                <string>2800 11750 250 50</string>
            </attr>
        </node>
        <node id="n243">
            <attr name="layout">
                <string>2800 11750 250 50</string>
            </attr>
        </node>
        <node id="n244">
            <attr name="layout">
                <string>2800 11750 250 50</string>
            </attr>
        </node>
        <node id="n245">
            <attr name="layout">
                <string>2950 11650 250 50</string>
            </attr>
        </node>
        <node id="n246">
            <attr name="layout">
                <string>500 12050 250 50</string>
            </attr>
        </node>
        <node id="n247">
            <attr name="layout">
                <string>150 12050 250 50</string>
            </attr>
        </node>
        <node id="n248">
            <attr name="layout">
                <string>850 12050 250 50</string>
            </attr>
        </node>
        <node id="n249">
            <attr name="layout">
                <string>850 12150 250 50</string>
            </attr>
        </node>
        <node id="n250">
            <attr name="layout">
                <string>850 12150 250 50</string>
            </attr>
        </node>
        <node id="n251">
            <attr name="layout">
                <string>850 12250 250 50</string>
            </attr>
        </node>
        <node id="n252">
            <attr name="layout">
                <string>1200 12050 250 50</string>
            </attr>
        </node>
        <node id="n253">
            <attr name="layout">
                <string>1550 12050 250 50</string>
            </attr>
        </node>
        <node id="n254">
            <attr name="layout">
                <string>1550 12150 250 50</string>
            </attr>
        </node>
        <node id="n255">
            <attr name="layout">
                <string>1750 12150 250 50</string>
            </attr>
        </node>
        <node id="n256">
            <attr name="layout">
                <string>1750 12150 250 50</string>
            </attr>
        </node>
        <node id="n257">
            <attr name="layout">
                <string>1750 12150 250 50</string>
            </attr>
        </node>
        <node id="n258">
            <attr name="layout">
                <string>1900 12050 250 50</string>
            </attr>
        </node>
        <node id="n259">
            <attr name="layout">
                <string>2250 12050 250 50</string>
            </attr>
        </node>
        <node id="n260">
            <attr name="layout">
                <string>2250 12150 250 50</string>
            </attr>
        </node>
        <node id="n261">
            <attr name="layout">
                <string>2450 12150 250 50</string>
            </attr>
        </node>
        <node id="n262">
            <attr name="layout">
                <string>2450 12250 250 50</string>
            </attr>
        </node>
        <node id="n263">
            <attr name="layout">
                <string>2600 12050 250 50</string>
            </attr>
        </node>
        <node id="n264">
            <attr name="layout">
                <string>500 12450 250 50</string>
            </attr>
        </node>
        <node id="n265">
            <attr name="layout">
                <string>150 12450 250 50</string>
            </attr>
        </node>
        <node id="n266">
            <attr name="layout">
                <string>850 12450 250 50</string>
            </attr>
        </node>
        <node id="n267">
            <attr name="layout">
                <string>850 12550 250 50</string>
            </attr>
        </node>
        <node id="n268">
            <attr name="layout">
                <string>1050 12550 250 50</string>
            </attr>
        </node>
        <node id="n269">
            <attr name="layout">
                <string>1200 12450 250 50</string>
            </attr>
        </node>
        <node id="n270">
            <attr name="layout">
                <string>500 12850 250 50</string>
            </attr>
        </node>
        <node id="n271">
            <attr name="layout">
                <string>150 12850 250 50</string>
            </attr>
        </node>
        <node id="n272">
            <attr name="layout">
                <string>850 12850 250 50</string>
            </attr>
        </node>
        <node id="n273">
            <attr name="layout">
                <string>850 12950 250 50</string>
            </attr>
        </node>
        <node id="n274">
            <attr name="layout">
                <string>1050 12950 250 50</string>
            </attr>
        </node>
        <node id="n275">
            <attr name="layout">
                <string>1200 12850 250 50</string>
            </attr>
        </node>
        <node id="n276">
            <attr name="layout">
                <string>50 50 250 50</string>
            </attr>
        </node>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n0" to="n2">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n0">
            <attr name="label">
                <string>let:name = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n0" to="n4">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n8">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n1">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n7">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n3">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n5">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n0" to="n6">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n1" to="n1">
            <attr name="label">
                <string>let:name = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n2" to="n2">
            <attr name="label">
                <string>let:name = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n3" to="n3">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n4" to="n4">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n5" to="n5">
            <attr name="label">
                <string>let:name = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n6" to="n6">
            <attr name="label">
                <string>let:name = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n7" to="n7">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n8" to="n8">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n9" to="n10">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n9" to="n9">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n10" to="n12">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n10" to="n13">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n10" to="n10">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n10" to="n11">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n11" to="n11">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n12" to="n12">
            <attr name="label">
                <string>let:value = 20</string>
            </attr>
        </edge>
        <edge from="n13" to="n13">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n13" to="n14">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n14" to="n15">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n14" to="n16">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n14" to="n14">
            <attr name="label">
                <string>let:template = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n15" to="n15">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n16" to="n16">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n16" to="n17">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n17" to="n19">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:template = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n17" to="n21">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n17" to="n23">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n17" to="n18">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n17" to="n17">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n18" to="n18">
            <attr name="label">
                <string>let:name = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n19" to="n19">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n19" to="n20">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n20" to="n20">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n21" to="n22">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n21" to="n21">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n22" to="n22">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n23" to="n23">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n23" to="n24">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n24" to="n30">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:template = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n24" to="n26">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n24" to="n25">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n24" to="n28">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>flag:separate</string>
            </attr>
        </edge>
        <edge from="n24" to="n24">
            <attr name="label">
                <string>type:ActionCreate</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n25" to="n25">
            <attr name="label">
                <string>let:name = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n26" to="n26">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n26" to="n27">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n27" to="n27">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n28" to="n28">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n28" to="n29">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n29" to="n29">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n30" to="n30">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n30" to="n31">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>let:procedure = &quot;run&quot;</string>
            </attr>
        </edge>
        <edge from="n31" to="n32">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n31" to="n37">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n31" to="n31">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n31" to="n35">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n31" to="n33">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>let:name = &quot;Current&quot;</string>
            </attr>
        </edge>
        <edge from="n32" to="n32">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n33" to="n34">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n33" to="n33">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n34" to="n34">
            <attr name="label">
                <string>let:name = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n35" to="n35">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n35" to="n36">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>let:name = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n36" to="n36">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n37" to="n37">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n38" to="n40">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n38" to="n39">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>let:procedure = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n38" to="n38">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n39" to="n39">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n40" to="n43">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n40" to="n41">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n40" to="n42">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n40" to="n40">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n41" to="n41">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n42" to="n42">
            <attr name="label">
                <string>let:name = &quot;single_producer&quot;</string>
            </attr>
        </edge>
        <edge from="n43" to="n43">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n44" to="n46">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n44" to="n44">
            <attr name="label">
                <string>let:procedure = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n44" to="n45">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n45" to="n45">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n46" to="n46">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n46" to="n47">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n46" to="n48">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n46" to="n49">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n47" to="n47">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n48" to="n48">
            <attr name="label">
                <string>let:name = &quot;single_consumer&quot;</string>
            </attr>
        </edge>
        <edge from="n49" to="n49">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n50" to="n51">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:procedure = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n50" to="n52">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n50" to="n50">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n51" to="n51">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n52" to="n55">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n52" to="n54">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n52" to="n52">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n52" to="n53">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n53" to="n53">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n54" to="n54">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n55" to="n55">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n56" to="n56">
            <attr name="label">
                <string>let:procedure = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n56" to="n57">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n56" to="n58">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n57" to="n57">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n58" to="n60">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n58" to="n59">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n58" to="n61">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n58" to="n58">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n59" to="n59">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n60" to="n60">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n61" to="n61">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n62" to="n65">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n62" to="n64">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>let:procedure = &quot;run&quot;</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n62" to="n63">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n62" to="n62">
            <attr name="label">
                <string>let:class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n63" to="n63">
            <attr name="label">
                <string>let:name = &quot;l_p&quot;</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n64" to="n64">
            <attr name="label">
                <string>let:name = &quot;l_c&quot;</string>
            </attr>
        </edge>
        <edge from="n65" to="n66">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n65" to="n67">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n65" to="n65">
            <attr name="label">
                <string>let:procedure = &quot;live&quot;</string>
            </attr>
        </edge>
        <edge from="n66" to="n66">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n66" to="n66">
            <attr name="label">
                <string>let:name = &quot;l_p&quot;</string>
            </attr>
        </edge>
        <edge from="n67" to="n67">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n67" to="n68">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n68" to="n70">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n68" to="n69">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n68" to="n68">
            <attr name="label">
                <string>let:procedure = &quot;live&quot;</string>
            </attr>
        </edge>
        <edge from="n69" to="n69">
            <attr name="label">
                <string>let:name = &quot;l_c&quot;</string>
            </attr>
        </edge>
        <edge from="n69" to="n69">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n70" to="n70">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n71" to="n76">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n73">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n71">
            <attr name="label">
                <string>let:name = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n71" to="n71">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n71" to="n74">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n72">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n75">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n71" to="n77">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n72" to="n72">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n72" to="n72">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n73" to="n73">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n73" to="n73">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n74" to="n74">
            <attr name="label">
                <string>let:name = &quot;l_consumed_item&quot;</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n75" to="n75">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n76" to="n76">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>let:name = &quot;l_consumed_item&quot;</string>
            </attr>
        </edge>
        <edge from="n77" to="n77">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n78" to="n81">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n78" to="n80">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n78" to="n78">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n78" to="n79">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n79" to="n79">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n80" to="n80">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n81" to="n82">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n81" to="n85">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n81" to="n81">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n82" to="n82">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n82" to="n84">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n82" to="n83">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n83" to="n83">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n84" to="n84">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n85" to="n86">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n85" to="n85">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n86" to="n88">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n86" to="n87">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n86" to="n89">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n86" to="n86">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n87" to="n87">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n88" to="n88">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n89" to="n90">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n89" to="n89">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n90" to="n91">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n90" to="n92">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n90" to="n90">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n90" to="n93">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n91" to="n91">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n92" to="n92">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n93" to="n93">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n94" to="n95">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n94" to="n96">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>let:procedure = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n94" to="n94">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n95" to="n95">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n96" to="n96">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n96" to="n98">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n96" to="n97">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n96" to="n99">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n97" to="n97">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n98" to="n98">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n99" to="n99">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n100" to="n101">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n100" to="n106">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>let:procedure = &quot;live&quot;</string>
            </attr>
        </edge>
        <edge from="n100" to="n100">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n101" to="n105">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n101" to="n101">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n101" to="n102">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n102" to="n104">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n102" to="n102">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n102" to="n103">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n103" to="n103">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n104" to="n104">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n105" to="n105">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n106" to="n108">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n106" to="n107">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n106" to="n106">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n107" to="n107">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n107" to="n102">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n108" to="n108">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n108" to="n109">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n109" to="n113">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>let:procedure = &quot;consume&quot;</string>
            </attr>
        </edge>
        <edge from="n109" to="n109">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n109" to="n110">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n109" to="n111">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>let:name = &quot;Current&quot;</string>
            </attr>
        </edge>
        <edge from="n110" to="n110">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n111" to="n112">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n111" to="n111">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n112" to="n112">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n113" to="n113">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n113" to="n114">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n114" to="n119">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n114" to="n116">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n114" to="n115">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n114" to="n114">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n115" to="n115">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n116" to="n116">
            <attr name="label">
                <string>type:IntegerSubtraction</string>
            </attr>
        </edge>
        <edge from="n116" to="n118">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n116" to="n117">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n117" to="n117">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n118" to="n118">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n119" to="n119">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n119" to="n101">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n119" to="n106">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n120" to="n122">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n120" to="n121">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>let:procedure = &quot;consume&quot;</string>
            </attr>
        </edge>
        <edge from="n120" to="n120">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n121" to="n121">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n122" to="n125">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n122" to="n123">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n122" to="n122">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>let:procedure = &quot;has_item&quot;</string>
            </attr>
        </edge>
        <edge from="n123" to="n123">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n123" to="n124">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n124" to="n124">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n125" to="n125">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n125" to="n126">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n126" to="n128">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n126" to="n130">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n126" to="n127">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n126" to="n126">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>let:name = &quot;l_consumed_item&quot;</string>
            </attr>
        </edge>
        <edge from="n127" to="n127">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>let:procedure = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n128" to="n129">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n128" to="n128">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n129" to="n129">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n130" to="n130">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n130" to="n131">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n131" to="n133">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n131" to="n132">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n131" to="n131">
            <attr name="label">
                <string>let:procedure = &quot;remove&quot;</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n132" to="n132">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n133" to="n133">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n134" to="n135">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n134" to="n134">
            <attr name="label">
                <string>let:procedure = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n134" to="n136">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n135" to="n135">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n136" to="n136">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n136" to="n138">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n136" to="n137">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n136" to="n139">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n137" to="n137">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n138" to="n138">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n139" to="n139">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:procedure = &quot;l_consumed_item&quot;</string>
            </attr>
        </edge>
        <edge from="n140" to="n142">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>let:class = &quot;CONSUMER&quot;</string>
            </attr>
        </edge>
        <edge from="n140" to="n140">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n140" to="n141">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n141" to="n141">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n142" to="n145">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n142" to="n144">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n142" to="n143">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n142" to="n142">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n143" to="n143">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n144" to="n144">
            <attr name="label">
                <string>let:name = &quot;l_consumed_item&quot;</string>
            </attr>
        </edge>
        <edge from="n145" to="n145">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n146" to="n148">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>let:name = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n146" to="n147">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n146" to="n146">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n147" to="n147">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n148" to="n148">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n149" to="n150">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n149" to="n149">
            <attr name="label">
                <string>let:class = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n150" to="n150">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n150" to="n152">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n150" to="n153">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n150" to="n151">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n151" to="n151">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n152" to="n152">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n153" to="n153">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>let:class = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n154" to="n155">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n154" to="n156">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n154" to="n154">
            <attr name="label">
                <string>let:procedure = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n155" to="n155">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n156" to="n158">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n156" to="n156">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n156" to="n157">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n156" to="n159">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n157" to="n157">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n158" to="n158">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n159" to="n159">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n160" to="n161">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n160" to="n162">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:class = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n160" to="n160">
            <attr name="label">
                <string>let:procedure = &quot;has_item&quot;</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n161" to="n161">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n162" to="n167">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n162" to="n162">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n162" to="n163">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n162" to="n164">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n163" to="n163">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n164" to="n166">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n164" to="n164">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n164" to="n165">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n165" to="n165">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n166" to="n166">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n167" to="n167">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:class = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n168" to="n170">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n168" to="n169">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n168" to="n168">
            <attr name="label">
                <string>let:procedure = &quot;put&quot;</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:name = &quot;a_value&quot;</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n169" to="n169">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n170" to="n170">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n170" to="n171">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n170" to="n174">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n171" to="n172">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n171" to="n171">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n171" to="n173">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>let:name = &quot;a_value&quot;</string>
            </attr>
        </edge>
        <edge from="n172" to="n172">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n173" to="n173">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n174" to="n174">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n174" to="n175">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n175" to="n175">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n175" to="n179">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n175" to="n176">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n176" to="n176">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n176" to="n177">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>let:procedure = &quot;has_item&quot;</string>
            </attr>
        </edge>
        <edge from="n177" to="n177">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n177" to="n178">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n178" to="n178">
            <attr name="label">
                <string>let:name = &quot;Current&quot;</string>
            </attr>
        </edge>
        <edge from="n178" to="n178">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n179" to="n180">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n179" to="n179">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n180" to="n181">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n180" to="n180">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n180" to="n183">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n180" to="n182">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n181" to="n181">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n182" to="n182">
            <attr name="label">
                <string>let:name = &quot;a_value&quot;</string>
            </attr>
        </edge>
        <edge from="n183" to="n183">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:procedure = &quot;remove&quot;</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>let:class = &quot;INVENTORY&quot;</string>
            </attr>
        </edge>
        <edge from="n184" to="n185">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n184" to="n184">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n185" to="n188">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n185" to="n185">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n185" to="n186">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n186" to="n187">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n186" to="n186">
            <attr name="label">
                <string>let:procedure = &quot;has_item&quot;</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n187" to="n187">
            <attr name="label">
                <string>let:name = &quot;Current&quot;</string>
            </attr>
        </edge>
        <edge from="n188" to="n188">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n188" to="n189">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n189" to="n191">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n189" to="n189">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n189" to="n190">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n189" to="n192">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>let:name = &quot;item&quot;</string>
            </attr>
        </edge>
        <edge from="n190" to="n190">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n191" to="n191">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n192" to="n192">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n193" to="n196">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n198">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n197">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>type:ObjectTemplate</string>
            </attr>
        </edge>
        <edge from="n193" to="n199">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n194">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n193" to="n193">
            <attr name="label">
                <string>let:name = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n193" to="n195">
            <attr name="label">
                <string>attribute</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n194" to="n194">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n195" to="n195">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n196" to="n196">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n197" to="n197">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n198" to="n198">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n198" to="n198">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n199" to="n199">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n199" to="n199">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n200" to="n203">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>let:procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n200" to="n201">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n200" to="n202">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n200" to="n200">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n201" to="n201">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n202" to="n202">
            <attr name="label">
                <string>let:index = 2</string>
            </attr>
        </edge>
        <edge from="n203" to="n207">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n203" to="n204">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n203" to="n203">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n204" to="n206">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n204" to="n204">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n204" to="n205">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n205" to="n205">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n205" to="n205">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n206" to="n206">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n206" to="n206">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n207" to="n208">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n207" to="n207">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n208" to="n208">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n208" to="n211">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n208" to="n210">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n208" to="n209">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n209" to="n209">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n209" to="n209">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n210" to="n210">
            <attr name="label">
                <string>let:name = &quot;a_item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n210" to="n210">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n211" to="n211">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n211" to="n212">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n212" to="n214">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n212" to="n213">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n212" to="n212">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n212" to="n215">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n213" to="n213">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n213" to="n213">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n214" to="n214">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n214" to="n214">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n215" to="n216">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n215" to="n215">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n216" to="n217">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n216" to="n218">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n216" to="n219">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n216" to="n216">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n217" to="n217">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n217" to="n217">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n218" to="n218">
            <attr name="label">
                <string>let:value = 0</string>
            </attr>
        </edge>
        <edge from="n218" to="n218">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n219" to="n219">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n220" to="n222">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>let:procedure = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n220" to="n221">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n220" to="n220">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n221" to="n221">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n221" to="n221">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n222" to="n225">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n222" to="n223">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n222" to="n222">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n222" to="n224">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n223" to="n223">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n223" to="n223">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n224" to="n224">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n224" to="n224">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n225" to="n225">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>let:procedure = &quot;live&quot;</string>
            </attr>
        </edge>
        <edge from="n226" to="n227">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n226" to="n232">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n226" to="n226">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n227" to="n231">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n227" to="n227">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n227" to="n228">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n228" to="n228">
            <attr name="label">
                <string>type:BooleanGreaterThanExpression</string>
            </attr>
        </edge>
        <edge from="n228" to="n230">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n228" to="n229">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n229" to="n229">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n229" to="n229">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n230" to="n230">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n230" to="n230">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n231" to="n231">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n232" to="n232">
            <attr name="label">
                <string>type:ActionTest</string>
            </attr>
        </edge>
        <edge from="n232" to="n234">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n232" to="n233">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n233" to="n233">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n233" to="n228">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n234" to="n234">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n234" to="n235">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n235" to="n236">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n235" to="n239">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n235" to="n235">
            <attr name="label">
                <string>let:procedure = &quot;produce&quot;</string>
            </attr>
        </edge>
        <edge from="n235" to="n235">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n235" to="n237">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n236" to="n236">
            <attr name="label">
                <string>let:name = &quot;Current&quot;</string>
            </attr>
        </edge>
        <edge from="n236" to="n236">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n237" to="n237">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n237" to="n237">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n237" to="n238">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n238" to="n238">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n238" to="n238">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n239" to="n240">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n239" to="n239">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n240" to="n242">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n240" to="n245">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n240" to="n241">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n240" to="n240">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n241" to="n241">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n242" to="n243">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n242" to="n244">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n242" to="n242">
            <attr name="label">
                <string>type:IntegerSubtraction</string>
            </attr>
        </edge>
        <edge from="n243" to="n243">
            <attr name="label">
                <string>let:name = &quot;item_count&quot;</string>
            </attr>
        </edge>
        <edge from="n243" to="n243">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n244" to="n244">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n244" to="n244">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n245" to="n245">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n245" to="n227">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n245" to="n232">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n246" to="n247">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>let:procedure = &quot;produce&quot;</string>
            </attr>
        </edge>
        <edge from="n246" to="n248">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n246" to="n246">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>type:ParameterMapping</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n247" to="n247">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n248" to="n248">
            <attr name="label">
                <string>type:ActionPreconditionTest</string>
            </attr>
        </edge>
        <edge from="n248" to="n252">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n248" to="n249">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n249" to="n249">
            <attr name="label">
                <string>type:BooleanNegationExpression</string>
            </attr>
        </edge>
        <edge from="n249" to="n250">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n250" to="n250">
            <attr name="label">
                <string>let:procedure = &quot;has_item&quot;</string>
            </attr>
        </edge>
        <edge from="n250" to="n251">
            <attr name="label">
                <string>query_target</string>
            </attr>
        </edge>
        <edge from="n250" to="n250">
            <attr name="label">
                <string>type:QueryExpression</string>
            </attr>
        </edge>
        <edge from="n251" to="n251">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n251" to="n251">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n252" to="n253">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n252" to="n252">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n253" to="n255">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n253" to="n253">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n253" to="n254">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n253" to="n258">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n254" to="n254">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n254" to="n254">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n255" to="n257">
            <attr name="label">
                <string>right</string>
            </attr>
        </edge>
        <edge from="n255" to="n256">
            <attr name="label">
                <string>left</string>
            </attr>
        </edge>
        <edge from="n255" to="n255">
            <attr name="label">
                <string>type:IntegerAddition</string>
            </attr>
        </edge>
        <edge from="n256" to="n256">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n256" to="n256">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n257" to="n257">
            <attr name="label">
                <string>type:IntegerConstant</string>
            </attr>
        </edge>
        <edge from="n257" to="n257">
            <attr name="label">
                <string>let:value = 1</string>
            </attr>
        </edge>
        <edge from="n258" to="n259">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n258" to="n258">
            <attr name="label">
                <string>type:ControlState</string>
            </attr>
        </edge>
        <edge from="n259" to="n259">
            <attr name="label">
                <string>type:ActionCommand</string>
            </attr>
        </edge>
        <edge from="n259" to="n263">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n259" to="n259">
            <attr name="label">
                <string>let:procedure = &quot;put&quot;</string>
            </attr>
        </edge>
        <edge from="n259" to="n261">
            <attr name="label">
                <string>parameter</string>
            </attr>
        </edge>
        <edge from="n259" to="n260">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n260" to="n260">
            <attr name="label">
                <string>type:ParameterExpression</string>
            </attr>
        </edge>
        <edge from="n260" to="n260">
            <attr name="label">
                <string>let:name = &quot;a_inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n261" to="n261">
            <attr name="label">
                <string>let:index = 1</string>
            </attr>
        </edge>
        <edge from="n261" to="n261">
            <attr name="label">
                <string>type:Parameter</string>
            </attr>
        </edge>
        <edge from="n261" to="n262">
            <attr name="label">
                <string>expression</string>
            </attr>
        </edge>
        <edge from="n262" to="n262">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n262" to="n262">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n263" to="n263">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>let:procedure = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n264" to="n266">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n264" to="n264">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n264" to="n265">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n265" to="n265">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n265" to="n265">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n266" to="n267">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n266" to="n266">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n266" to="n269">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n266" to="n268">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n267" to="n267">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n267" to="n267">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n268" to="n268">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n268" to="n268">
            <attr name="label">
                <string>let:name = &quot;inventory&quot;</string>
            </attr>
        </edge>
        <edge from="n269" to="n269">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n270" to="n271">
            <attr name="label">
                <string>variable</string>
            </attr>
        </edge>
        <edge from="n270" to="n272">
            <attr name="label">
                <string>to_action</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>type:InitialState</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>let:procedure = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n270" to="n270">
            <attr name="label">
                <string>let:class = &quot;PRODUCER&quot;</string>
            </attr>
        </edge>
        <edge from="n271" to="n271">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n271" to="n271">
            <attr name="label">
                <string>type:Variable</string>
            </attr>
        </edge>
        <edge from="n272" to="n275">
            <attr name="label">
                <string>to_state</string>
            </attr>
        </edge>
        <edge from="n272" to="n273">
            <attr name="label">
                <string>target</string>
            </attr>
        </edge>
        <edge from="n272" to="n274">
            <attr name="label">
                <string>source</string>
            </attr>
        </edge>
        <edge from="n272" to="n272">
            <attr name="label">
                <string>type:ActionAssignment</string>
            </attr>
        </edge>
        <edge from="n273" to="n273">
            <attr name="label">
                <string>let:name = &quot;Result&quot;</string>
            </attr>
        </edge>
        <edge from="n273" to="n273">
            <attr name="label">
                <string>type:LocalExpression</string>
            </attr>
        </edge>
        <edge from="n274" to="n274">
            <attr name="label">
                <string>type:AttributeExpression</string>
            </attr>
        </edge>
        <edge from="n274" to="n274">
            <attr name="label">
                <string>let:name = &quot;value&quot;</string>
            </attr>
        </edge>
        <edge from="n275" to="n275">
            <attr name="label">
                <string>type:FinalState</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>let:root_class = &quot;APPLICATION&quot;</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>let:root_procedure = &quot;make&quot;</string>
            </attr>
        </edge>
        <edge from="n276" to="n276">
            <attr name="label">
                <string>type:Initialization</string>
            </attr>
        </edge>
    </graph>
</gxl>
