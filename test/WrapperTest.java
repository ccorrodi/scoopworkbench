/**
 * Original old testing setup. Obsolete.
 */

import configuration.WrapperTestConfiguration;
import groove.lts.GTS;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import wrapper.exploration.GrooveExploration;
import wrapper.exploration.GrooveExplorationBuilder;
import wrapper.input.Renderer;
import wrapper.input.Strategy;
import wrapper.result.ExplorationRun;
import wrapper.result.ExplorationRunSet;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BinaryOperator;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class WrapperTest {
    private final WrapperTestConfiguration argument;
    private static WrapperTestConfiguration initializedArgument;
    private static boolean explorationSucceeded;

    private final Path gpsFolder = Paths.get("scoop.gps");
    private static boolean initialized;

    private static GTS generatedGts;
    private static ExplorationRun generatedRun;

    public WrapperTest(WrapperTestConfiguration argument) {
        this.argument = argument;
    }

    /**
     * Create object arrays with elements corresponding to the constructor parameters.
     * @return
     */
    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> collect() {
        final List<Object[]> result = new ArrayList<>();

        List<WrapperTestConfiguration> tests = new ArrayList<WrapperTestConfiguration>();
        /*
        List<configuration.WrapperTestConfiguration> tests = new ArrayList<configuration.WrapperTestConfiguration>(Arrays.asList(

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_eager", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(4_000, 6_000)
                        .withApplicationBounds(50_000, 60_000)
                        .withInternalStateBounds(30_000, 60_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_eager", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(5_600, 6_000)
                        .withApplicationBounds(60_000, 80_000)
                        .withInternalStateBounds(45_000, 50_000)
                        .withFinalStates(15)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_lazy", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(5_600, 6_000)
                        .withApplicationBounds(60_000, 80_000)
                        .withInternalStateBounds(45_000, 50_000)
                        .withFinalStates(3)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_lazy", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(9_500, 10_000)
                        .withApplicationBounds(120_000, 130_000)
                        .withInternalStateBounds(80_000, 85_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_producer_consumer_5", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(4_000, 4_100)
                        .withApplicationBounds(50_000, 52_000)
                        .withInternalStateBounds(30_000, 35_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_producer_consumer_5", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(12_000, 13_000)
                        .withApplicationBounds(150_000, 160_000)
                        .withInternalStateBounds(100_000, 110_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_producer_consumer_20", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(12_000, 13_000)
                        .withApplicationBounds(150_000, 170_000)
                        .withInternalStateBounds(100_000, 110_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_producer_consumer_20", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(50_000, 51_000)
                        .withApplicationBounds(620_000, 650_000)
                        .withInternalStateBounds(410_000, 430_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_barbershop_2_1_1", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(38_000, 39_000)
                        .withApplicationBounds(480_000, 530_000)
                        .withInternalStateBounds(320_000, 330_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_barbershop_2_1_1", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(54_000, 55_000)
                        .withApplicationBounds(700_000, 720_000)
                        .withInternalStateBounds(460_000, 470_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_savages_1_2_1", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(35_000, 36_000)
                        .withApplicationBounds(440_000, 480_000)
                        .withInternalStateBounds(290_000, 300_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_savages_1_2_1", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(79_000, 80_000)
                        .withApplicationBounds(1_000_000, 1_100_000)
                        .withInternalStateBounds(650_000, 680_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_eager_no_commands", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(400, 500)
                        .withApplicationBounds(5_000, 7_000)
                        .withInternalStateBounds(3_000, 5_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_eager_no_commands", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(400, 500)
                        .withApplicationBounds(6_000, 7_000)
                        .withInternalStateBounds(3_000, 5_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_lazy_no_commands", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(800, 900)
                        .withApplicationBounds(11_000, 12_000)
                        .withInternalStateBounds(7_000, 8_000)
                        .withFinalStates(3)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_2_1_lazy_no_commands", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(900, 1_000)
                        .withApplicationBounds(11_000, 13_000)
                        .withInternalStateBounds(7_000, 9_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_eager_no_commands", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(3_000, 3_500)
                        .withApplicationBounds(40_000, 50_000)
                        .withInternalStateBounds(28_000, 30_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_eager_no_commands", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(3_000, 3_500)
                        .withApplicationBounds(40_000, 50_000)
                        .withInternalStateBounds(28_000, 32_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_lazy_no_commands", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(10_000, 11_000)
                        .withApplicationBounds(130_000, 150_000)
                        .withInternalStateBounds(90_000, 100_000)
                        .withFinalStates(4)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_lazy_no_commands", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(11_000, 12_000)
                        .withApplicationBounds(140_000, 160_000)
                        .withInternalStateBounds(90_000, 110_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_eager", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(99_000, 100_000)
                        .withApplicationBounds(1_200_000, 1_300_000)
                        .withInternalStateBounds(800_000, 900_000)
                        .withFinalStates(1)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_eager", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(199_000, 200_000)
                        .withApplicationBounds(2_500_000, 2_600_000)
                        .withInternalStateBounds(1_600_000, 1_800_000)
                        .withFinalStates(1332)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_lazy", SCOOPRuntime.RUNTIME_RQ)
                        .withStateBounds(170_000, 171_000)
                        .withApplicationBounds(2_000_000, 2_300_000)
                        .withInternalStateBounds(1_400_000, 1_500_000)
                        .withFinalStates(4)
                        .build(),

                new configuration.WrapperTestConfigurationBuilder("paper_dining_philosophers_3_1_lazy", SCOOPRuntime.RUNTIME_QOQ)
                        .withStateBounds(440_000, 450_000)
                        .withApplicationBounds(5_600_000, 5_800_000)
                        .withInternalStateBounds(3_700_000, 3_800_000)
                        .withFinalStates(1)
                        .build()
        ));
        */

        tests.forEach((test) -> result.add(new Object[] { test }));
        return result;
    }

    /**
     * Run the test for the current values set by the constructor.
     * @throws Exception
     */
    /*
    @Before
    public void explore() {
        try {
            if (argument.equals(initializedArgument)) {
                return;
            }
            explorationSucceeded = false;
            initializedArgument = argument;

            //GrooveExploration exploration = new GrooveExploration(parser.getParsedGpsFolder(), parser.getParsedStartGraph());
            GrooveExplorationBuilder builder = new GrooveExplorationBuilder(gpsFolder, argument.startGraph, Renderer.PLAINTEXT_RENDERER)
                    .withStrategy(Strategy.BFS)
                    .withRuntime(argument.scoop.runtime)
                    .withRepetitions(1)
                    .withWarmup(0)
                    .withTimeout(0);

            // Perform the exploration.
            GrooveExploration exploration = builder.build();
            exploration.perform();
            //exploration.render();

            ExplorationRunSet runs = exploration.getRuns();
            ExplorationRun run = exploration.getRuns().getFirst();
            WrapperTest.generatedRun = run;
            WrapperTest.generatedGts = run.getGts();

            explorationSucceeded = true;
        } catch (Exception e) {
            fail("Exception raised during exploration: " + e.toString());
        }
    }

    private <T> void assertFormatBinaryBoolean(String format, BinaryOperator op, T first, T second) {
        assertTrue(String.format(format, first, second), (boolean) op.apply(first, second));
    }

    private void assertLessOrEqual(String format, Comparable expected, Comparable value) {
        BinaryOperator<Comparable> op = (x, y) -> { return x.compareTo(y) <= 0; };
        assertFormatBinaryBoolean(format, op, expected, value);
    }

    private void assertGreaterOrEqual(String format, Comparable expected, Comparable value) {
        BinaryOperator<Comparable> op = (x, y) -> { return x.compareTo(y) >= 0; };
        assertFormatBinaryBoolean(format, op, expected, value);
    }

    @Test
    public void testRuleApplications() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value(s); testRuleApplications ignored.", argument.expectedMinApplications >= 0 && argument.expectedMaxApplications >= 0);
        assertLessOrEqual("Too few rule applications. Expected %s, actual %s.",
                this.argument.expectedMinApplications, generatedRun.getRawTransitions());
        assertGreaterOrEqual("Too much rule applications. Expected %s, actual %s.",
                this.argument.expectedMaxApplications, generatedRun.getRawTransitions());
    }

    @Test
    public void testStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testMinStates ignored.", argument.expectedMinStates >= 0 && argument.expectedMaxStates >= 0);
        assertLessOrEqual("Too few states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMinStates, generatedGts.getStateCount());
        assertGreaterOrEqual("Too much states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMaxStates, generatedGts.getStateCount());
    }

    public void testMinStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testMinStates ignored.", argument.expectedMinStates >= 0);
        assertLessOrEqual("Too few states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMinStates, generatedGts.getStateCount());
    }

    public void testMaxStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testMaxStates ignored.", argument.expectedMaxStates >= 0);
        assertGreaterOrEqual("Too much states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMaxStates, generatedGts.getStateCount());
    }

    @Test
    public void testInternalStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testInternalStates ignored.", argument.expectedMinInternalStates >= 0 && argument.expectedMaxInternalStates >= 0);
        assertLessOrEqual("Too few internal states generated (below minimum). Expected %s, actual: %s.",
                this.argument.expectedMinInternalStates, generatedRun.getRawStates());
        assertGreaterOrEqual("Too much internal states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMaxInternalStates, generatedRun.getRawStates());
    }

    public void testMinInternalStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testMinInternalStates ignored.", argument.expectedMinInternalStates >= 0);
        assertLessOrEqual("Too few internal states generated (below minimum). Expected %s, actual: %s.",
                this.argument.expectedMinInternalStates, generatedRun.getRawStates());
    }

    public void testMaxInternalStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testMaxInternalStates ignored.", argument.expectedMaxInternalStates >= 0);
        assertGreaterOrEqual("Too much internal states generated (below minimum). Expected %s, actual %s.",
                this.argument.expectedMaxInternalStates, generatedRun.getRawStates());
    }

//	@Test
//	public void testMinFinalNodes() {
//		Assume.assumeTrue("Negative value; test ignored.", expectedMinFinalNodes >= 0);
//		assertLessOrEqual("Too few final nodes generated (below minimum). Expected %s > %s.",
//				this.expectedMinFinalNodes, generatedGts.getFinalStateCount());
//	}
//
//	@Test
//	public void testMaxFinalNodes() {
//		Assume.assumeTrue("Negative value; test ignored.", expectedMaxFinalNodes >= 0);
//		assertGreaterOrEqual("Too much final nodes (below minimum). Expected %s < %s.",
//				this.expectedMaxFinalNodes, generatedGts());
//	}

    @Test
    public void testFinalStates() {
        assumeSuccessful();
        Assume.assumeTrue("Negative value; testFinalStates ignored.", argument.expectedFinalStates >= 0);
        assertEquals("Number of result states do not match.",
                this.argument.expectedFinalStates, generatedGts.getFinalStateCount());
    }
    */

    /**
     * Assume that we are testing on a successful exploration. Otherwise, the tests do not make sense
     * in that context and are thus ignored. A failure is generated in explore() itself if that's the
     * case, so the full test class will fail with one failure (from explore()) and the remaining tests
     * ignored.
     */
    private void assumeSuccessful() {
        Assume.assumeTrue("Exploration failed.", explorationSucceeded);
    }

}
