package graph;

public class GXLEdge {
    private final GXLNode source;
    private final GXLNode target;
    private final String label;

    public GXLEdge(GXLNode from, GXLNode to, String label) {
        this.source = from;
        this.target = to;
        this.label = label;

        from.addOutgoing(this);
        to.addIncoming(this);
    }

    public GXLNode source() { return source; }
    public GXLNode target() { return target; }
}
