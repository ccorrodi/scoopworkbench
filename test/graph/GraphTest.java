package graph;

import graph.GXLGraph;
import graph.GXLNode;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

public class GraphTest {
    @Test
    public void testGXLParsing() {
        File f = new File("test-data/graph/sample1.gxl");
        GXLGraph g = new GXLGraph(f);

        assertEquals(34, g.nodeCount());
        assertEquals(33, g.edgeCount());

        List<GXLNode> initialStates = g.nodesLabeled("type:InitialState", "let:class = string:\"APPLICATION\"", "let:procedure = string:\"make\"");
        assertEquals(1, initialStates.size());

        GXLNode initial = initialStates.get(0);
        assertEquals(8, initial.outgoingEdgeCount());
        assertEquals(1, initial.incomingEdgeCount());
    }
}
