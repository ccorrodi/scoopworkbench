package graph;

import groove.grammar.host.HostEdge;
import groove.grammar.host.HostGraph;
import groove.grammar.host.HostNode;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GXLGraph {
    HashMap<Integer, GXLNode> nodes = new HashMap<>();
    Set<GXLEdge> edges = new HashSet<>();

    public GXLGraph(HostGraph graph) {
        // todo: like with a gxl file, but use the given HostGraph
        graph.nodeSet();
        graph.edgeSet();

        // Nodes
        for (HostNode node : graph.nodeSet()) {
            int parsedId = parseId(node.toString());
            if (parsedId != -1) {
                GXLNode graphNode = new GXLNode(parsedId);
                graphNode.addLabel("type:" + node.getType().toString());
                nodes.put(parsedId, graphNode);
            }
        }

        // Edges
        for (HostEdge edge : graph.edgeSet()) {
            int sourceId = parseId(edge.source().toString());
            int targetId = parseId(edge.target().toString());
            String label = edge.label().toString();

            GXLNode from = nodes.get(sourceId);
            GXLNode to = nodes.get(targetId);

            if (sourceId == targetId) {
                nodes.get(sourceId).addLabel(label);
            }
            if (targetId != -1) {
                GXLEdge graphEdge = new GXLEdge(from, to, label);
                edges.add(graphEdge);
            } else {
                nodes.get(sourceId).addLabel(edge.label() + "=" + edge.target().toString());
            }
        }
    }



    public GXLGraph(File aGXLFile) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(aGXLFile);
            doc.getDocumentElement().normalize();
            XPath xpath = XPathFactory.newInstance().newXPath();

            XPathExpression expr;
            NodeList nl;

            // Nodes
            expr = xpath.compile("gxl/graph/node");
            nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);
                String id = node.getAttributes().getNamedItem("id").getTextContent();
                int parsedId = parseId(id);
                nodes.put(parsedId, new GXLNode(parsedId));
            }

            // Edges
            expr = xpath.compile("gxl/graph/edge");
            nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);

                // from / to
                String source = node.getAttributes().getNamedItem("from").getTextContent();
                String target = node.getAttributes().getNamedItem("to").getTextContent();

                // label
                String label = (String) xpath.compile("attr[@name='label']/string/text()").evaluate(node, XPathConstants.STRING);

                if (source.equals(target)) {
                    int id = parseId(source);
                    nodes.get(id).addLabel(label);
                } else {
                    GXLNode from = nodes.get(parseId(source));
                    GXLNode to = nodes.get(parseId(target));
                    GXLEdge edge = new GXLEdge(from, to, label);
                    edges.add(edge);
                }
            }
        } catch (IOException | SAXException | XPathExpressionException | ParserConfigurationException e) {
            edges = new HashSet<>();
            nodes = new HashMap<>();
        }
    }

    private int parseId(String id) {
        Matcher m = Pattern.compile("n(\\d+)").matcher(id);
        m.find();
        if (m.matches()) {
            return Integer.parseInt(m.group(1));
        } else {
            return -1;
        }
    }

    public int nodeCount() {
        return nodes.size();
    }

    public int edgeCount() {
        return edges.size();
    }

    public List<GXLNode> nodesLabeled(String... args) {
        LinkedList<GXLNode> out = new LinkedList<>();
        for (GXLNode n : nodes.values()) {
            boolean matches = true;
            for (String s : args) {
                if (!n.hasLabel(s)) {
                    matches = false;
                }
            }
            if (matches) out.add(n);
        }
        return out;
    }

    public boolean hasNodesLabeled(String... args) {
        return !nodesLabeled(args).isEmpty();
    }
}
