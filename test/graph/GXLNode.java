package graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class GXLNode {
    private final int id;
    private List<String> labels = new LinkedList<>();

    private final Set<GXLEdge> incoming = new HashSet<>();
    private final Set<GXLEdge> outgoing = new HashSet<>();

    public GXLNode(int id) {
        this.id = id;
    }

    public void addLabel(String label) {
        this.labels.add(label);
    }

    public void addOutgoing(GXLEdge edge) {
        this.outgoing.add(edge);
    }

    public void addIncoming(GXLEdge edge) {
        this.incoming.add(edge);
    }

    public boolean hasLabel(String s) {
        for (String l : labels) {
            if (l.equals(s)) return true;
        }
        return false;
    }

    public int outgoingEdgeCount() {
        return outgoing.size();
    }

    public int incomingEdgeCount() {
        return incoming.size();
    }

    public Set<GXLEdge> incomingEdges() {
        return incoming;
    }

    public Set<GXLEdge> outgoingEdges() {
        return outgoing;
    }

    @Override
    public String toString() {
        return "GXLNode(" + labels.get(0).toString() + ")";
    }
}
