package configuration;

import wrapper.exploration.GrooveExploration;
import wrapper.exploration.GrooveExplorationBuilder;
import wrapper.input.Renderer;
import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;

import java.nio.file.Path;

public class WrapperTestConfiguration {
    public String startGraph = "auto_explore";
    public SCOOPRuntime runtime = SCOOPRuntime.RUNTIME_QOQ;
    public int expectedMinStates = -1;
    public int expectedMaxStates = -1;
    public int expectedMinApplications = -1;
    public int expectedMaxApplications = -1;
    public int expectedMinInternalStates = -1;
    public int expectedMaxInternalStates = -1;
    public int expectedFinalStates = -1;
    public Path gpsFolder = null;

    public final int id;
    public int repetitions = 1;
    public int warmup = 0;
    public int timeout = 0;
    public Strategy strategy = Strategy.BFS;

    public WrapperTestConfiguration(int id, Path gpsFolder, String startGraph, SCOOPRuntime runtime) {
        this.id = id;
        this.startGraph = startGraph;
        this.gpsFolder = gpsFolder;
        this.runtime = runtime;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof WrapperTestConfiguration && ((WrapperTestConfiguration) o).id == id;
    }

    @Override
    public String toString() {
        return startGraph + " " + runtime.toString();
    }

    public GrooveExploration buildExploration() {
        return new GrooveExplorationBuilder(gpsFolder, startGraph, Renderer.PLAINTEXT_RENDERER)
                .withStrategy(Strategy.BFS)
                .withRuntime(runtime)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
    }

    public void statesBetween(int min, int max) {
        expectedMinStates = min;
        expectedMaxStates = max;
    }

    public void internalStatesBetween(int min, int max) {
        expectedMinInternalStates = min;
        expectedMaxInternalStates = max;
    }
}
