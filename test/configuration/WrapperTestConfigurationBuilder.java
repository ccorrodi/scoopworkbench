package configuration;

import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;

import java.nio.file.Path;

public class WrapperTestConfigurationBuilder {
    private static int currentArgumentId = 0;

    WrapperTestConfiguration argument;

    public WrapperTestConfigurationBuilder(String startGraph, Path path, SCOOPRuntime runtime) {
        argument = new WrapperTestConfiguration(currentArgumentId, path, startGraph, runtime);
        currentArgumentId++;
    }

    public WrapperTestConfigurationBuilder withStartGraph(String graph) {
        argument.startGraph = graph;
        return this;
    }


    public WrapperTestConfigurationBuilder withRuntime(SCOOPRuntime runtime) {
        argument.runtime = runtime;
        return this;
    }


    public WrapperTestConfigurationBuilder withStateBounds(int min, int max) {
        argument.expectedMinStates = min;
        argument.expectedMaxStates = max;
        return this;
    }

    public WrapperTestConfigurationBuilder withInternalStateBounds(int min, int max) {
        argument.expectedMinInternalStates = min;
        argument.expectedMaxInternalStates = max;
        return this;
    }

    public WrapperTestConfigurationBuilder withApplicationBounds(int min, int max) {
        argument.expectedMinApplications = min;
        argument.expectedMaxApplications = max;
        return this;
    }

    public WrapperTestConfigurationBuilder withFinalStates(int states) {
        argument.expectedFinalStates = states;
        return this;
    }

    public WrapperTestConfiguration build() {
        return argument;
    }

    public WrapperTestConfigurationBuilder withStrategy(Strategy strategy) {
        argument.strategy = strategy;
        return this;
    }

    public WrapperTestConfigurationBuilder withGpsFolder(Path folder) {
        argument.gpsFolder = folder;
        return this;
    }

    public WrapperTestConfigurationBuilder withRepetitions(int i) {
        argument.repetitions = i;
        return this;
    }

    public WrapperTestConfigurationBuilder withWarmup(int i) {
        argument.warmup = i;
        return this;
    }

    public WrapperTestConfigurationBuilder withTimeout(int i) {
        argument.timeout = i;
        return this;
    }
}
