package scoop;

import configuration.WrapperTestConfiguration;
import configuration.WrapperTestConfigurationBuilder;
import graph.GXLGraph;
import groove.lts.GTS;
import groove.lts.GraphState;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import wrapper.exploration.GrooveExploration;
import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;
import wrapper.result.ExplorationRun;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BinaryOperator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public abstract class ScoopTestShared {
    protected final Path gpsFolder = Paths.get("scoop.gps");
    protected GrooveExploration exploration;
    protected ExplorationRun run;
    protected GTS gts;
    protected WrapperTestConfiguration testConfiguration;

    //******************
    // Utility
    //******************

    /**
     * Set the fields exploration, run, and gts for the given configuration.
     * @param testConfiguration
     */
    public void runAndSetFields(WrapperTestConfiguration testConfiguration) {
        this.testConfiguration = testConfiguration;
        exploration = performWith(testConfiguration);
        run = exploration.getRuns().getFirst();
        gts = run.getGts();
    }


    public void requireOverride() {
        assertTrue("Test method should be overridden.", isOverridden());
    }

    private GrooveExploration performWith(WrapperTestConfiguration testConfiguration) {
        GrooveExploration exploration = testConfiguration.buildExploration();
        exploration.perform();
        exploration.render();
        return exploration;
    }

    //*******************************************************
    // Subclass setup
    //*******************************************************

    protected abstract SCOOPRuntime runtime();
    protected boolean isOverridden() { return false; }

    //*******************************************************
    // Helpers
    //*******************************************************

    private void testFinalStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testFinalStates ignored.", config.expectedFinalStates >= 0);
        assertEquals("Number of result states do not match.",
                config.expectedFinalStates, run.getGts().getFinalStateCount());
    }

    private void testRuleApplications(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value(s); testRuleApplications ignored.", config.expectedMinApplications >= 0 && config.expectedMaxApplications >= 0);
        assertLessOrEqual("Too few rule applications. Expected %s, actual %s.",
                config.expectedMinApplications, run.getRawTransitions());
        assertGreaterOrEqual("Too much rule applications. Expected %s, actual %s.",
                config.expectedMaxApplications, run.getRawTransitions());
    }

    private void testStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testMinStates ignored.", config.expectedMinStates >= 0 && config.expectedMaxStates >= 0);
        assertLessOrEqual("Too few states generated (below minimum). Expected %s, actual %s.",
                config.expectedMinStates, run.getGts().getStateCount());
        assertGreaterOrEqual("Too much states generated (below minimum). Expected %s, actual %s.",
                config.expectedMaxStates, run.getGts().getStateCount());
    }


    public void assertStateCountBetween(int min, int max) {
        Assume.assumeTrue("Negative value; testMinStates ignored.", min >= 0);
        assertLessOrEqual("Too few states generated (below minimum). Expected %s, actual %s.",
                min, gts.getStateCount());
        assertGreaterOrEqual("Too much states generated (below minimum). Expected %s, actual %s.",
                max, gts.getStateCount());
    }

    private void testMinStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testMinStates ignored.", config.expectedMinStates >= 0);
        assertLessOrEqual("Too few states generated (below minimum). Expected %s, actual %s.",
                config.expectedMinStates, run.getGts().getStateCount());
    }

    private void testMaxStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testMaxStates ignored.", config.expectedMaxStates >= 0);
        assertGreaterOrEqual("Too much states generated (below minimum). Expected %s, actual %s.",
                config.expectedMaxStates, run.getGts().getStateCount());
    }

    private void testInternalStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testInternalStates ignored.", config.expectedMinInternalStates >= 0 && config.expectedMaxInternalStates >= 0);
        assertLessOrEqual("Too few internal states generated (below minimum). Expected %s, actual: %s.",
                config.expectedMinInternalStates, run.getRawStates());
        assertGreaterOrEqual("Too much internal states generated (below minimum). Expected %s, actual %s.",
                config.expectedMaxInternalStates, run.getRawStates());
    }

    private void testMinInternalStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testMinInternalStates ignored.", config.expectedMinInternalStates >= 0);
        assertLessOrEqual("Too few internal states generated (below minimum). Expected %s, actual: %s.",
                config.expectedMinInternalStates, run.getRawStates());
    }

    private void testMaxInternalStates(WrapperTestConfiguration config, ExplorationRun run) {
        Assume.assumeTrue("Negative value; testMaxInternalStates ignored.", config.expectedMaxInternalStates >= 0);
        assertGreaterOrEqual("Too much internal states generated (below minimum). Expected %s, actual %s.",
                config.expectedMaxInternalStates, run.getRawStates());
    }

    private <T> void assertFormatBinaryBoolean(String format, BinaryOperator op, T first, T second) {
        assertTrue(String.format(format, first, second), (boolean) op.apply(first, second));
    }

    private void assertLessOrEqual(String format, Comparable expected, Comparable value) {
        BinaryOperator<Comparable> op = (x, y) -> { return x.compareTo(y) <= 0; };
        assertFormatBinaryBoolean(format, op, expected, value);
    }

    private void assertGreaterOrEqual(String format, Comparable expected, Comparable value) {
        BinaryOperator<Comparable> op = (x, y) -> { return x.compareTo(y) >= 0; };
        assertFormatBinaryBoolean(format, op, expected, value);
    }

    public void assertGtsIsSuccessful(GTS gts) {
        boolean success = false;
        for (GraphState gs : gts.getFinalStates()) {
            GXLGraph g = new GXLGraph(gs.getGraph());
            if (g.hasNodesLabeled("type:Success")) {
                success = true;
                break;
            }
        }
        assertTrue("There are success nodes.", success);
    }

    protected void assertNotStuck(GTS gts) {
        boolean success = true;
        for (GraphState gs : gts.getFinalStates()) {
            GXLGraph g = new GXLGraph(gs.getGraph());
            if (!g.hasNodesLabeled("type:Success")) {
                success = true;
                break;
            }
        }
        assertTrue("All final states are successes.", success);
    }

    protected void assertSingleSuccessNode(GTS gts) {
        assertEquals("Exactly one final state.", 1, gts.getFinalStateCount());
        assertTrue("Final state is a success.",
                new GXLGraph(gts.getFinalStates().iterator().next().getGraph()).hasNodesLabeled("type:Success"));
    }



    protected void assertHasErrorWithMessage(GTS gts, String message) {
        GXLGraph graph = new GXLGraph(gts.getFinalStates().iterator().next().getGraph());
        assertTrue("Exploration should yield an error.", graph.hasNodesLabeled("type:Error", message));
    }

    protected void assertHasDeadlockError(GTS gts) {
        GXLGraph graph = new GXLGraph(gts.getFinalStates().iterator().next().getGraph());
        assertTrue("Exploration should yield a deadlock error.", graph.hasNodesLabeled("type:DeadlockError"));
    }

}
