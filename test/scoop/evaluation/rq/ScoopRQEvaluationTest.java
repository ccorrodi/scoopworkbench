package scoop.evaluation.rq;

import org.junit.Test;
import scoop.ScoopEvaluationTest;
import wrapper.input.SCOOPRuntime;

public class ScoopRQEvaluationTest extends ScoopEvaluationTest {
    @Override
    protected SCOOPRuntime runtime() {
        return SCOOPRuntime.RUNTIME_RQ;
    }

    @Override
    protected boolean isOverridden() {
        return true;
    }

    @Override
    public void testAllEtapsPaperDP21Lazy() {
        super.testAllEtapsPaperDP21Lazy();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP21LazyNoCommands() {
        super.testAllEtapsPaperDP21LazyNoCommands();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP31Lazy() {
        super.testAllEtapsPaperDP31Lazy();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP31LazyNoCommands() {
        super.testAllEtapsPaperDP31LazyNoCommands();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP41Lazy() {
        super.testAllEtapsPaperDP41Lazy();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP41LazyNoCommands() {
        super.testAllEtapsPaperDP41LazyNoCommands();
        assertHasDeadlockError(gts);
    }

    @Override
    public void testAllEtapsPaperDP21Eager() {
        super.testAllEtapsPaperDP21Eager();
        assertSingleSuccessNode(gts);
    }


    @Test
    public void testAllEtapsPaperDP31Eager() {
        super.testAllEtapsPaperDP31Eager();
        assertSingleSuccessNode(gts);
    }
}
