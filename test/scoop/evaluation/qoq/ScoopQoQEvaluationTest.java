package scoop.evaluation.qoq;

import configuration.WrapperTestConfiguration;
import configuration.WrapperTestConfigurationBuilder;
import org.junit.Test;
import scoop.ScoopEvaluationTest;
import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;

public class ScoopQoQEvaluationTest extends ScoopEvaluationTest {
    @Override
    protected SCOOPRuntime runtime() {
        return SCOOPRuntime.RUNTIME_QOQ;
    }

    @Override
    protected boolean isOverridden() {
        return true;
    }

    @Override
    public void testAllEtapsPaperDP31LazyNoCommands() {
        super.testAllEtapsPaperDP31LazyNoCommands();
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP21Lazy() {
        super.testAllEtapsPaperDP21Lazy();
        assertStateCountBetween(9_000, 10_000);
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP21LazyNoCommands() {
        super.testAllEtapsPaperDP21LazyNoCommands();
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP31Lazy() {
        super.testAllEtapsPaperDP31Lazy();
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP41Lazy() {
        super.testAllEtapsPaperDP41Lazy();
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP41LazyNoCommands() {
        super.testAllEtapsPaperDP41LazyNoCommands();
        assertSingleSuccessNode(gts);
    }

    @Override
    public void testAllEtapsPaperDP21Eager() {
        super.testAllEtapsPaperDP21Eager();
        assertHasErrorWithMessage(gts, "message=string:\"Mutual exclusion error. Both philosophers are in the eat method and have issued the use commands.\"");
    }

    @Test
    public void testAllEtapsPaperDP31Eager() {
        super.testAllEtapsPaperDP31Eager();
        assertHasErrorWithMessage(gts, "message=string:\"Mutual exclusion error. Both philosophers are in the eat method and have issued the use commands.\"");
    }
}
