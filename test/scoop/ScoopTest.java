package scoop;

import configuration.WrapperTestConfiguration;
import configuration.WrapperTestConfigurationBuilder;
import graph.GXLGraph;
import groove.lts.GTS;
import groove.lts.GraphState;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import wrapper.exploration.GrooveExploration;
import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;
import wrapper.result.ExplorationRun;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BinaryOperator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public abstract class ScoopTest extends ScoopTestShared {


    //*******************************************************
    // Tests that are executed with all runtimes, but have different outcomes.
    // E.g., programs usually generate more states when executed with QoQ as
    // opposed to RQ.
    //*******************************************************
    @Test
    public void testAeDiningPhilosophers() {
        requireOverride();
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_dining_philosophers", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
    }

    //*******************************************************
    // Shared tests are tests that should behave the same for all runtimes.
    // They do not need to be overriddenn, as we do not test for any scoop.runtime
    // specific things.
    //*******************************************************

    @Test
    public void testAllAeAssignments() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_assignments", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Ignore("broken")
    @Test
    public void testAllAeDscoopPaperWithdrawExample() {
        fail("broken");
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_dscoop_paper_withdraw_example", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertGtsIsSuccessful(gts);
        assertNotStuck(gts);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeSeparateCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_separate_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeSeparateQueries() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_separate_queries", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeLocalCommandsAndQueries() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_local_commands_and_queries", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeLocalCommandsAndQueriesWithSeparateKeyword() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_local_commands_and_queries_with_separate_keyword", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeContractsAndWaitconditions() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_contracts_and_waitconditions", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeCreationProcedures() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_creation_procedures", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeLocalCreationProcedures() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_local_creation_procedures", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

//    @Ignore("Ignored. Reason: Start graph not correct (b2.get_a.command statement).")
//    @Test
//    public void testAeNestedAndQualifiedCalls() {
//
//        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_nested_and_qualified_calls", gpsFolder, runtime())
//                .withStrategy(Strategy.BFS)
//                .withRepetitions(1)
//                .withWarmup(0)
//                .withTimeout(0)
//                .build();
//        runAndSetFields(testConfiguration);
//        assertSingleSuccessNode(gts);
//    }

    @Test
    public void testAeConditionals() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_conditionals", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeLoop() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_loop", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeArithmeticExpressions() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_arithmetic_expressions", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAeAssignmentsSeparate() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test_assignments_separate", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }
    /**
     * This is for the 'test' project folder which changes over time! Should rather copy a 'test' folder to a new
     * folder and fix a test.
     */
    @Test
    public void testAeTest() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("ae_test", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }
}