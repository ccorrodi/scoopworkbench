package scoop.runtime.qoq;

import configuration.WrapperTestConfiguration;
import configuration.WrapperTestConfigurationBuilder;
import graph.GXLEdge;
import graph.GXLGraph;
import graph.GXLNode;
import org.junit.Test;
import scoop.ScoopTest;
import wrapper.exploration.GrooveExploration;
import wrapper.input.SCOOPRuntime;
import wrapper.input.Strategy;
import wrapper.result.ExplorationRun;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScoopQoQTest extends ScoopTest {
    @Override
    protected SCOOPRuntime runtime() {
        return SCOOPRuntime.RUNTIME_QOQ;
    }

    @Override
    protected boolean isOverridden() {
        return true;
    }

    @Test
    public void testQoqInitializeRoot() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_qoq_initialize_root", gpsFolder, SCOOPRuntime.RUNTIME_QOQ)
                .withFinalStates(1)
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        GrooveExploration exploration = testConfiguration.buildExploration();
        exploration.perform();
        exploration.render();

        ExplorationRun run = exploration.getRuns().getFirst();

        // Create the graph
        GXLGraph graph = new GXLGraph(run.getGts().getFinalStates().iterator().next().getGraph());
        List<GXLNode> start = graph.nodesLabeled("type:InitialState", "class=string:\"APPLICATION\"", "procedure=string:\"make\"");
        assertEquals(1, start.size());
        for (GXLNode n : start) {
            assertEquals(1, n.incomingEdgeCount());
            for (GXLEdge e : n.incomingEdges()) {
                assertTrue(e.source().hasLabel("type:Processor"));
                assertTrue(e.source().hasLabel("flag:_token"));
            }
        }
        //checkConfigurationProperties(testConfiguration, run);
    }
}
