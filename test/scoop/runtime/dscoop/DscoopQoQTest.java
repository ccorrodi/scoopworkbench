package scoop.runtime.dscoop;

import scoop.ScoopTest;
import wrapper.input.SCOOPRuntime;

import static org.junit.Assert.fail;

public class DscoopQoQTest extends ScoopTest {
    @Override
    protected SCOOPRuntime runtime() {
        return SCOOPRuntime.RUNTIME_DSCOOP_QOQ;
    }

    @Override
    protected boolean isOverridden() {
        return true;
    }


    //*******************************************************
    // Tests can be overridden manually.
    //*******************************************************

    @Override
    public void testAllAeAssignments() {
        super.testAllAeAssignments();
    }

    //*******************************************************
    // Inherited tests that are mandatory to override.
    //*******************************************************


    @Override
    public void testAeDiningPhilosophers() {
        super.testAeDiningPhilosophers();
        fail("Not tested yet.");
    }
}