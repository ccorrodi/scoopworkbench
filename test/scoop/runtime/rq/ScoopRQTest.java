package scoop.runtime.rq;

import scoop.ScoopTest;
import wrapper.input.SCOOPRuntime;

public class ScoopRQTest extends ScoopTest {
    @Override
    protected SCOOPRuntime runtime() {
        return SCOOPRuntime.RUNTIME_RQ;
    }

    @Override
    protected boolean isOverridden() {
        return true;
    }

    @Override
    public void testAeDiningPhilosophers() {
        super.testAeDiningPhilosophers();
    }
}