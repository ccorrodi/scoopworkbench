package scoop;

import configuration.WrapperTestConfiguration;
import configuration.WrapperTestConfigurationBuilder;
import org.junit.Ignore;
import org.junit.Test;
import wrapper.input.Strategy;

public abstract class ScoopEvaluationTest extends ScoopTestShared {
    //**************************************************************************
    // ETAPS paper programs
    //
    // These include the start graphs from the ETAPS paper. However, the larger
    // ones are ignored (@Ignore) due to the long duration.
    //**************************************************************************

    @Test
    public void testAllEtapsPaperPC5() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_producer_consumer_5", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperPC20() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_producer_consumer_20", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertGtsIsSuccessful(gts);
        assertNotStuck(gts);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperDP21Eager() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_2_1_eager", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertGtsIsSuccessful(gts);
    }

    @Test
    public void testAllEtapsPaperDP21Lazy() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_2_1_lazy", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertGtsIsSuccessful(gts);
    }

    @Test
    public void testAllEtapsPaperDP31Eager() {
        requireOverride();
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_3_1_eager", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        // successful in RQ, mutex error in QoQ
    }

    @Test
    public void testAllEtapsPaperDP31Lazy() {
        requireOverride();
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_3_1_lazy", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        // This one should fail with RQ, but succeed with QoQ
    }

    @Ignore
    @Test
    public void testAllEtapsPaperDP41Eager() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_4_1_eager", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Ignore
    @Test
    public void testAllEtapsPaperDP41Lazy() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_4_1_lazy", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperDP21EagerNoCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_2_1_eager_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperDP21LazyNoCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_2_1_lazy_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertGtsIsSuccessful(gts);
    }

    @Test
    public void testAllEtapsPaperDP31EagerNoCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_3_1_eager_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperDP31LazyNoCommands() {

        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_3_1_lazy_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        // This one should fail with RQ, but succeed with QoQ
    }

    @Ignore
    @Test
    public void testAllEtapsPaperDP41EagerNoCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_4_1_eager_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Ignore
    @Test
    public void testAllEtapsPaperDP41LazyNoCommands() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_philosophers_4_1_lazy_no_commands", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperDiningSavages() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_dining_savages_1_2_1", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }

    @Test
    public void testAllEtapsPaperBarbershop() {
        WrapperTestConfiguration testConfiguration = new WrapperTestConfigurationBuilder("manual_test_etaps_paper_barbershop_2_1_1", gpsFolder, runtime())
                .withStrategy(Strategy.BFS)
                .withRepetitions(1)
                .withWarmup(0)
                .withTimeout(0)
                .build();
        runAndSetFields(testConfiguration);
        assertSingleSuccessNode(gts);
    }
}
