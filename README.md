# Scoop Workbench

## Setup

You need to place a folder `groove` containing the [GROOVE 
binaries](http://groove.cs.utwente.nl/) in the root directory. Then, simply 
use the gradle tasks to execute the program. Currently, the following tasks 
are present:

- `test`: Runs all exploration tests
- `testRuntime`: Runs the simple runtime tests corresponding to the sources in 
  `eiffel_test_sources`. These are simple programs and should be executable in 
  few minutes, making this task suitable for development and repeated 
  execution.
- `testEvaluation`: Runs the evaluation tests (programs as presented in the 
  FASE paper). These can take several hours to complete.
