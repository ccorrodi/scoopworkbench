package wrapper.input;

public enum Renderer {
    PLAINTEXT_RENDERER,
    XML_RENDERER,
    STATS_RENDERER
}