package wrapper.input;

public enum Strategy {
    BFS(false),
    DFS(false),
    LTL(true);

    private final boolean hasParameter;
    private String parameter;

    Strategy(boolean hasParameter) {
        this.hasParameter = hasParameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }

    public boolean hasParameter() {
        return this.hasParameter;
    }
}