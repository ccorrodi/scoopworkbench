package wrapper.input;

import exception.CommandLineParseException;
import exception.TodoException;
import org.apache.commons.cli.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * This parser class provides functionality for parsing command-line arguments given as
 * an array of Strings (see setUp() method). The fields are filled upon calling setUp()
 * and values are accessible from outside through various getters.
 */
public class GrooveWrapperCommandLineParser {
    private static final String LONG_OPT_CPM_DIR = "gps-folder";
    private static final String LONG_OPT_START_GRAPH = "start-graph";
    private static final String LONG_OPT_REPETITIONS = "repetitions";
    private static final String LONG_OPT_WARMUP = "warmup";
    private static final String LONG_OPT_TIMEOUT = "timeout";
    private static final String LONG_OPT_OUTPUT_RENDERER = "renderer";
    private static final String LONG_OPT_RUNTIME = "scoop.runtime";
    private static final String LONG_OPT_HELP = "help";

    private static final String GROOVE_LONG_OPT_GRAMMAR_PROPERTY = "grammar-property";
    private static final String GROOVE_LONG_OPT_ACCEPTOR = "acceptor";
    private static final String GROOVE_LONG_OPT_OUTPUT_FLAGS = "output-flags";
    private static final String GROOVE_LONG_OPT_RESULT_FILE_NAME = "result-state-file-name";
    private static final String GROOVE_LONG_OPT_LOG_DIR = "log-directory";
    private static final String GROOVE_LONG_OPT_LTS_OUTPUT_FILE = "lts-output-file-name";
    private static final String GROOVE_LONG_OPT_MAX_RESULT_STATES = "max-result-states";
    private static final String GROOVE_LONG_OPT_STRATEGY = "strategy";
    private static final String GROOVE_LONG_OPT_SPANNING = "save-spanning-only";
    private static final String GROOVE_LONG_OPT_TRACES = "save-traces-only";
    private static final String GROOVE_LONG_OPT_VERBOSITY = "verbosity";

    private static final int EXIT_CODE_FAIL = -1;

    // Command-line options, used for parsing and printing usage
    private Options options;

    // Store last/current CommandLine for easy access.
    private CommandLine lastCmd;

    /** Parsed arguments */

    private Path parsedGpsFolder;
    private Renderer parsedRenderer = Renderer.PLAINTEXT_RENDERER;
    private SCOOPRuntime parsedRuntime = SCOOPRuntime.RUNTIME_QOQ;

    private int parsedRepetitions = 1;
    private int parsedWarmup = 0;
    private int parsedTimeout = 0;

    public Path getParsedGpsFolder() {
        return parsedGpsFolder;
    }

    private String parsedStartGraph;

    public String getParsedStartGraph() {
        return parsedStartGraph;
    }

    private int parsedVerbosity = -1;

    public int getParsedVerbosity() {
        return parsedVerbosity;
    }

    /**
     * Constructor that sets up the parser using the given arguments.
     * @param args
     */
    public GrooveWrapperCommandLineParser(String[] args) throws CommandLineParseException {
        setUp(args);
    }

    /**
     * Empty constructor. When using this constructor, the first
     * call on the object should be a call of the setUp method.
     */
    public GrooveWrapperCommandLineParser() {}

    public void setUp(String[] args) throws CommandLineParseException {
        options = new Options();

        // --gps specifies the folder representing a CPM implementation in GROOVE
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_CPM_DIR)
                .hasArg()
                .withArgName("directory")
                .withDescription("Path to CPM implementation.")
                .create()
        );

        // --start-graph specifies the file that contains the start graph
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_START_GRAPH)
                .hasArg()
                .withArgName("file")
                .withDescription("Path to CPM start graph file.")
                .create()
        );

        // --output-renderer specifies the renderer to be used; possible values are text, xml, stats
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_OUTPUT_RENDERER)
                .hasArg()
                .withArgName("renderer")
                .withDescription("Output renderer. Possible values: text, xml, stats.")
                .create());

        // --repetitions specifies the number of times the exploration should be repeated (useful for generating stats)
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_REPETITIONS)
                .hasArg()
                .withArgName("repetitions")
                .withDescription("Number of repetitions.")
                .create());

        // --warmup specifies the number of times the exploration should be run before the main repetitions start
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_WARMUP)
                .hasArg()
                .withArgName("warmup")
                .withDescription("Number of warmup runs.")
                .create());

        // --timeout in seconds
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_TIMEOUT)
                .hasArg()
                .withArgName("timeout")
                .withDescription("Seconds before exploration is aborted.")
                .create());

        // --scoop.runtime specifies the scoop.runtime semantics that should be used; can be qoq or rq.
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_RUNTIME)
                .hasArg()
                .withArgName("scoop.runtime")
                .withDescription("Runtime to be used, either qoq or rq.")
                .create());

        // --help: print help text
        options.addOption(OptionBuilder
                .withLongOpt(LONG_OPT_HELP)
                .hasArg(false)
                .withDescription("Print help text.")
                .create()
        );

//		/**
//		 * Mapping of original GROOVE parameters
//		 */
//		// -D: set grammar property, allowed settings are
//		// checkIsomorphism=<boolean> and
//		// controlProgram=<names>
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_GRAMMAR_PROPERTY)
//						.withValueSeparator('=')
//						.hasArg()
//						.create('D')
//		);
//
//		// -a: set the acceptor
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_ACCEPTOR)
//						.hasArg()
//						.create('D')
//		);
//
//		// -ef: flags for the "-o" option, legal values:
//		// s - label for start state, default 'start'
//		// f - label final states, default 'final'
//		// o - label open states, default 'open'
//		// n - label states with number, default 's#', '#' replaced by number
//		// t - include transient states, default 't#', '#' replaced by depth
//		// r - result state label, default 'result'
//		// TODO: do we want this one?
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_OUTPUT_FLAGS)
//						.hasArg()
//						.create("ef")
//		);
//
//		// -f: save result states in separate files. mandatory '#' in file name,
//		//     which then gets replaced by the state number. extension is optional,
//		//     defaults to '.gst'
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_RESULT_FILE_NAME)
//						.hasArg()
//						.create('f')
//		);
//
//		// -l: log directory
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_LOG_DIR)
//						.hasArg()
//						.create('l')
//		);
//
//		// -o: save the generated LTS to a file with given name, '#' is instantiated
//		//     with the grammar ID. see also "-ef" option.
//		//     Extension not required, defaults to ".gxl".
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_LTS_OUTPUT_FILE)
//						.hasArg()
//						.create('o')
//		);
//
//		// -r: Stop exploration after given number of result states, defaults to 0 for "unbounded".
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_MAX_RESULT_STATES)
//						.hasArg()
//						.create('r')
//		);
//
//		// -s: Set strategy to given value. Valid values are:
//		//     bfs, dfs, linear, random, state, rete, retelinear, reterandom, crule:[!]id, cnbound:n,
//		//     "cebound:id>n,...", ltl:prop, "ltlbounded:idn,...;prop", "ltlpocket:idn,...;prop", remote:host
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_STRATEGY)
//						.hasArg()
//						.create('s')
//		);
//
//		// -spanning: boolean, if switched on, only the spanning tree of the LTS will be saved
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_SPANNING)
//						.create("spanning")
//		);
//
//		// -traces: boolean, if switched on, only the result traces of the LTS will be saved (which may be only
//		//          the start state, if there are no result states). Overrides -spanning if both are given.
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_TRACES)
//						.create("traces")
//		);
//
//		// -v: Set verbosity level, range = 0 to 2, default = 1.
//		options.addOption(OptionBuilder
//						.withLongOpt(GROOVE_LONG_OPT_VERBOSITY)
//						.hasArg()
//						.create('v')
//		);

        try {
            CommandLineParser parser = new BasicParser();
            CommandLine cmd = parser.parse(options, args);
            this.lastCmd = cmd;
        } catch (ParseException e) {
            System.out.println(e.getLocalizedMessage());
            fail();
        }
    }

    public void process() throws CommandLineParseException {
        /** Check parameter values and store them in appropriate fields.
         * On error, print messages and generic help text.
         */
        if (lastCmd.hasOption(LONG_OPT_HELP)) {
            printUsage();
            System.exit(0);
        }

        parseGpsDir();
        parseStartGraph();
        parseOutputRenderer();
        parseRepetitions();
        parseWarmup();
        parseRuntime();
        parseTimeout();

        // The GROOVE CLI provides additional parameters. We do not offer them since they are on a lower level of
        // abstraction, but keep them here for completeness. In future versions, we might want to use some of them,
        // or rather provide additional parameters and options that set certain values.
//		parseGrooveGrammarProperties();
//		parseGrooveAcceptor();
//		parseGrooveOutputFlags();
//		parseGrooveResultFileName();
//		parseGrooveLogDir();
//		parseGrooveLtsOutputFile();
//		parseGrooveMaxStates();
//		parseGrooveStrategy();
//		parseGrooveSpanning();
//		parseGrooveTraces();
//		parseGrooveVerbosity();
    }

	/*
	GROOVE parameters currently ignored, but we may want to cover them in future versions.

	private void parseGrooveVerbosity() throws CommandLineParseException {
		//GROOVE_LONG_OPT_VERBOSITY;
		checkHasAtMostOnce(GROOVE_LONG_OPT_VERBOSITY);
		if (hasAtLeastOne(GROOVE_LONG_OPT_VERBOSITY)) {
			try {
				int verbosity = Integer.parseInt(lastCmd.getOptionValue(GROOVE_LONG_OPT_VERBOSITY));
				if (verbosity >= 0 && verbosity <= 2) {
					parsedVerbosity = verbosity;
				} else {
					fail();
				}
			} catch (NumberFormatException e) {
				fail();
			}
		}
	}

	private void parseGrooveTraces() throws CommandLineParseException {
		//GROOVE_LONG_OPT_TRACES;
		checkHasAtMostOnce(GROOVE_LONG_OPT_TRACES);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_TRACES) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_TRACES);
		}
	}

	private void parseGrooveSpanning() throws CommandLineParseException {
		//GROOVE_LONG_OPT_SPANNING;
		checkHasAtMostOnce(GROOVE_LONG_OPT_SPANNING);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_SPANNING) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_SPANNING);
		}
	}

	private void parseGrooveStrategy() throws CommandLineParseException {
		//GROOVE_LONG_OPT_STRATEGY;
		checkHasAtMostOnce(GROOVE_LONG_OPT_STRATEGY);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_STRATEGY) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_STRATEGY);
		}
	}

	private void parseGrooveMaxStates() throws CommandLineParseException {
		//GROOVE_LONG_OPT_MAX_STATES;
		checkHasAtMostOnce(GROOVE_LONG_OPT_MAX_RESULT_STATES);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_MAX_RESULT_STATES) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_MAX_RESULT_STATES);
		}
	}

	private void parseGrooveLtsOutputFile() throws CommandLineParseException {
		//GROOVE_LONG_OPT_LTS_OUTPUT_FILE;
		checkHasAtMostOnce(GROOVE_LONG_OPT_LTS_OUTPUT_FILE);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_LTS_OUTPUT_FILE) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_LTS_OUTPUT_FILE);
		}
	}

	private void parseGrooveLogDir() throws CommandLineParseException {
		//GROOVE_LONG_OPT_LOG_DIR;
		checkHasAtMostOnce(GROOVE_LONG_OPT_LOG_DIR);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_LOG_DIR) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_LOG_DIR);
		}
	}

	private void parseGrooveResultFileName() throws CommandLineParseException {
		//GROOVE_LONG_OPT_RESULT_FILE_NAME;
		checkHasAtMostOnce(GROOVE_LONG_OPT_RESULT_FILE_NAME);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_RESULT_FILE_NAME) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_RESULT_FILE_NAME);
		}
	}

	private void parseGrooveOutputFlags() throws CommandLineParseException {
		//GROOVE_LONG_OPT_OUTPUT_FLAGS;
		checkHasAtMostOnce(GROOVE_LONG_OPT_OUTPUT_FLAGS);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_OUTPUT_FLAGS) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_OUTPUT_FLAGS);
		}
	}

	private void parseGrooveAcceptor() throws CommandLineParseException {
		//GROOVE_LONG_OPT_ACCEPTOR;
		checkHasAtMostOnce(GROOVE_LONG_OPT_ACCEPTOR);
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_ACCEPTOR) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_ACCEPTOR);
		}
	}

	private void parseGrooveGrammarProperties() throws CommandLineParseException {
		//GROOVE_LONG_OPT_GRAMMAR_PROPERTY;
		if (lastCmd.getOptionValues(GROOVE_LONG_OPT_GRAMMAR_PROPERTY) != null) {
			throw new TodoException("not implemented: parsing GROOVE command line parameter " + GROOVE_LONG_OPT_GRAMMAR_PROPERTY);
		}
	}
	*/

    private void parseStartGraph() throws CommandLineParseException {
        //LONG_OPT_START_GRAPH;
        checkHasOption(LONG_OPT_START_GRAPH);
        checkHasAtMostOnce(LONG_OPT_START_GRAPH);
        this.parsedStartGraph = lastCmd.getOptionValue(LONG_OPT_START_GRAPH);
    }

    private void parseGpsDir() throws CommandLineParseException {
        //LONG_OPT_CPM_DIR;
        checkHasOption(LONG_OPT_CPM_DIR);
        checkHasAtMostOnce(LONG_OPT_CPM_DIR);
        this.parsedGpsFolder = Paths.get(lastCmd.getOptionValue(LONG_OPT_CPM_DIR));
        if (Files.notExists(this.parsedGpsFolder)) {
            System.out.printf("Specified %s path does not exist.\n", LONG_OPT_CPM_DIR);
            fail();
        } else if (!Files.isDirectory(this.parsedGpsFolder)) {
            System.out.printf("Specified gps path is not a folder.\n");
            fail();
        }
    }

    private void parseOutputRenderer() throws CommandLineParseException {
        // LONG_OPT_OUTPUT_RENDERER
        checkHasAtMostOnce(LONG_OPT_OUTPUT_RENDERER);
        if (lastCmd.hasOption(LONG_OPT_OUTPUT_RENDERER)) {
            switch (lastCmd.getOptionValue(LONG_OPT_OUTPUT_RENDERER)) {
                case "text":
                    this.parsedRenderer = Renderer.PLAINTEXT_RENDERER;
                    break;
                case "xml":
                    this.parsedRenderer = Renderer.XML_RENDERER;
                    break;
                case "stats":
                    this.parsedRenderer = Renderer.STATS_RENDERER;
                    break;
                default:
                    System.out.printf("Invalid renderer: %s\n", lastCmd.getOptionValue(LONG_OPT_OUTPUT_RENDERER));
                    fail();
            }
        }
    }

    private void parseRepetitions() throws CommandLineParseException {
        // LONG_OPT_REPETITIONS
        checkHasAtMostOnce(LONG_OPT_REPETITIONS);
        if (lastCmd.hasOption(LONG_OPT_REPETITIONS)) {
            this.parsedRepetitions = Integer.parseInt(lastCmd.getOptionValue(LONG_OPT_REPETITIONS));
        }
    }

    private void parseWarmup() throws CommandLineParseException {
        // LONG_OPT_WARMUP
        checkHasAtMostOnce(LONG_OPT_WARMUP);
        if (lastCmd.hasOption(LONG_OPT_WARMUP)) {
            this.parsedWarmup = Integer.parseInt(lastCmd.getOptionValue(LONG_OPT_WARMUP));
        }
    }

    private void parseTimeout() throws CommandLineParseException {
        // LONG_OPT_TIMEOUT
        checkHasAtMostOnce(LONG_OPT_TIMEOUT);
        if (lastCmd.hasOption(LONG_OPT_TIMEOUT)) {
            this.parsedTimeout = Integer.parseInt(lastCmd.getOptionValue(LONG_OPT_TIMEOUT));
        }
    }

    private void parseRuntime() throws CommandLineParseException {
        // LONG_OPT_RUNTIME
        checkHasAtMostOnce(LONG_OPT_RUNTIME);
        if (lastCmd.hasOption(LONG_OPT_RUNTIME)) {
            switch (lastCmd.getOptionValue(LONG_OPT_RUNTIME)) {
                case "qoq":
                    this.parsedRuntime = SCOOPRuntime.RUNTIME_QOQ;
                    break;
                case "rq":
                    this.parsedRuntime = SCOOPRuntime.RUNTIME_RQ;
                    break;
                default:
                    System.out.printf("Invalid scoop.runtime %s. Must be in {rq,qoq}.\n", lastCmd.getOptionValue(LONG_OPT_RUNTIME));
                    fail();
            }
        }
    }

    /**
     * Show help text.
     */
    public void printUsage() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(this.getClass().getSimpleName(), options);
    }

    /**
     * Check whether an option is specified at most once. If not, an error
     * message is printed and fail() is called.
     */
    private void checkHasAtMostOnce(String opt) throws CommandLineParseException {
        if (lastCmd.getOptionValues(opt) != null && lastCmd.getOptionValues(opt).length > 1) {
            System.err.printf("More than one --%s specified.\n", opt);
            fail();
        }
    }

    private boolean hasAtLeastOne(String opt) {
        return lastCmd.getOptionValues(opt) != null;
    }

    /**
     * Check whether the option is present (at least once).
     */
    private void checkHasOption(String opt) throws CommandLineParseException {
        if (lastCmd.getOptionValues(opt) == null) {
            System.err.printf("Missing option --%s.\n", opt);
            fail();
        }
    }

    /**
     * Generic reaction when something went wrong. The usage is printed and
     * the program terminated.
     */
    private void fail() throws CommandLineParseException {
        printUsage();
        throw new CommandLineParseException();
    }

    public Renderer getParsedRenderer() {
        return parsedRenderer;
    }

    public int getParsedRepetitions() {
        return parsedRepetitions;
    }

    public SCOOPRuntime getParsedRuntime() {
        return parsedRuntime;
    }

    public int getParsedWarmup() {
        return parsedWarmup;
    }

    public int getParsedTimeout() {
        return parsedTimeout;
    }
}
