package wrapper.input;

/**
 * Enum containing QoQ and RQ values.
 */
public enum SCOOPRuntime {
    RUNTIME_QOQ("QoQ"),
    RUNTIME_RQ("RQ"),
    RUNTIME_DSCOOP_QOQ("DSCOOP");

    String name;

    SCOOPRuntime(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
