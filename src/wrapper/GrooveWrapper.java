package wrapper;

import exception.CommandLineParseException;
import groove.explore.StrategyValue;
import wrapper.exploration.GrooveExploration;
import wrapper.exploration.GrooveExplorationBuilder;
import wrapper.input.GrooveWrapperCommandLineParser;
import wrapper.input.Strategy;
import wrapper.result.PlaintextOutputFormatter;
import wrapper.result.ResultSetRenderer;
import wrapper.result.XMLOutputFormatter;

/**
 * Command line interface and entry point.
 *
 * In the GROOVE source code, command-line things are handled in the following files:
 * - explore/Generator.java (main class)
 * - util/cli/GrooveCmdLineTool (base class of Generator)
 * - util/cli/GrooveCmdLineParser
 * The options themselves are defined in explore/Generator.java as annotations (using arg4j library).
 *
 * Also see "Generator.jar -h" for full list of arguments.
 *
 * The description from "Generator.jar -h" follows:
 ARGUMENTS
 grammar    : Grammar location (default extension .gps)
 start      : Start graph names (defined in grammar, no extension) or start graph files (extension
 .gst)
 output-renderer : Plaintext or XML renderer.

 OPTIONS
 -D key=val : Set grammar property <key> to <val>. Legal settings are:
 - checkIsomorphism=boolean - switch isomorphism checking on or off
 - controlProgram=names - set the control program(s) to be used
 See groove.grammar.GrammarProperties for other allowed key/value pairs
 -a acc     : Set the acceptor to <acc>. The acceptor determines when a state is counted as a
 result of the exploration. Legal values are:
 final      - When final (default)
 inv:[!]id  - If rule <id> is [not] applicable
 ruleapp:id - If there is an <id>-labelled transition
 formula:f  - If <f> holds (a boolean formula of rules separated by &, |, !)
 any        - Always (all states are results)
 cycle      - If the state starts a cycle
 none       - Never (no states are results)
 -ef flags  : Flags for the "-o" option. Legal values are:
 s - label start state (default: 'start')
 f - label final states (default: 'final')
 o - label open states (default: 'open')
 n - label state with number (default: 's#', '#' replaced by number)
 t - include transient states (label: 't#', '#' replaced by depth)
 r - result state label (default: 'result')
 Specify label to be used by appending flag with 'label' (single-quoted)
 -f file    : Save result states in separate files, with names derived from <file>, in which the
 mandatory '#' is instantiated with the state number. The optional extension
 determines the output format (default is .gst)
 -h         : Print this help message and exit
 -l dir     : Log the generation process in the directory <dir>
 -o file    : Save the generated LTS to a file with name derived from <file>, in which '#' is
 instantiated with the grammar ID. The "-ef"-option controls some additional state
 labels. The optional extension determines the output format (default is .gxl)
 -r num     : Stop exploration after <num> result states (default is 0 for "unbounded")
 -s strgy   : Set the exploration strategy to <strgy>. Legal values are:
 bfs         - Breadth-first Exploration
 dfs         - Depth-first Exploration
 linear      - Linear
 random      - Random linear
 state       - Single-State
 rete        - Rete-based DFS
 retelinear  - Rete-based Linear
 reterandom  - Rete-based Random Linear
 crule:[!]id - Conditional: stop when rule <id> [not] applicable
 cnbound:n   - Conditional: up to <n> nodes
 cebound:id>n,...
 - Conditional: up to <n> edges labelled <id>
 ltl:prop    - LTL Model Checking
 ltlbounded:idn,...;prop
 - Bounded LTL Model Checking
 ltlpocket:idn,...;prop
 - Pocket LTL Model Checking
 remote:host - Remote
 -spanning  : If switched on, only the spanning tree of the LTS will be saved
 -traces    : If switched on, only the result traces of the LTS will be saved (which may be only
 the start state, if there are no result states). Overrides -spanning if both are given
 -v level   : Set verbosity level (range = 0 to 2, default = 1)

 */
public class GrooveWrapper {
    public static void main(String[] args) {
        new GrooveWrapper().run(args);
    }

    public void run(String[] args) {
        GrooveWrapperCommandLineParser parser = new GrooveWrapperCommandLineParser();
        try {
            parser.setUp(args);
            parser.process();
        } catch (CommandLineParseException e) {
            System.exit(-1);
        }

        GrooveExplorationBuilder builder = new GrooveExplorationBuilder(parser.getParsedGpsFolder(), parser.getParsedStartGraph(), parser.getParsedRenderer())
                .withStrategy(Strategy.BFS)
                .withRuntime(parser.getParsedRuntime())
                .withRepetitions(parser.getParsedRepetitions())
                .withWarmup(parser.getParsedWarmup())
                .withTimeout(parser.getParsedTimeout());

        // Perform the exploration.
        GrooveExploration exploration = builder.build();
        exploration.perform();
        exploration.render();
    }
}
