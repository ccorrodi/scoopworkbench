package wrapper.result;

import groove.lts.GraphState;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ExplorationRunSet extends LinkedList<ExplorationRun> {
	private final String graphName;
	private final String runTimeName;

	public ExplorationRunSet(String graphName, String runTimeName) {
		this.graphName = graphName;
		this.runTimeName = runTimeName;
	}

	public ExplorationRunSet(List<ExplorationRun> runs, String graphName, String runTimeName) {
		this.graphName = graphName;
		this.runTimeName = runTimeName;
		this.addAll(runs);
	}
	
	public int meanConfigurations() {
		List<Integer> configurations = new LinkedList<>();
		for (ExplorationRun run : this) {
			configurations.add(run.getGts().getStateCount());
		}
		Collections.sort(configurations);
		return configurations.get(configurations.size() / 2);
	}

	public double meanMemoryUsageInGb() {
		List<Double> memoryUsages = new LinkedList<>();
		for (ExplorationRun run : this) {
			memoryUsages.add(run.getMemoryInBytes());
		}
		Collections.sort(memoryUsages);
		return memoryUsages.get(memoryUsages.size() / 2) / (1024.*1024.*1024);
	}

	public double stdDevMemoryUsageInGb() {
		List<Double> memoryUsages = new LinkedList<>();
		for (ExplorationRun run : this) {
			memoryUsages.add(run.getMemoryInBytes());
		}
		StandardDeviation stdDev = new StandardDeviation();
		return stdDev.evaluate(doubleListToDoubleArray(memoryUsages)) / (1024.*1024.*1024);
	}

	public double meanTime() {
		List<Double> times = new LinkedList<>();
		for (ExplorationRun run : this) {
			times.add(run.getExecutionTime());
		}
		Collections.sort(times);
		return times.get(times.size() / 2);
	}

	public double stdDevTime() {
		List<Double> times = new LinkedList<>();
		for (ExplorationRun run : this) {
			times.add(run.getExecutionTime());
		}
		StandardDeviation stdDev = new StandardDeviation();
		return stdDev.evaluate(doubleListToDoubleArray(times));
	}

	public int meanTransitions() {
		List<Integer> transitionCounts = new LinkedList<>();
		for (ExplorationRun run : this) {
			transitionCounts.add(new Integer(run.getRawTransitions()));
		}
		Collections.sort(transitionCounts);
		return transitionCounts.get(transitionCounts.size() / 2);
	}

	public int meanStates() {
		List<Integer> stateCounts = new LinkedList<>();
		for (ExplorationRun run : this) {
			stateCounts.add(new Integer(run.getRawStates()));
		}
		Collections.sort(stateCounts);
		return stateCounts.get(stateCounts.size() / 2);
	}

	private double[] doubleListToDoubleArray(List<Double> doubles) {
		double[] result = new double[doubles.size()];
		for (int i = 0; i < doubles.size(); i++) {
			result[i] = doubles.get(i);
		}
		return result;
	}

	private double[] longListToDoubleArray(List<Long> doubles) {
		double[] result = new double[doubles.size()];
		for (int i = 0; i < doubles.size(); i++) {
			result[i] = doubles.get(i);
		}
		return result;
	}

	private double[] intListToDoubleArray(List<Integer> doubles) {
		double[] result = new double[doubles.size()];
		for (int i = 0; i < doubles.size(); i++) {
			result[i] = doubles.get(i);
		}
		return result;
	}

	public int meanStartGraphNodeCount() {
		List<Integer> sizes = new LinkedList<>();
		for (ExplorationRun run : this) {
			sizes.add(run.getGts().startState().getGraph().nodeCount());
		}
		Collections.sort(sizes);
		return sizes.get(sizes.size() / 2);
	}

	public int meanStartGraphEdgeCount() {
		List<Integer> sizes = new LinkedList<>();
		for (ExplorationRun run : this) {
			sizes.add(run.getGts().startState().getGraph().edgeCount());
		}
		Collections.sort(sizes);
		return sizes.get(sizes.size() / 2);
	}

	public int meanFinalGraphNodeCount() {
		List<Integer> sizes = new LinkedList<>();
		for (ExplorationRun run : this) {
			for (GraphState state : run.getGts().getFinalStates()) {
				sizes.add(state.getGraph().nodeCount());
			}
		}
		Collections.sort(sizes);
		return sizes.get(sizes.size() / 2);
	}

	public int meanFinalGraphEdgeCount() {
		List<Integer> sizes = new LinkedList<>();
		for (ExplorationRun run : this) {
			for (GraphState state : run.getGts().getFinalStates()) {
				sizes.add(state.getGraph().edgeCount());
			}
		}
		Collections.sort(sizes);
		return sizes.get(sizes.size() / 2);
	}

	public String getGraphName() {
		return graphName;
	}

	public String getRunTimeName() {
		return runTimeName;
	}
}
