package wrapper.result;


public abstract class ExplorationResult {
	public abstract void accept(ExplorationResultOutputFormatter explorationResultOutputFormatter);
	public abstract String getKind();
	public abstract String getMessage();
	public abstract String prettyPrintType();
}
