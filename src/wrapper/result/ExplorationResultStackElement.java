package wrapper.result;

public class ExplorationResultStackElement {
	private final String featureName;
	private final String className;

	public ExplorationResultStackElement(String className, String featureName) {
		this.className = className;
		this.featureName = featureName;
	}

	public String getClassName() {
		return className;
	}

	public String getFeatureName() {
		return featureName;
	}
}
