package wrapper.result;

/**
 * Dummy exploration result.
 */
public class DummyExplorationResult extends ExplorationResult {
	private String message;

	public DummyExplorationResult(String message) {
		this.message = message;
	}

	@Override
	public void accept(ExplorationResultOutputFormatter explorationResultOutputFormatter) {
		explorationResultOutputFormatter.formatDummyExplorationResult(this);
	}

	@Override
	public String getKind() {
		return "DUMMY";
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String prettyPrintType() {
		return "Dummy error";
	}
}
