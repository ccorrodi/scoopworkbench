package wrapper.result;

import java.util.List;

/**
 *
 */
public class ExplorationResultStackTrace {
	private final int processorId;
	private final List<ExplorationResultStackElement> trace;

	public ExplorationResultStackTrace(int processorId, List<ExplorationResultStackElement> trace) {
		this.processorId = processorId;
		this.trace = trace;
	}

	public int getProcessorId() {
		return processorId;
	}

	public List<ExplorationResultStackElement> getTrace() {
		return trace;
	}
}
