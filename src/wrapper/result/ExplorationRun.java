package wrapper.result;

import groove.explore.Exploration;
import groove.lts.GTS;

import java.util.List;

public class ExplorationRun {
	private final GTS gts;
	private final Exploration exploration;
	private final List<ExplorationResult> results;
	private final int rawStates;
	private final int rawTransitions;
	private final double memoryInBytes;
	private final double executionTime;

	public ExplorationRun(Exploration exploration, List<ExplorationResult> results, int rawStates, int rawTransitions,
                          double memoryInBytes, double timeInSeconds) {
		this.exploration = exploration;
		this.gts = exploration.getGTS();
		this.results = results;
		this.rawStates = rawStates;
		this.rawTransitions = rawTransitions;
		this.executionTime = timeInSeconds;
		this.memoryInBytes = memoryInBytes;
	}

	public List<ExplorationResult> getResults() {
		return results;
	}

	public GTS getGts() {
		return gts;
	}

	public Exploration getExploration() {
		return exploration;
	}

	public int getRawTransitions() {
		return rawTransitions;
	}

	public int getRawStates() {
		return rawStates;
	}

	public double getMemoryInBytes() {
		return memoryInBytes;
	}

	public double getExecutionTime() {
		return executionTime;
	}

	/**
	 * Number of raw transitions (TODO: really?)
	 * @return
	 */
    public int transitionCount() {
		return gts.edgeSet().size();
    }
}