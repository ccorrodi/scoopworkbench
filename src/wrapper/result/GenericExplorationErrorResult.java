package wrapper.result;

public class GenericExplorationErrorResult extends ExplorationResult {
	private final String errorType;
	private final String message;

	public GenericExplorationErrorResult(String errorType, String message) {
		this.errorType = errorType;
		this.message = message;
	}

	@Override
	public void accept(ExplorationResultOutputFormatter explorationResultOutputFormatter) {
		explorationResultOutputFormatter.formatGenericExplorationErrorResult(this);
	}

	@Override
	public String getKind() {
		return errorType;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String prettyPrintType() {
		return "Dummy error";
	}
}
