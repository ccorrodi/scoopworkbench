package wrapper.result;

import groove.explore.ExplorationListener;

import java.util.List;

public interface OutputRenderer extends ExplorationResultOutputFormatter {
	ExplorationListener getExplorationReporter();
	void outputTimeoutMessage();
	void renderResults(List<ExplorationResult> results);
	String getLastRenderOutput();
	void renderRuns(ExplorationRunSet runs);
}
