package wrapper.result;

/**
 * An output formatter must provide functionality to process each ExplorationResult subtype.
 */
interface ExplorationResultOutputFormatter {
	void formatGenericResult(ExplorationResult result);
	void formatDummyExplorationResult(DummyExplorationResult result);
	void formatDeadlockExplorationResult(DeadlockExplorationResult deadlockExplorationResult);
	void formatSuccessExplorationResult(SuccessExplorationResult successExplorationResult);
	void formatGenericExplorationErrorResult(GenericExplorationErrorResult genericExplorationErrorResult);
}
