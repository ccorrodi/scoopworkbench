package wrapper.result;

import java.util.LinkedList;
import java.util.List;

/**
 * Class holding information about a deadlock.
 */
public class DeadlockExplorationResult extends ExplorationResult {
	private String message;

	public DeadlockExplorationResult(String message) {
		this.message = message;
		stackTraces = new LinkedList<>();
	}

	@Override
	public void accept(ExplorationResultOutputFormatter explorationResultOutputFormatter) {
		explorationResultOutputFormatter.formatDeadlockExplorationResult(this);
	}

	@Override
	public String getKind() {
		return "DEADLOCK";
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public String prettyPrintType() {
		return "Deadlock error";
	}

	private List<ExplorationResultStackTrace> stackTraces;

	public void addStackTrace(ExplorationResultStackTrace stackTrace) {
		stackTraces.add(stackTrace);
	}

	public List<ExplorationResultStackTrace> getStackTraces() {
		return stackTraces;
	}
}
