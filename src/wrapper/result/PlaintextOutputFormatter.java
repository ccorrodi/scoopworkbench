package wrapper.result;

import groove.explore.Exploration;
import groove.explore.ExplorationListener;
import groove.explore.util.ExplorationReporter;
import groove.lts.*;

import java.io.IOException;
import java.util.List;

public class PlaintextOutputFormatter implements OutputRenderer {
    private StringBuilder builder;
    private String lastOutput;
    private int currentResultNumber;

    public PlaintextOutputFormatter() {
        builder = new StringBuilder();
    }

    private void addResultHeader(ExplorationResult result) {
        builder.append(String.format("Result state %d:\n", currentResultNumber));
        //builder.append("\tType: " + result.prettyPrintType() + "\n");
        //builder.append("\tMessage: " + result.getMessage() + "\n");
    }

    private void addResultFooter() {
        builder.append("\n");
    }

    @Override
    public void formatGenericResult(ExplorationResult result) {
        addResultHeader(result);
        builder.append("\tType: " + result.getKind() + "\n");
        builder.append("\tMessage: " + result.getMessage() + "\n");
        addResultFooter();
    }

    @Override
    public void formatDummyExplorationResult(DummyExplorationResult result) {
        addResultHeader(result);
        builder.append("\tType: Success\n");
        builder.append("\tMessage: " + result.getMessage() + "\n");
        addResultFooter();
    }

    @Override
    public void formatDeadlockExplorationResult(DeadlockExplorationResult deadlockExplorationResult) {
        formatGenericResult(deadlockExplorationResult);
    }

    @Override
    public void formatSuccessExplorationResult(SuccessExplorationResult successExplorationResult) {
        addResultHeader(successExplorationResult);
        builder.append("\tType: Success\n");
        builder.append("\tMessage: " + successExplorationResult.getMessage() + "\n");
        addResultFooter();	}

    @Override
    public void formatGenericExplorationErrorResult(GenericExplorationErrorResult genericExplorationErrorResult) {
        addResultHeader(genericExplorationErrorResult);
        builder.append("\tType: " + genericExplorationErrorResult.getKind() + "\n");
        builder.append("\tMessage: " + genericExplorationErrorResult.getMessage() + "\n");
        addResultFooter();
    }

    @Override
    public void renderResults(List<ExplorationResult> results) {
        if (results.size() == 0) {
            builder.append("No errors found.\n");
        } else {
            builder.append("Exploration results:\n\n");

            currentResultNumber = 1;
            for (ExplorationResult result : results) {
                result.accept(this);
                currentResultNumber++;
            }
            builder.append("\nEnd of output.\n");
        }
    }

    @Override
    public String getLastRenderOutput() {
        return builder.toString();
    }

    @Override
    public void renderRuns(ExplorationRunSet runs) {
        if (runs.size() > 1)
            builder.append("Ran %d times. The following output is collected from the first run.\n");

        if (runs.size() == 0) {
            builder.append("No runs.\n");
        } else {
            ExplorationRun run = runs.getFirst();
            builder.append(String.format("Internal states: %,7d, Transitions: %,7d\n",
                    run.getRawStates(), run.getRawTransitions()));

            builder.append(String.format("Recipe   states: %,7d, Transitions: %,7d\n\n",
                    run.getGts().getStates().size(), run.getGts().getTransitionCount(), run.getGts().getStateCount()));
            //builder.append(String.format("Final states: %d\n", run.getGts().getFinalStateCount()));

            renderResults(run.getResults());
        }

		/*
		System.out.printf("Final states of first run: %d\n", runs.get(0).getGts().getFinalStateCount());

		System.out.printf("States & Transitions & Time (stddev) & Memory (stddev)\n");
		System.out.printf("%d & %d & %.2f (%.2f) & %.2f (%.2f)\n",
				runs.meanStates(), runs.meanTransitions(),
				runs.meanTime(), runs.stdDevTime(),
				runs.meanMemoryUsageInGb(), runs.stdDevMemoryUsageInGb());
		*/
    }

    private CounterListener counterListener = new CounterListener();

    class CounterListener implements GTSListener {
        int states = 0, transitions = 0, count = 0;

        @Override
        public void addUpdate(GTS gts, GraphState graphState) {
            states++;
            count++;
            maybePrint(gts);
        }

        @Override
        public void addUpdate(GTS gts, GraphTransition graphTransition) {
            transitions++;
            count++;
            maybePrint(gts);
        }

        private void maybePrint(GTS gts) {
            if ((states + transitions) % 1000 == 0 && !((states + transitions) % 10000 == 0)) {
                System.out.printf(".");
            } else if ((states + transitions) % 10000 == 0) {
                System.out.printf(" %,10d states, %,10d transitions, %,10d open states.\n",
                        states, transitions, gts.getOpenStateCount());
            }
        }

        @Override
        public void statusUpdate(GTS gts, GraphState graphState, Status.Flag flag, int i) {}
    };

    private ExplorationReporter explorationReporter = new ExplorationReporter() {
        @Override
        public void start(Exploration exploration, GTS gts) {
        }

        @Override
        public void stop(GTS gts) {
            System.out.printf("\n\n");
        }

        @Override
        public void abort(GTS gts) {
        }

        @Override
        public void report() throws IOException {}
    };

    @Override
    public ExplorationListener getExplorationReporter() {
        return explorationReporter;
    }

    @Override
    public void outputTimeoutMessage() {
        System.out.printf("\nTimeout reached.\n\n");
    }
}
