package wrapper.result;

public class SuccessExplorationResult extends ExplorationResult {
	private String message;

	public SuccessExplorationResult(String message) {
		this.message = message;
	}

	@Override
	public void accept(ExplorationResultOutputFormatter explorationResultOutputFormatter) {
		explorationResultOutputFormatter.formatSuccessExplorationResult(this);
	}

	@Override
	public String getKind() {
		return "DUMMY";
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String prettyPrintType() {
		return "Termination without errors";
	}
}
