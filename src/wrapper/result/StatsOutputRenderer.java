package wrapper.result;

import groove.explore.Exploration;
import groove.explore.ExplorationListener;
import groove.explore.util.ExplorationReporter;
import groove.lts.GTS;

import java.io.IOException;
import java.util.List;

public class StatsOutputRenderer implements OutputRenderer {
	private StringBuilder builder;
	private String lastOutput;
	private int currentResultNumber;

	public StatsOutputRenderer() {
		builder = new StringBuilder();
	}

	private void addResultHeader(ExplorationResult result) {
		builder.append(String.format("Exploration result %d:\n", currentResultNumber));
		builder.append("\tType: " + result.getClass().getSimpleName() + "\n");
		builder.append("\tMessage: " + result.getMessage() + "\n");
	}

	private void addResultFooter() {
		builder.append("\n\n");
	}

	@Override
	public void formatGenericResult(ExplorationResult result) {
		addResultHeader(result);
		addResultFooter();
	}

	@Override
	public void formatDummyExplorationResult(DummyExplorationResult result) {
		addResultHeader(result);
		builder.append("\tMessage: " + result.getMessage());
		addResultFooter();
	}

	@Override
	public void formatDeadlockExplorationResult(DeadlockExplorationResult deadlockExplorationResult) {
		formatGenericResult(deadlockExplorationResult);
	}

	@Override
	public void formatSuccessExplorationResult(SuccessExplorationResult successExplorationResult) {
		formatGenericResult(successExplorationResult);
	}

	@Override
	public void formatGenericExplorationErrorResult(GenericExplorationErrorResult genericExplorationErrorResult) {
		formatGenericResult(genericExplorationErrorResult);
	}

	@Override
	public void renderResults(List<ExplorationResult> results) {
		if (results.size() == 0) {
			builder.append("No results states generated.\n");
		} else {
			builder.append("Exploration results:\n\n");

			currentResultNumber = 1;
			for (ExplorationResult result : results) {
				result.accept(this);
				currentResultNumber++;
			}
			builder.append("End of output.\n");
		}
	}

	@Override
	public String getLastRenderOutput() {
		return builder.toString();
	}

	@Override
	public void renderRuns(ExplorationRunSet runs) {
		builder = new StringBuilder();
		builder.append("Start graph,Runtime,Configurations,Rule applications,Start graph nodes,Start graph edges,Final graph nodes,Final graph edges,Time,Memory\n");
		builder.append(String.format("%s,%s,%d,%d,%d,%d,%d,%d,%.2f,%.2f\n",
				runs.getGraphName(),
				runs.getRunTimeName(),
				runs.meanConfigurations(), runs.meanTransitions(),
				runs.meanStartGraphNodeCount(), runs.meanStartGraphEdgeCount(),
				runs.meanFinalGraphNodeCount(), runs.meanFinalGraphEdgeCount(),
				runs.meanTime(), runs.meanMemoryUsageInGb())
		);

		/*
		System.out.printf("Final states of first run: %d\n", runs.get(0).getGts().getFinalStateCount());

		System.out.printf("Start graph & Runtime & Configurations & Rule applications & Start graph size & Final graph size & Time (stddev) & Memory (stddev)\n");
		System.err.printf("%s & %s & %d & %d & %d/%d & %d/%d & %.2f (%.2f) & %.2f (%.2f)\\\\\n",
				runs.getGraphName().replace("_", "\\_"),
				runs.getRunTimeName(),
				runs.meanConfigurations(), runs.meanTransitions(),
				runs.meanStartGraphNodeCount(), runs.meanStartGraphEdgeCount(),
				runs.meanFinalGraphNodeCount(), runs.meanFinalGraphEdgeCount(),
				runs.meanTime(), runs.stdDevTime(),
				runs.meanMemoryUsageInGb(), runs.stdDevMemoryUsageInGb());
		*/
	}

	/*
	@Override
	public GTSListener getCounterListener() {
		return counterListener;
	}
	*/

	private ExplorationReporter explorationReporter = new ExplorationReporter() {
		@Override
		public void start(Exploration exploration, GTS gts) {
		}

		@Override
		public void stop(GTS gts) {
			System.out.printf("\n\n");
		}

		@Override
		public void abort(GTS gts) {
		}

		@Override
		public void report() throws IOException {
		}
	};

	@Override
	public ExplorationListener getExplorationReporter() {
		return explorationReporter;
	}

	@Override
	public void outputTimeoutMessage() {
		System.out.printf("\nTimeout reached.\n\n");
	}
}
