package wrapper.result;

import exception.TodoException;
import groove.explore.Exploration;
import groove.explore.ExplorationListener;
import groove.explore.util.ExplorationReporter;
import groove.lts.GTS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

/**
 * Output formatter that generates XML for easier postprocessing in other tools.
 */
public class XMLOutputFormatter implements OutputRenderer {
	private Document doc;
	private Element root;
	private int recipeStates;
	private int recipeTransitions;

	public XMLOutputFormatter() {
		reset();
	}

	@Override
	public void formatGenericResult(ExplorationResult result) {
		throw new TodoException("Generic result not implemented.");
	}

	@Override
	public void formatDummyExplorationResult(DummyExplorationResult result) {
		Element resultElement = doc.createElement("result");
		resultElement.setAttribute("type", result.getKind());
		root.appendChild(resultElement);

		addMessage(resultElement, result);
	}

	@Override
	public void formatDeadlockExplorationResult(DeadlockExplorationResult result) {
		Element resultElement = doc.createElement("result");
		resultElement.setAttribute("type", result.getKind());
		root.appendChild(resultElement);

		addMessage(resultElement, result);

		for (ExplorationResultStackTrace trace : result.getStackTraces()) {
			Element traceElement = newElement(resultElement, "stack_trace");
			Element processorId = newElement(traceElement, "processor_id");
			processorId.setTextContent(Integer.toString(trace.getProcessorId()));
		}
	}

	@Override
	public void formatSuccessExplorationResult(SuccessExplorationResult result) {
		Element resultElement = doc.createElement("result");
		resultElement.setAttribute("type", result.getKind());
		root.appendChild(resultElement);

		addMessage(resultElement, result);
	}

	@Override
	public void formatGenericExplorationErrorResult(GenericExplorationErrorResult genericExplorationErrorResult) {
		formatGenericResult(genericExplorationErrorResult);
	}

	private Element newElement(Element parent, String name) {
		Element element = doc.createElement(name);
		parent.appendChild(element);
		return element;
	}

	private void addMessage(Element parent, ExplorationResult result) {
		Element element;
		element = doc.createElement("message");
		element.setTextContent(result.getMessage());
		parent.appendChild(element);
	}

	private void reset() {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			//DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
			// Use domImplementation for creating a document with a specific dtd.
			doc = documentBuilder.newDocument();

			root = doc.createElement("root");
			doc.appendChild(root);
		} catch (ParserConfigurationException e) {

			e.printStackTrace();
		}

		// Example document creation that facilitates validation.
		/*
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		documentBuilderFactory.setValidating(true);
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
		DocumentType documentType = domImplementation.createDocumentType("doctype", "mydoctype", "http://www.gupro.de/GXL/gxl-1.0.dtd");
		Document doc = domImplementation.createDocument("http://www.gupro.de/GXL/gxl-1.0.dtd", "gxl", documentType);
		doc.setXmlStandalone(true);
		Element gxl = doc.getDocumentElement();
		Element graph = doc.createElementNS("http://www.gupro.de/GXL/gxl-1.0.dtd", "graph");
		graph.setAttribute("xmlns", "http://www.gupro.de/GXL/gxl-1.0.dtd");
		graph.setAttribute("role", "graph");
		graph.setAttribute("edgeids", "false");
		graph.setAttribute("edgemode", "directed");
		graph.setAttribute("id", "debug_translation");
		gxl.appendChild(graph);
		*/
	}

	@Override
	public void renderResults(List<ExplorationResult> results) {
		reset();

		for (ExplorationResult result : results) {
			result.accept(this);
		}
	}

	@Override
	public String getLastRenderOutput() {
		return getFormattedDocument();
	}

	@Override
	public void renderRuns(ExplorationRunSet runs) {
		System.out.printf("renderRuns TODO\n");
	}

	public String getFormattedDocument() {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

			StreamResult result = new StreamResult(new StringWriter());
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			String xmlString = result.getWriter().toString();

			return xmlString;
		} catch (javax.xml.transform.TransformerException e) {
			e.printStackTrace();
		}
		return "";
	}

	private ExplorationReporter explorationReporter = new ExplorationReporter() {
		@Override
		public void start(Exploration exploration, GTS gts) {
		}

		@Override
		public void stop(GTS gts) {
		}

		@Override
		public void abort(GTS gts) {
		}

		@Override
		public void report() throws IOException {}
	};

	@Override
	public ExplorationListener getExplorationReporter() {
		return explorationReporter;
	}

	@Override
	public void outputTimeoutMessage() {
		System.out.printf("Timeout reached.\n");
	}
}
