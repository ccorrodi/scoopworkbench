package wrapper.exploration;

import groove.explore.StrategyValue;
import wrapper.exploration.GrooveExploration;
import wrapper.input.Renderer;
import wrapper.input.SCOOPRuntime;
import wrapper.result.OutputRenderer;
import wrapper.result.PlaintextOutputFormatter;
import wrapper.result.StatsOutputRenderer;
import wrapper.result.XMLOutputFormatter;

import java.nio.file.Path;

/**
 * Class implementing the builder pattern for creating a GrooveExploration object.
 */
public class GrooveExplorationBuilder {
    private GrooveExploration exploration;

    /**
     * Initialize exploration with given gps folder and start graph name, the
     * only two mandatory parameters.
     * @param gpsFolder
     * @param startGraph
     */
    public GrooveExplorationBuilder(Path gpsFolder, String startGraph, Renderer renderer) {
        // Select renderer.
        OutputRenderer outputRenderer;
        switch (renderer) {
            case XML_RENDERER:
                outputRenderer = new XMLOutputFormatter();
                break;
            case PLAINTEXT_RENDERER:
                outputRenderer = new PlaintextOutputFormatter();
                break;
            case STATS_RENDERER:
                outputRenderer = new StatsOutputRenderer();
                break;
            default:
                outputRenderer = new PlaintextOutputFormatter();
                break;
        }

        exploration = new GrooveExploration(gpsFolder, startGraph, outputRenderer);
    }

    public wrapper.exploration.GrooveExplorationBuilder withStrategy(wrapper.input.Strategy strategy) {
        exploration.setStrategy(strategy);
        return this;
    }

    public wrapper.exploration.GrooveExplorationBuilder withParameterizedStrategy(StrategyValue strategyValue, String parameter) {
        exploration.setStrategyWithParameter(strategyValue, parameter);
        return this;
    }

    public GrooveExploration build() {
        return exploration;
    }

    public wrapper.exploration.GrooveExplorationBuilder withRepetitions(int repetitions) {
        exploration.setRepetitions(repetitions);
        return this;
    }

    public wrapper.exploration.GrooveExplorationBuilder withRuntime(SCOOPRuntime parsedRuntime) {
        exploration.setRuntime(parsedRuntime);
        return this;
    }

    public wrapper.exploration.GrooveExplorationBuilder withWarmup(int warmup) {
        exploration.setWarmup(warmup);
        return this;
    }

    public wrapper.exploration.GrooveExplorationBuilder withTimeout(int timeout) {
        exploration.setTimeout(timeout);
        return this;
    }
}
