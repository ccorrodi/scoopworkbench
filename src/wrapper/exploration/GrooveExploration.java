package wrapper.exploration;

import exception.TodoException;
import groove.explore.AcceptorValue;
import groove.explore.Exploration;
import groove.explore.ExploreType;
import groove.explore.StrategyValue;
import groove.explore.encode.Serialized;
import groove.explore.encode.Template;
import groove.explore.strategy.Strategy;
import groove.grammar.CheckPolicy;
import groove.grammar.Grammar;
import groove.grammar.GrammarProperties;
import groove.grammar.host.HostEdge;
import groove.grammar.host.HostNode;
import groove.grammar.model.GrammarModel;
import groove.lts.*;
import groove.util.parse.FormatException;
import wrapper.input.SCOOPRuntime;
import wrapper.result.*;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Class wrapping exploration of GROOVE systems.
 *
 * This class provides CPM/SCOOP specific functionality to initialize and run
 * a GROOVE state-space exploration. In addition, the original (generic)
 * command-line parameters should also be set using this class. This
 * way, GrooveExploration provides a single point for setting up an
 * exploration without having to access internal GROOVE objects for
 * fine-tuning.
 */
public class GrooveExploration {
    private final String startGraph;
    private final Path gpsFolder;
    private GTS gts;

    private Exploration exploration;

    private Serialized strategy;
    private Serialized acceptor;
    private int nrResults;

    private OutputRenderer renderer;
    private int repetitions;
    private int warmup;

    private ExplorationRunSet runs;
    private SCOOPRuntime scoopRuntime = SCOOPRuntime.RUNTIME_QOQ;
    private int timeout;

    GrooveExploration(Path gpsFolder, String startGraph, OutputRenderer renderer) {
        this.gpsFolder = gpsFolder;
        this.startGraph = startGraph;
        this.renderer = renderer;

        // Set defaults.
        setStrategy(wrapper.input.Strategy.BFS);
        setAcceptor(AcceptorValue.FINAL);
        nrResults = 0;
    }

    private void setAcceptor(AcceptorValue value) {
        this.acceptor = value.toSerialized();
    }

    void setStrategy(wrapper.input.Strategy strategy) {
        StrategyValue strategyValue = null;

        switch (strategy) {
            case BFS:
                strategyValue = StrategyValue.BFS;
                break;
            case DFS:
                strategyValue = StrategyValue.DFS;
                break;
            case LTL:
                strategyValue = StrategyValue.LTL;
                break;
            default:
                throw new TodoException("Strategy selection not implemented.");
        }

        Template<Strategy> strategyTemplate = strategyValue.getTemplate();
        if (strategy.hasParameter()) {
            this.strategy = strategyTemplate.toSerialized(strategy.getParameter());
        } else {
            this.strategy = strategyTemplate.toSerialized();
        }
    }

    void setStrategyWithParameter(StrategyValue strategyValue, String param) {
        Template<Strategy> strategyTemplate = strategyValue.getTemplate();
        strategy = strategyTemplate.toSerialized(param);
    }

    void setSpanning() {
        throw new TodoException("setSpanning");
    }

    void setTraces() {
        throw new TodoException("setTraces");
    }

    void setVerbosity() {
        throw new TodoException("setVerbosity");
    }

    private void setUpExploration() {
        try {
            GrammarModel model = GrammarModel.newInstance(gpsFolder.toFile(), false);
            GrammarProperties grammarProperties = model.getProperties();
            grammarProperties.setProperty("startGraph", startGraph);

            String baseControlPrograms = "control scoop_generic_recipes";
            switch (scoopRuntime) {
                case RUNTIME_QOQ:
                    grammarProperties.setProperty("controlProgram", baseControlPrograms + " scoop_control_qoq");
                    break;
                case RUNTIME_RQ:
                    grammarProperties.setProperty("controlProgram", baseControlPrograms + " scoop_control_rq");
                    break;
                case RUNTIME_DSCOOP_QOQ:
                    grammarProperties.setProperty("controlProgram", baseControlPrograms + " dscoop_control_qoq");
            }

            String typeGraphs = "errors meta scoop_actions scoop_bookkeeping scoop_bookkeeping_qoq scoop_bookkeeping_rq scoop_expressions_compact scoop_misc scoop_sync_storage_qoq_semantics scoop_sync_storage_rq_semantics dscoop_bookkeeping_qoq";
            grammarProperties.setProperty("typeGraph", typeGraphs);
            grammarProperties.setTypePolicy(CheckPolicy.ERROR);

            model.setProperties(grammarProperties);
            Grammar grammar = model.toGrammar();
            gts = new GTS(grammar);
            counterListener = new CounterListener();
            gts.addLTSListener(counterListener);
            exploration = new Exploration(ExploreType.DEFAULT, gts, gts.startState());
            //FIXME:exploration = new Exploration(strategy, acceptor, nrResults);
        } catch (IOException | FormatException e) {
            e.printStackTrace();
        }
    }

    private CounterListener counterListener;

    void setRuntime(SCOOPRuntime runtime) {
        this.scoopRuntime = runtime;
    }

    void setWarmup(int warmup) {
        this.warmup = warmup;
    }

    void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public ExplorationRunSet getRuns() {
        return runs;
    }

    private class CounterListener implements GTSListener {
        int states = 0, transitions = 0, count = 0;

        @Override
        public void addUpdate(GTS gts, GraphState graphState) {
            states++;
            count++;
            maybePrint(gts);
        }

        @Override
        public void addUpdate(GTS gts, GraphTransition graphTransition) {
            transitions++;
            count++;
            maybePrint(gts);
        }

        private void maybePrint(GTS gts) {
            if ((states + transitions) % 1000 == 0 && !((states + transitions) % 10000 == 0)) {
                System.out.printf(".");
            } else if ((states + transitions) % 10000 == 0) {
                System.out.printf(" %,10d states, %,10d transitions, %,10d open states.\n",
                        states, transitions, gts.getOpenStateCount());
            }
        }

        @Override
        public void statusUpdate(GTS gts, GraphState graphState, Status.Flag flag, int i) {
        }
    }

    public void perform() {
        String runTimeName = "";
        switch (scoopRuntime) {
            case RUNTIME_QOQ:
                runTimeName = "QoQ";
                break;
            case RUNTIME_RQ:
                runTimeName = "RQ";
                break;
        }
        this.runs = new ExplorationRunSet(startGraph, runTimeName);
        for (int i = 0; i < warmup + repetitions; i++) {
            List<ExplorationResult> results = new LinkedList<>();
            // Using ExecutorService, execute the exploration and check if the exploration has been interrupted.
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            setUpExploration();
            final long[] runTimeNano = new long[1];
            final double[] memoryUsed = new double[1];
            final Future<Void> future = executor.submit(
                    () -> {
                        Runtime runtime = Runtime.getRuntime();
                        runtime.gc();
                        //long startMemory = scoop.runtime.totalMemory() - scoop.runtime.freeMemory();
                        exploration.addListener(renderer.getExplorationReporter());
                        long startTime = System.nanoTime();
                        exploration.play();
                        //FIXME:exploration.play(gts, gts.startState());
                        runTimeNano[0] = System.nanoTime() - startTime;
                        runtime.gc();
                        //long endMemory = scoop.runtime.totalMemory() - scoop.runtime.freeMemory();
                        long peakMemoryUsed = 0;
                        List<MemoryPoolMXBean> pools = ManagementFactory.getMemoryPoolMXBeans();
                        for (MemoryPoolMXBean pool : pools) {
                            MemoryUsage peak = pool.getPeakUsage();
                            peakMemoryUsed += peak.getUsed();
                        }
                        memoryUsed[0] = peakMemoryUsed;
                        return null;
                    });
            try {
                if (timeout > 0) {
                    future.get(timeout, TimeUnit.SECONDS);
                } else {
                    future.get();
                }
            } catch (TimeoutException e) {
                renderer.outputTimeoutMessage();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            executor.shutdownNow(); // Shut down executor immediately, terminating running tasks.

            if (i >= warmup) {
                String errorStatus = "";
                for (GraphState state : gts.getFinalStates()) {
                    // Iterator through all nodes (e.g. for finding nodes of a certain type).
                    for (HostNode node : state.getGraph().nodeSet()) {
                        String nodeType = node.getType().text();

                        if (nodeType.equals("DeadlockError")) {
                            String message = getMessage(state, node);
                            results.add(new DeadlockExplorationResult(message));
                        }

                        else if (nodeType.equals("Success")) {
                            results.add(new SuccessExplorationResult("No error found."));
                        } else if (nodeType.contains("Error") ||
                                nodeType.contains("Failure")) {
                            String message = getMessage(state, node);
                            results.add(new GenericExplorationErrorResult(nodeType, message));
                        }
                    }
                }
                runs.add(new ExplorationRun(exploration, results, counterListener.states, counterListener.transitions,
                        memoryUsed[0], ((long) runTimeNano[0]) / 1000000000.));
            }
        }
    }

    /**
     * Fetch the message attached to the HostNode representing an Error state.
     */
    private String getMessage(GraphState state, HostNode error) {
        for (HostEdge edge : state.getGraph().edgeSet(error)) {
            if (edge.label().toString().equals("message")) {
                return edge.target().toString().substring("string:".length() + 1, edge.target().toString().length() - 1);
            }
        }
        return "No message attached.";
    }

    public void render() {
        renderer.renderRuns(runs);
        System.out.printf(renderer.getLastRenderOutput());
    }

    void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }
}